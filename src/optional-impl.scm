
(define-alias optional java.util.Optional)

(define (make-optional val) ::optional
  (optional:ofNullable val))

(define (make-empty-optional) ::optional
  (optional:empty))

(define (optional? obj) ::boolean
  (instance? obj java.util.Optional))

(define (optional-set? o::optional) ::boolean
  (o:isPresent))

(define (optional-ref o::optional)
  (o:get))
  
(define (optional-ref/default o::optional d)
  (o:orElse d))

(define (optional-ref/get o::optional proc)
  (o:orElseGet (lambda () (proc))))

(define (optional-filter pred? o::optional) ::optional
  (o:filter (lambda (a) ::boolean (pred? a))))

(define (optional-call f o::optional)
  (o:ifPresent (lambda (a) (f a))))

(define (optional-map f o::optional) ::optional
  (o:map (lambda (a) (f a))))

(define (optional-flat-map f o::optional) ::optional
  (o:flatMap (lambda (a) ::optional (f a))))

(define (optional=? a::optional b::optional) ::boolean
  (a:equals b))

(define (optional-hash o::optional) ::int
  (o:hashCode))
