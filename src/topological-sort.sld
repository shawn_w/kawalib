;;; tsort, api from SLIB -*- scheme -*-
(define-library (topological-sort)
  (export tsort)
  (import
   (scheme base)
   (srfi 1)
   (srfi 117)
   (queue)
   (srfi 126))
  (include "topological-sort-impl.scm"))
(define-library (tsort)
  (export tsort)
  (import topological-sort))
