;;; uniform vector utility functions
(define-library (uvector-utils)
  (import (kawa base)
          (srfi 128))
  (export
   s8v-binary-search s8vector-copy
   s8vector=? s8vector<? s8vector-comparator
   s8vector-fill!
   s8vector-hash s8vector-sort! s8vector-copy!
   s8vector-index s8vector-index-right
   s8vector-find s8vector-find-right
   s8vector-for-each s8vector-map!
   s8vector-fold s8vector-map
   
   u8v-binary-search u8vector-copy
   u8vector=? u8vector<? u8vector-comparator
   u8vector-fill!
   u8vector-hash u8vector-sort! u8vector-copy!
   u8vector-index u8vector-index-right
   u8vector-find u8vector-find-right
   u8vector-for-each u8vector-map!
   u8vector-fold u8vector-map

   s16v-binary-search s16vector-copy
   s16vector=? s16vector<? s16vector-comparator
   s16vector-fill!
   s16vector-hash s16vector-sort! s16vector-copy!
   s16vector-index s16vector-index-right
   s16vector-find s16vector-find-right
   s16vector-for-each s16vector-map!
   s16vector-fold s16vector-map
   
   u16v-binary-search u16vector-copy
   u16vector=? u16vector<? u16vector-comparator
   u16vector-fill!
   u16vector-hash u16vector-sort! u16vector-copy! 
   u16vector-index u16vector-index-right
   u16vector-find u16vector-find-right
   u16vector-for-each u16vector-map!
   u16vector-fold u16vector-map
   
   s32v-binary-search s32vector-copy
   s32vector=? s32vector<? s32vector-comparator
   s32vector-fill!
   s32vector-hash s32vector-sort! s32vector-copy!
   s32vector-index s32vector-index-right
   s32vector-find s32vector-find-right
   s32vector-for-each s32vector-map!
   s32vector-fold s32vector-map
   
   u32v-binary-search u32vector-copy
   u32vector=? u32vector<? u32vector-comparator
   u32vector-fill!
   u32vector-hash u32vector-sort! u32vector-copy!
   u32vector-index u32vector-index-right
   u32vector-find u32vector-find-right
   u32vector-for-each u32vector-map!
   u32vector-fold u32vector-map
   
   s64v-binary-search s64vector-copy
   s64vector=? s64vector<? s64vector-comparator
   s64vector-fill!
   s64vector-hash s64vector-sort! s64vector-copy!
   s64vector-index s64vector-index-right
   s64vector-find s64vector-find-right
   s64vector-for-each s64vector-map!
   s64vector-fold s64vector-map
   
   u64v-binary-search u64vector-copy
   u64vector=? u64vector<? u64vector-comparator
   u64vector-fill!
   u64vector-hash u64vector-sort! u64vector-copy!
   u64vector-index u64vector-index-right
   u64vector-find u64vector-find-right
   u64vector-for-each u64vector-map!
   u64vector-fold u64vector-map
   
   f32v-binary-search f32vector-copy
   f32vector=? f32vector<? f32vector-comparator
   f32vector-fill!
   f32vector-hash f32vector-sort! f32vector-copy!
   f32vector-index f32vector-index-right
   f32vector-find f32vector-find-right
   f32vector-for-each f32vector-map!
   f32vector-fold f32vector-map
   
   f64v-binary-search f64vector-copy
   f64vector=? f64vector<? f64vector-comparator
   f64vector-fill!
   f64vector-hash f64vector-sort! f64vector-copy!
   f64vector-index f64vector-index-right
   f64vector-find f64vector-find-right
   f64vector-for-each f64vector-map!
   f64vector-fold f64vector-map
   )
  (include "uvec-utils-impl.scm"))
