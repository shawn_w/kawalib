;;; Permuations of vectors -*- scheme -*-

(module-name permutations)
(module-export next-permutation! permutations->list permutations->stream
               random-source-make-permutations random-permutation
               )

(import
 (kawa base)
 (srfi 27)
 (srfi 41)
 (srfi 117)
 (srfi 132)
 (srfi 133))

(define (next-permutation! vec less?)
  (let ((vec<? (lambda (i j) (less? (vector-ref vec i) (vector-ref vec j))))
        (last (vector-length vec))
        (i (- (vector-length vec) 1))
        (i1 #f)
        (i2 #f))
    (if (< last 2)
        #f
        (let loop ()
          (set! i1 i)
          (set! i (- i 1))
          (cond
           ((vec<? i i1)
            (do ((i2tmp (- last 1) (- i2tmp 1)))
                ((vec<? i i2tmp) (set! i2 i2tmp)))
            (vector-swap! vec i i2)
            (vector-reverse! vec i1 last)
            #t)
           ((= i 0)
            (vector-reverse! vec)
            #f)
           (else
            (loop)))))))

(define (permutations->stream vec-or-list less?)
  (if (vector? vec-or-list)
      (stream-let loop ((more #t)
                        (vec (vector-sort less? vec-or-list)))
                  (if more
                      (stream-cons (vector-copy vec)
                                   (loop (next-permutation! vec less?)
                                         vec))
                      stream-null))
      (stream-map vector->list
                  (permutations->stream (list->vector vec-or-list)
                                        less?))))

(define (permutations->list vec less?)
  (stream->list (permutations->stream vec less?)))

;; Following functions are from SRFI-27's 'recommended usage' section

;  Copyright (C) Sebastian Egner (2002). All Rights Reserved.

;  Permission is hereby granted, free of charge, to any person obtaining a copy of
;  this software and associated documentation files (the "Software"), to deal in
;  the Software without restriction, including without limitation the rights to
;  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
;  of the Software, and to permit persons to whom the Software is furnished to do
;  so, subject to the following conditions:

;  The above copyright notice and this permission notice shall be included in all
;  copies or substantial portions of the Software.

;  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;  SOFTWARE.


;; Return a generator that takes one integer argument N and returns a random
;; permutation of the set {0, ..., N-1}.
(define (random-source-make-permutations s)
  (let ((rand (random-source-make-integers s)))
    (lambda (n)
      (let ((x (make-vector n 0)))
        (do ((i 1 (+ i 1)))
            ((= i n))
          (vector-set! x i i))
        (do ((k n (- k 1)))
            ((= k 1) x)
          (let* ((i (- k 1))
                 (j (rand k))
                 (xi (vector-ref x i))
                 (xj (vector-ref x j)))
            (vector-set! x i xj)
            (vector-set! x j xi)))))))

;; (random-permutation N) returns a random permutation of the set {0, ..., N-1}
(define random-permutation
  (random-source-make-permutations default-random-source))
