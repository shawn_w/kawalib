;;; Basic stack datatype -*- scheme -*-

(define-library (stack)
  (export
   stack make-stack
   stack? stack-empty? stack-size
   stack-push! stack-pop! stack-peek

  make-istack istack istack? istack-empty? istack-size
  istack-push istack-pop istack-peek)
  
  (import (rename (collections)
                  (coll-queue? stack?)
                  (coll-empty? stack-empty?)
                  (coll-size stack-size)
                  (coll-add! stack-push!)
                  (coll-queue-remove! stack-pop!)
                  (coll-queue-element stack-peek)))
  (import (rename (kawa base)
                  (list istack)
                  (list? istack?)
                  (null? istack-empty?)
                  (length istack-size)
                  (cons istack-push)
                  (car istack-peek)))

  (begin
    (define (make-stack #!optional (vals '()))
      (coll-deque->stack (coll-make-array-deque vals)))
    (define (stack #!rest vals) (make-stack vals))
    (define (make-istack #!optional (st '())) st)
    (define (istack-pop st) (values (istack-peek st) (cdr st)))
  ))
