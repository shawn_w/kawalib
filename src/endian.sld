;;; endian conversion functions -*- scheme -*-
(define-library (endian)
  (export
   u16-big->little s16-big->little u16-little->big s16-big->little
   u32-big->little s32-big->little u32-little->big s32-big->little
   u64-big->little s64-big->little u64-little->big s64-big->little

   u16-big->native s16-big->native u16-little->native s16-big->native
   u32-big->native s32-big->native u32-little->native s32-big->native
   u64-big->native s64-big->native u64-little->native s64-big->native

   u16-native->big s16-native->big u16-native->little s16-native->little
   u32-native->big s32-native->big u32-native->little s32-native->little
   u64-native->big s64-native->big u64-native->little s64-native->little)
  (import (scheme base)
          (srfi 74)
          (only (data-structures) identity))
  (begin
    (define (u16-big->little n::ushort)
      (let ((blob (make-blob 2)))
        (blob-u16-set! (endianness big) blob 0 n)
        (blob-u16-ref (endianness little) blob 0)))
    (define (s16-big->little n::short)
      (let ((blob (make-blob 2)))
        (blob-s16-set! (endianness big) blob 0 n)
        (blob-s16-ref (endianness little) blob 0)))
    (define (u32-big->little n::uint)
      (let ((blob (make-blob 4)))
        (blob-u32-set! (endianness big) blob 0 n)
        (blob-u32-ref (endianness little) blob 0)))
    (define (s32-big->little n::int)
      (let ((blob (make-blob 4)))
        (blob-s32-set! (endianness big) blob 0 n)
        (blob-s32-ref (endianness little) blob 0)))
    (define (u64-big->little n::ulong)
      (let ((blob (make-blob 8)))
        (blob-u64-set! (endianness big) blob 0 n)
        (blob-u64-ref (endianness little) blob 0)))
    (define (s64-big->little n::long)
      (let ((blob (make-blob 8)))
        (blob-s64-set! (endianness big) blob 0 n)
        (blob-s64-ref (endianness little) blob 0)))
    
    (define (u16-little->big n::ushort)
      (let ((blob (make-blob 2)))
        (blob-u16-set! (endianness little) blob 0 n)
        (blob-u16-ref (endianness big) blob 0)))
    (define (s16-little->big n::short)
      (let ((blob (make-blob 2)))
        (blob-s16-set! (endianness little) blob 0 n)
        (blob-s16-ref (endianness big) blob 0)))
    (define (u32-little->big n::uint)
      (let ((blob (make-blob 4)))
        (blob-u32-set! (endianness little) blob 0 n)
        (blob-u32-ref (endianness big) blob 0)))
    (define (s32-little->big n::int)
      (let ((blob (make-blob 4)))
        (blob-s32-set! (endianness little) blob 0 n)
        (blob-s32-ref (endianness big) blob 0)))
    (define (u64-little->big n::ulong)
      (let ((blob (make-blob 8)))
        (blob-u64-set! (endianness little) blob 0 n)
        (blob-u64-ref (endianness big) blob 0)))
    (define (s64-little->big n::long)
      (let ((blob (make-blob 8)))
        (blob-s64-set! (endianness little) blob 0 n)
        (blob-s64-ref (endianness big) blob 0)))

    ;; Big-endian versions
    (define u16-native->little u16-big->little)
    (define s16-native->little s16-big->little)
    (define u32-native->little u32-big->little)
    (define s32-native->little s32-big->little)
    (define u64-native->little u64-big->little)
    (define s64-native->little s64-big->little)

    (define u16-native->big identity)
    (define s16-native->big identity)
    (define u32-native->big identity)
    (define s32-native->big identity)
    (define u64-native->big identity)
    (define s64-native->big identity)

    (define u16-little->native u16-little->big)
    (define s16-little->native s16-little->big)
    (define u32-little->native u32-little->big)
    (define s32-little->native s32-little->big)
    (define u64-little->native u64-little->big)
    (define s64-little->native s64-little->big)
    
    (define u16-big->native identity)
    (define s16-big->native identity)
    (define u32-big->native identity)
    (define s32-big->native identity)
    (define u64-big->native identity)
    (define s64-big->native identity)

    ;; Little endian versions
    #|
    (define u16-native->little identity)
    (define s16-native->little identity)
    (define u32-native->little identity)
    (define s32-native->little identity)
    (define u64-native->little identity)
    (define s64-native->little identity)

    (define u16-native->big u16-little->big)
    (define s16-native->big s16-little->big)
    (define u32-native->big u32-little->big)
    (define s32-native->big s32-little->big)
    (define u64-native->big u64-little->big)
    (define s64-native->big s64-little->big)

    (define u16-little->native identity)
    (define s16-little->native identity)
    (define u32-little->native identity)
    (define s32-little->native identity)
    (define u64-little->native identity)
    (define s64-little->native identity)
    
    (define u16-big->native u16-big->little)
    (define s16-big->native s16-big->little)
    (define u32-big->native u32-big->little)
    (define s32-big->native s32-big->little)
    (define u64-big->native u64-big->little)
    (define s64-big->native s64-big->little)
    |#
    
    ))

   
   
