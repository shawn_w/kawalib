(define (n2f a) (if (< a 0) #f a))

(define-syntax make-binary-search
  (syntax-rules ()
    ((_ type ctype name)
     (define name
       (make-procedure
        (lambda ((vec :: type) (key :: ctype))
          (n2f
           (java.util.Arrays:binarySearch (vec:getBuffer) 0 (vec:size) key)))
        (lambda ((vec :: type) (key :: ctype) (from :: int))
          (n2f
           (java.util.Arrays:binarySearch (vec:getBuffer) from (vec:size) key)))
        (lambda ((vec :: type) (key :: ctype) (from :: int) (to :: int))
          (n2f
           (java.util.Arrays:binarySearch (vec:getBuffer) from to key))))))))

(define-syntax make-copy
  (syntax-rules ()
    ((_ type name)
     (define name
       (make-procedure
        (lambda ((vec :: type)) :: type
          (make type
            (java.util.Arrays:copyOf (vec:getBuffer) (vec:size))))
        (lambda ((vec :: type) (from :: int)) :: type
          (make type
            (java.util.Arrays:copyOf (vec:getBuffer) from (vec:size))))
        (lambda ((vec :: type) (from :: int) (to :: int)) :: type
          (make type
            (java.util.Arrays:copyOfRange (vec:getBuffer) from to))))))))

(define-syntax make-copy!
  (syntax-rules ()
    ((_ type name)
     (define name
       (make-procedure
        (lambda ((dest :: type) (destpos :: int) (src :: type)
                 (srcpos :: int) (len :: int))
          (java.lang.System:arraycopy
           (src:getBuffer) srcpos
           (dest:getBuffer) destpos len))
        (lambda ((dest :: type) (destpos :: int) (src :: type))
          (name dest destpos src 0 (min (- (dest:size) destpos) (src:size))))
        (lambda ((dest :: type) (destpos :: int) (src :: type) (srcpos :: int))
          (name dest destpos src srcpos (- (src:size) srcpos))))))))

(define-syntax make-comps
  (syntax-rules ()
    ((_ type eqname ltname)
     (begin
       (define (eqname (a :: type) (b :: type)) :: boolean
         (let ((alen (a:size))
               (blen (b:size))
               (abuf (a:getBuffer))
               (bbuf (b:getBuffer)))
           (if (= alen blen)
               (let loop ((i 0))
                 (if (= i alen)
                     #t
                     (if (= (abuf i) (bbuf i))
                         (loop (+ i 1))
                         #f)))
               #f)))
       (define (ltname (a:: type) (b :: type)) :: boolean
         (let ((alen (a:size))
               (blen (b:size))
               (abuf (a:getBuffer))
               (bbuf (b:getBuffer)))
           (if (< alen blen)
               #t
               (if (= alen blen)
                   (let loop ((i 0))
                     (if (= i alen)
                         #t
                         (if (>= (abuf i) (bbuf i))
                             #f
                             (loop (+ i 1)))))
                   #f))))))))

(define-syntax make-fill
  (syntax-rules ()
    ((_ type ctype name)
     (define name
       (make-procedure
        (lambda ((vec :: type) (val :: ctype))
          (java.util.Arrays:fill (vec:getBuffer) 0 (vec:size) val))
        (lambda ((vec :: type) (val :: ctype) (from :: int))
          (java.util.Arrays:fill (vec:getBuffer) from (vec:size) val))
        (lambda ((vec :: type) (val :: ctype) (from :: int) (to :: int))
          (java.util.Arrays:fill (vec:getBuffer) from to val)))))))

;;; Can't use java.util.Arrays.hashCode because of how the underlying
;;; array might be a different length than the vector. Use the same
;;; algorithm, though.
(define-syntax make-hash
  (syntax-rules ()
    ((_ type name)
     (define (name (vec :: type)) :: int
       (let ((buf (vec:getBuffer))
             (len (vec:size)))
         (if (= len 0)
             0
             (let loop ((hashval 1)
                        (n 0))
               (if (= n len)
                   hashval
                   (loop (+ (* 31 hashval) (buf n)) (+ n 1))))))))))

(define-syntax make-ssort
  (syntax-rules ()
    ((_ type name)
     (define name
       (make-procedure
        (lambda ((vec :: type))
          (java.util.Arrays:sort (vec:getBuffer) 0 (vec:size)))
        (lambda ((vec :: type) (from :: int) (to :: int))
          (java.util.Arrays:sort (vec:getBuffer) from to)))))))

(define-syntax make-usort
  (syntax-rules ()
    ((_ type name)
     (define name
       (make-procedure
        (lambda ((vec :: type))
          (name vec 0 (vec:size)))          
        (lambda ((vec :: type) (from :: int) (to :: int))
          (let ((buf (vec:getBuffer))
                (len (vec:size)))
            (java.util.Arrays:sort buf from to)
            (let ((posindex (let loop ((i ::int 0))
                              (if (= i len)
                                  #f
                                  (if (>= (buf i) 0)
                                      i
                                      (loop (+ i 1)))))))
              (when (and posindex (> posindex 0))
                    (let ((tmp (java.util.Arrays:copyOf
                                buf (as int posindex))))
                      (java.lang.System:arraycopy
                       buf posindex buf 0 (- len posindex))
                      (java.lang.System:arraycopy
                       tmp 0 buf (- len posindex) tmp:length)))))))))))

(define-syntax make-index
  (syntax-rules ()
    ((_ type name rname)
     (begin
       (define (name pred? (vec :: type) #!optional (from 0) (to #f))
         (let ((buf (vec:getBuffer))
               (len (if to to (vec:size))))
           (let loop ((i ::int from))
             (if (= i len)
                 #f
                 (if (pred? (buf i))
                     i
                     (loop (+ i 1)))))))
       (define (rname pred? (vec :: type) #!optional (from 0) (to #f))
         (let ((buf (vec:getBuffer)))
           (let loop ((i ::int (- (if to to (vec:size)) 1)))
             (if (< i from)
                 #f
                 (if (pred? (buf i))
                     i
                     (loop (- i 1)))))))))))

(define-syntax make-find
  (syntax-rules ()
    ((_ type ctype name rname)
     (begin
       (define (name (val :: ctype) (vec :: type)
                     #!optional (from 0) (to #f))
         (let ((buf (vec:getBuffer))
               (len (if to to (vec:size))))
           (let loop ((i ::int from))
             (if (= i len)
                 #f
                 (if (= (buf i) val)
                     i
                     (loop (+ i 1)))))))
       (define (rname (val :: ctype) (vec :: type) #!optional (from 0) (to #f))
         (let ((buf (vec:getBuffer)))
           (let loop ((i ::int (- (if to to (vec:size)) 1)))
             (if (< i from)
                 #f
                 (if (= (buf i) val)
                     i
                     (loop (- i 1)))))))))))

(define-syntax make-iterators
  (syntax-rules ()
    ((_ type ctype fldname frname mname mname!)
     (begin
       (define (fldname f init (vec :: type))
         (let ((buf (vec:getBuffer))
               (len (vec:size)))
           (let loop ((acc init)
                      (i ::int 0))
             (if (= i len)
                 acc
                 (loop (f (buf i) acc) (+ i 1))))))        
       (define (frname f (vec :: type))
         (let ((len (vec:size))
               (buf (vec:getBuffer)))
           (do ((i ::int 0 (+ i 1)))
               ((= i len))
             (f (buf i)))))
       (define (mname f (vec :: type)) :: type
         (let* ((len (vec:size))
                (buf (vec:getBuffer))
                (dest (ctype[] length: len)))
           (do ((i ::int 0 (+ i 1)))
               ((= i len) (make type dest))
             (set! (dest i) (f (buf i))))))                
       (define (mname! f (vec :: type))
         (let ((buf (vec:getBuffer)))
           (do ((i ::int (- (vec:size) 1) (- i 1)))
               ((< i 0))
             (set! (buf i) (f (buf i))))))))))

(make-binary-search s8vector byte s8v-binary-search)
(make-copy <s8vector> s8vector-copy)
(make-copy! s8vector s8vector-copy!)
(make-comps s8vector s8vector=? s8vector<?)
(make-fill s8vector byte s8vector-fill!)
(make-hash s8vector s8vector-hash)
(make-ssort s8vector s8vector-sort!)
(make-index s8vector s8vector-index s8vector-index-right)
(make-find s8vector byte s8vector-find s8vector-find-right)
(define s8vector-comparator
  (make-comparator s8vector? s8vector=? s8vector<? s8vector-hash))
(make-iterators <s8vector> byte s8vector-fold s8vector-for-each s8vector-map
                s8vector-map!)

(make-binary-search u8vector ubyte u8v-binary-search)
(make-copy <u8vector> u8vector-copy)
(make-copy! u8vector u8vector-copy!)
(make-comps u8vector u8vector=? u8vector<?)
(make-fill u8vector ubyte u8vector-fill!)
(make-hash u8vector u8vector-hash)
(make-usort u8vector u8vector-sort!)
(make-index u8vector u8vector-index u8vector-index-right)
(make-find u8vector ubyte u8vector-find u8vector-find-right)
(define u8vector-comparator
  (make-comparator u8vector? u8vector=? u8vector<? u8vector-hash))
(make-iterators <u8vector> ubyte u8vector-fold u8vector-for-each
u8vector-map u8vector-map!)

(make-binary-search s16vector short s16v-binary-search)
(make-copy <s16vector> s16vector-copy)
(make-copy! s16vector s16vector-copy!)
(make-comps s16vector s16vector=? s16vector<?)
(make-fill s16vector short s16vector-fill!)
(make-hash s16vector s16vector-hash)
(make-ssort s16vector s16vector-sort!)
(make-index s16vector s16vector-index s16vector-index-right)
(make-find s16vector short s16vector-find s16vector-find-right)
(define s16vector-comparator
  (make-comparator s16vector? s16vector=? s16vector<? s16vector-hash))
(make-iterators <s16vector> short s16vector-fold s16vector-for-each
s16vector-map s16vector-map!)

(make-binary-search u16vector ushort u16v-binary-search)
(make-copy <u16vector> u16vector-copy)
(make-copy! u16vector u16vector-copy!)
(make-comps u16vector u16vector=? u16vector<?)
(make-fill u16vector ushort u16vector-fill!)
(make-hash u16vector u16vector-hash)
(make-usort u16vector u16vector-sort!)
(make-index u16vector u16vector-index u16vector-index-right)
(make-find u16vector ushort u16vector-find u16vector-find-right)
(define u16vector-comparator
  (make-comparator u16vector? u16vector=? u16vector<? u16vector-hash))
(make-iterators <u16vector> ushort u16vector-fold u16vector-for-each
u16vector-map u16vector-map!)

(make-binary-search s32vector int s32v-binary-search)
(make-copy <s32vector> s32vector-copy)
(make-copy! s32vector s32vector-copy!)
(make-comps s32vector s32vector=? s32vector<?)
(make-fill s32vector int s32vector-fill!)
(make-hash s32vector s32vector-hash)
(make-ssort s32vector s32vector-sort!)
(make-index s32vector s32vector-index s32vector-index-right)
(make-find s32vector int s32vector-find s32vector-find-right)
(define s32vector-comparator
  (make-comparator s32vector? s32vector=? s32vector<? s32vector-hash))
(make-iterators <s32vector> int s32vector-fold s32vector-for-each
s32vector-map s32vector-map!)

(make-binary-search u32vector uint u32v-binary-search)
(make-copy <u32vector> u32vector-copy)
(make-copy! u32vector u32vector-copy!)
(make-comps u32vector u32vector=? u32vector<?)
(make-fill u32vector uint u32vector-fill!)
(make-hash u32vector u32vector-hash)
(make-usort u32vector u32vector-sort!)
(make-index u32vector u32vector-index u32vector-index-right)
(make-find u32vector uint u32vector-find u32vector-find-right)
(define u32vector-comparator
  (make-comparator u32vector? u32vector=? u32vector<? u32vector-hash))
(make-iterators <u32vector> uint u32vector-fold u32vector-for-each
u32vector-map u32vector-map!)

(make-binary-search s64vector long s64v-binary-search)
(make-copy <s64vector> s64vector-copy)
(make-copy! s64vector s64vector-copy!)
(make-comps s64vector s64vector=? s64vector<?)
(make-fill s64vector long s64vector-fill!)
(make-hash s64vector s64vector-hash)
(make-ssort s64vector s64vector-sort!)
(make-index s64vector s64vector-index s64vector-index-right)
(make-find s64vector long s64vector-find s64vector-find-right)
(define s64vector-comparator
  (make-comparator s64vector? s64vector=? s64vector<? s64vector-hash))
(make-iterators <s64vector> long s64vector-fold s64vector-for-each
s64vector-map s64vector-map!)

(make-binary-search u64vector ulong u64v-binary-search)
(make-copy <u64vector> u64vector-copy)
(make-copy! u64vector u64vector-copy!)
(make-comps u64vector u64vector=? u64vector<?)
(make-fill u64vector ulong u64vector-fill!)
(make-hash u64vector u64vector-hash)
(make-usort u64vector u64vector-sort!)
(make-index u64vector u64vector-index u64vector-index-right)
(make-find u64vector ulong u64vector-find u64vector-find-right)
(define u64vector-comparator
  (make-comparator u64vector? u64vector=? u64vector<? u64vector-hash))
(make-iterators <u64vector> ulong u64vector-fold u64vector-for-each
u64vector-map u64vector-map!)

(make-binary-search f32vector float f32v-binary-search)
(make-copy <f32vector> f32vector-copy)
(make-copy! f32vector f32vector-copy!)
(make-comps f32vector f32vector=? f32vector<?)
(make-fill f32vector float f32vector-fill!)
(make-hash f32vector f32vector-hash)
(make-ssort f32vector f32vector-sort!)
(make-index f32vector f32vector-index f32vector-index-right)
(make-find f32vector float f32vector-find f32vector-find-right)
(define f32vector-comparator
  (make-comparator f32vector? f32vector=? f32vector<? f32vector-hash))
(make-iterators <f32vector> float f32vector-fold f32vector-for-each
f32vector-map f32vector-map!)

(make-binary-search f64vector double f64v-binary-search)
(make-copy <f64vector> f64vector-copy)
(make-copy! f64vector f64vector-copy!)
(make-comps f64vector f64vector=? f64vector<?)
(make-fill f64vector double f64vector-fill!)
(make-hash f64vector f64vector-hash)
(make-ssort f64vector f64vector-sort!)
(make-index f64vector f64vector-index f64vector-index-right)
(make-find f64vector double f64vector-find f64vector-find-right)
(define f64vector-comparator
  (make-comparator f64vector? f64vector=? f64vector<? f64vector-hash))
(make-iterators <f64vector> double f64vector-fold f64vector-for-each
f64vector-map f64vector-map!)
