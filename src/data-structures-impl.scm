;;; Reimplementation of chicken 4 data-structures library.

;; lists
(define (alist-ref key alist #!optional (test eqv?) (default #f))
  (let ((val (assoc key alist test)))
    (if val
        (cdr val)
        default)))

(define (alist-update key value alist #!optional (test eqv?))
  (alist-cons key value (alist-delete key alist test)))

(define (alist-update! key value alist #!optional (test eqv?))
  (let ((val (assoc key alist test)))
    (if val
        (begin
          (set-cdr! val value)
          alist)
        (alist-cons key value alist))))

(define (rassoc key list #!optional (test eqv?))
  (cond
   ((null? list) #f)
   ((test (cdar list)) (car list))
   (rassoc key (cdr list) test)))

(define (butlast list) (drop-right list 1))

(define (chop lst n)
  (let loop ((len (length lst))
             (lst lst))
    (if (<= len n)
        (list lst)  
        (let-values (((first rest) (split-at lst n)))
          (cons first (loop (- len n) rest))))))

(define (compress blist list)
  (let ((res (list-queue)))
    (for-each (lambda (test val)
                (if test (list-queue-add-back! res val))) blist list)
    (list-queue-list res)))

(define (flatten #!rest lists)
  (let ((res (list-queue)))
    (letrec ((flatten-one (lambda (item)
                            (if (atom? item)
                                (list-queue-add-back! res item)
                                (for-each flatten-one item)))))
      (for-each flatten-one lists))
    (list-queue-list res)))

(define (intersperse list x)
  (let ((res (list-queue)))
    (let loop ((list list))
      (cond
       ((null? list)
        (list-queue-list res))
       ((null? (cdr list))
        (list-queue-add-back! res (car list))
        (list-queue-list res))
       (else
        (list-queue-add-back! res (car list))
        (list-queue-add-back! res x)
        (loop (cdr list)))))))

(define (join lol #!optional (list '()))
  (concatenate (intersperse lol list)))

(define (tail? x list)
  (cond
   ((null? list)
    (null? x))
   ((eq? x list)
    #t)
   (else
    (tail? x (cdr list)))))

;; queues

(define (queue-push-back-list! q lst)
  (for-each (cut queue-push-back! q <>) (reverse lst)))

;; Sorting

(define (merge list1 list2 less?)
  (list-merge less? list1 list2))

(define (merge! list1 list2 less?)
  (list-merge! less? list1 list2))

(define (sort seq less?)
  (cond
   ((vector? seq) (vector-sort less? seq))
   ((list? seq) (list-sort less? seq))
   (else
    (error "sort must take a list or vector"))))

(define (sort! seq less?)
  (cond
   ((vector? seq) (vector-sort! less? seq))
   ((list? seq) (list-sort! less? seq))
   (else
    (error "sort! must take a list or vector"))))

(define (sorted? seq less?)
  (cond
   ((vector? seq) (vector-sorted? less? seq))
   ((list? seq) (list-sorted? less? seq))
   (else
    (error "sorted? must take a list or vector"))))

;; Strings

(define (->string val)
  (format #f "~A" val))

(define (conc #!rest args)
  (apply string-append (map ->string args)))

(define (string-chop str len)
  (let ((res (list-queue)))
    (let loop ((offset 0)
               (slen (string-length str)))
      (if (<= slen len)
          (begin
            (list-queue-add-back! res (substring str offset (+ offset slen)))
            (list-queue-list res))
          (begin
            (list-queue-add-back! res (substring str offset (+ offset len)))
            (loop (+ offset len) (- slen len)))))))

(define (string-chomp str #!optional (suffix "\n"))
  (if (string-suffix? suffix str)
      (string-drop-right str (string-length suffix))
      str))

(define (string-compare3 s1 s2)
  (cond
   ((string=? s1 s2) 0)
   ((string<? s1 s2) -1)
   (else 1)))

(define (string-compare3-ci s1 s2)
  (cond
   ((string-ci=? s1 s2) 0)
   ((string-ci<? s1 s2) -1)
   (else 1)))

(define (string-split str #!optional (tokens " \t\n") (keepempty? #f))
  (let ((re (make-string 1 #\[)))
    (string-for-each (lambda (c)
                       (if (member c '(#\- #\[ #\] #\:) char=?)
                           (string-append! re #\\))
                       (string-append! re c)) tokens)
    (string-append! re #\])
    (let ((words (regex-split re str)))
      (if keepempty?
          words
          (filter (lambda (word) (> (string-length word) 0)) words)))))

;; from and to are both strings
(define (string-tr str from to)
  (let ((res (make-string)))
    (string-for-each
     (lambda (c)
       (let ((p (string-index from c)))
         (string-append! res
                         (if p
                             (string-ref to p)
                             c)))) str)
    res))

;; from is a character
(define (string-tr2 str from to)
  (let ((res (make-string)))
    (string-for-each
     (lambda (c)
       (string-append! res
                       (if (char=? c from)
                           to
                           c))) str)
    res))

;; from is a string
(define (string-tr3 str from to)
  (let ((res (make-string))
        (cs (string->char-set from)))
    (string-for-each
     (lambda (c)
       (string-append! res
                       (if (char-set-contains? cs c)
                           to
                           c))) str)
    res))

(define (string-remove-chars str from)
  (let* ((cs (cond
             ((char? from)
              (char-set from))
             ((string? from)
              (string->char-set from))
             (else
              (apply char-set from))))
         (bad-char? (cut char-set-contains? cs <>)))
    (string-remove bad-char? str)))

(define (string-translate str from #!optional (to #f))
  (cond
   ((not to)
    (string-remove-chars str from))
   ((and (string? from) (string? to))
    (string-tr str from to))
   ((char? from)
    (string-tr2 str from to))
   ((string? from)
    (string-tr3 str from to))
   (else
    (error "string-translate: invalid argument"))))

(define (string-translate* str smap)
  (fold (lambda (trans str)
          (regex-replace* (regex-quote (car trans)) str (cdr trans)))
        str smap))

(define (substring=? str1 str2 #!optional (start1 0) (start2 0) (length #f))
  (let* ((length (if (integer? length)
                     length
                     (min (- (string-length str1) start1)
                          (- (string-length str2) start2))))
         (end1 (+ start1 length))
         (end2 (+ start2 length)))
    (if (string-contains str1 str2 start1 end1 start2 end2)
        #t
        #f)))

(define (substring-ci=? str1 str2 #!optional (start1 0) (start2 0) (length #f))
  (let* ((length (if (integer? length)
                     length
                     (min (- (string-length str1) start1)
                          (- (string-length str2) start2))))
         (end1 (+ start1 length))
         (end2 (+ start2 length)))
    (if (string-contains-ci str1 str2 start1 end1 start2 end2)
        #t
        #f)))

(define (substring-index which where #!optional (start 0))
  (string-contains where which start))

(define (substring-index-ci which where #!optional (start 0))
  (string-contains-ci where which start))

;; combinators
(define any? (lambda _ #t))

(define (constantly #!rest x)
  (lambda _ (apply values x)))

(define (complement proc) (lambda (arg) (not (proc arg))))

(define (conjoin #!rest preds)
  (lambda (val)
    (let loop ((preds preds))
      (cond
       ((null? preds) #t)
           (((car preds) val) (loop (cdr preds)))
           (else #f)))))

(define (disjoin #!rest preds)
  (lambda (val)
    (let loop ((preds preds))
      (cond
       ((null? preds) #f)
           (((car preds) val) #t)
           (else (loop (cdr preds)))))))

(define (compose #!rest procs)
  (let ((procs (reverse procs)))
    (lambda args        
      (if (null? procs)
          (apply values args)
          (let loop ((procs (cdr procs))
                     (args (receive a (apply (car procs) args) a)))
            (if (null? procs)
                (apply values args)
                (loop (cdr procs)
                      (receive a (apply (car procs) args) a))))))))

(define (flip proc2) (lambda (x y) (proc2 y x)))

(define (identity x) x)

(define (list-of? pred?)
  (lambda (lst)
    (every pred? lst)))

(define (each #!rest procs)
  (lambda args
    (if (null? procs)
        #!void
        (let loop ((procs procs))
          (if (null? (cdr procs))
              (apply (car procs) args)
              (begin
                (apply (car procs) args)
                (loop (cdr procs))))))))

(define (o #!rest procs)
  (let ((procs (reverse procs)))
    (lambda (arg)
      (if (null? procs)
          arg
          (let loop ((procs (cdr procs))
                     (arg ((car procs) arg)))
            (if (null? procs)
                arg
                (loop (cdr procs) ((car procs) arg))))))))
;; Searching

(define (vector-binary-search vec proc)
  (let loop ((low 0)
             (high (- (vector-length vec) 1)))
    (if (<= low high)
        (let* ((i (div (+ low high) 2))
               (c (proc (vector-ref vec i))))
          (cond
           ((= c 0) i)
           ((< c 0) (loop low (- i 1)))
           (else (loop (+ i 1) high))))
        #f)))

(define (binary-search seq proc)
  (if (vector? seq)
      (vector-binary-search seq proc)
      (error "binary-search sequence must be a vector")))

  
