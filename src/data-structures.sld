;; chicken data-structures library functions -*- scheme -*-
(define-library (data-structures)
  (export
   alist-ref alist-update alist-update! rassoc
   atom? butlast chop compress flatten intersperse join tail?

   list->queue make-queue queue? queue-length queue->list
   queue-add! queue-empty? queue-first queue-last queue-remove!
   queue-push-back! queue-push-back-list!

   merge merge! sort sort! sorted? topological-sort

   conc ->string string-chop
   string-chomp string-compare3 string-compare3-ci string-intersperse
   string-split string-translate string-translate* substring=?
   substring-ci=? substring-index substring-index-ci
   reverse-string-append

   any? constantly complement conjoin disjoin list-of?
   compose o each flip identity

   binary-search)

  (import (rename (kawa base)
                  (string-split kawa-string-split)
                  (string-index kawa-string-index)
                  (string-join string-intersperse)
                  (string-concatenate-reverse reverse-string-append))
          (kawa regex)
          (rename (srfi 1)
                  (not-pair? atom?))
          (only (srfi 13)
                string-index
                string-contains-ci)
          (srfi 14)
          (srfi 117)
          (rename (topological-sort)
                  (tsort topological-sort))
          (srfi 132)
          (srfi 128)
          (rename (queue)
                  (enqueue! queue-add!)
                  (queue-pop! queue-remove!)
                  (queue-push! queue-push-back!)
                  (queue-front queue-first)
                  (queue-rear queue-last))
          (rename (collections)
                  (coll-make-array-deque list->queue)
                  (coll->list queue->list)
                  (coll-size queue-length)))
  (include "data-structures-impl.scm"))
