;;; Interface to java.util.Optional -*- scheme -*-

(define-library (optional)
  (export
   make-optional make-empty-optional
   optional? optional-set?

   optional-ref optional-ref/default optional-ref/get
   optional-filter optional-call optional-map optional-flat-map 

   optional=? optional-hash
   )
  (import (kawa base))

  (include "optional-impl.scm"))
