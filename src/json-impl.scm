;;; Implements RFC-8259 flavor JSON

(define-record-type json (make-json jval)
  json?
  (jval %json-ref))

(define (json-bool? j::json) ::boolean
 (boolean? (%json-ref j)))

(define (json-null? j::json) ::boolean
  (java-null? (%json-ref j)))

(define (json-number? j::json) ::boolean
  (real? (%json-ref j)))

(define (json-string? j::json) ::boolean
  (string? (%json-ref j)))

(define (json-array? j::json) ::boolean
  (java.util.List? (%json-ref j)))

(define (json-object? j::json) ::boolean
  (java.util.Map? (%json-ref j)))

(define (json-ref j::json #!optional (index #f))
  (cond
   ((json-array? j)
    (cond
     ((integer? index)
      (coll-list-ref (%json-ref j) index))
      ((eq? index #f)
       (%json-ref j))
      (else
       (error "Invalid array index" index))))
   ((json-object? j)
    (cond
     ((string? index)
      (let ((v (coll-map-ref (%json-ref j) index)))
        (if (java-null? v) #f v)))
     ((eq? index #f)
      (%json-ref j))
     (else
      (error "Invalid object key" index))))
   (else
    (%json-ref j))))

(define (json-array-length j::json) ::int
  (if (json-array? j)
      (coll-size (%json-ref j))
      (error "Not a JSON array")))

(define json-null (make-json #!null))
(define json-true (make-json #t))
(define json-false (make-json #f))

;; Skip leading whitespace characters. Returns true if the next
;; character read will not be EOF
(define json-whitespace-chars (char-set #\space #\tab #\linefeed #\return))
(define (skip-whitespace port)
  (let ((next (peek-char port)))
    (cond
     ((eof-object? next) #f)
     ((char-set-contains? json-whitespace-chars next)
      (read-char port)
      (skip-whitespace port))
     (else #t))))

;; Read a character or raise an error at EOF.
(define (must-read-char port)
  (let ((c (read-char port)))
    (if (eof-object? c)
        (error "Unexpected end of file!")
        c)))

;; Peek a character or raise an error at EOF.
(define (must-peek-char port)
  (let ((c (peek-char port)))
    (if (eof-object? c)
        (error "Unexpected end of file!")
        c)))

;; Peek a character or return false at EOF.
(define (try-peek-char port)
  (let ((c (peek-char port)))
    (if (eof-object? c)
        #f
        c)))

;; Read this exact character or a member of a charset or raise an error.
(define (must-read-this-char port wanted)
  (let ((c (must-read-char port)))
    (if
     (or (and (char? wanted) (char=? c wanted))
         (and (char-set? wanted) (char-set-contains? wanted c)))
     c
     (error (format "Expected to read ~A, saw ~A instead!"
                    wanted c)))))

;; Read this exact character or a member of a charset, or return false.
(define (try-read-this-char port wanted)
  (let ((c (try-peek-char port)))
    (if
     (and c
          (or (and (char? wanted) (char=? c wanted))
              (and (char-set? wanted) (char-set-contains? wanted c))))
     (begin
       (read-char port)
       c)
     #f)))

;; Read and match the exact characters from a list and return val or
;; raise an error.
(define (read-literal port chars val)
  (if (null? chars)
      val
      (begin
        (must-read-this-char port (car chars))
        (read-literal port (cdr chars) val))))

;; Handle true, false and null
(define (parse-literal port)
  (let ((first (read-char port)))
    (cond
     ((char=? first #\t)
      (read-literal port '(#\r #\u #\e) #t))
     ((char=? first #\f)
      (read-literal port '(#\a #\l #\s #\e) #f))
     ((char=? first #\n)
      (read-literal port '(#\u #\l #\l) #!null))
     (else
      (error "Unexpected character in literal: " first)))))

;; Try to read a single 0 digit, or return false if it can't.
(define (maybe-read-zero port) (try-read-this-char port #\0))

(define json-digits (char-set #\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9))

;; Try to read a single digit or return false if it can't.
(define (try-parse-digit port)
  (let ((c (peek-char port)))
    (cond
     ((eof-object? c) #f)
     ((char-set-contains? json-digits c)
      (read-char port))
     (else #f))))

;; Read a single digit or error
(define (must-parse-digit port)
  (let ((c (must-peek-char port)))
      (if (char-set-contains? json-digits c)
            (read-char port)
          #f)))

;; Read a digit from 1 to 9.
(define (parse-digit1-9 port)
  (let ((d (must-parse-digit port)))
    (if (and d (not (char=? d #\0)))
        d
        (error "Expected to read 1-9!"))))

;; Read 0 or more digits.
(define (parse-digits* port str)
  (let ((d (try-parse-digit port)))
    (if d
        (begin
          (string-append! str d)
          (parse-digits* port str))
        str)))

;; Read 1 or more digits.
(define (parse-digits+ port)
  (let ((d (must-parse-digit port)))
    (if d
        (parse-digits* port (make-string 1 d))
        (error "Invalid digit!"))))

;; Read the int part of a number.
(define (parse-int port)
  (if (maybe-read-zero port)
      "0"
      (parse-digits* port (make-string 1 (parse-digit1-9 port)))))

;; Read json numbers
(define json-e (char-set #\e #\E))
(define json-+- (char-set #\+ #\-))
(define (parse-number port)
  (let ((str (make-string)))
    (when (try-read-this-char port #\-)
          (string-append! str #\-))
    (string-append! str (parse-int port))
    (when (try-read-this-char port #\.)
          (let ((frac (parse-digits+ port)))
            (string-append! str #\. frac)))
    (when (try-read-this-char port json-e)
          (let ((sign (try-read-this-char port json-+-)))
            (let ((exp (parse-digits+ port)))
              (string-append! str #\e)
              (when sign
                    (string-append! str sign))
              (string-append! str exp))))
    (let ((num (string->number str)))
      (if (and (real? num) (finite? num))
          num
          (error "Number unrepresentable!" str)))))

(define (parse-unicode-escape port)
  (let ((hexdigits (make-string 4)))
    (string-set! hexdigits 0 (must-read-char port))
    (string-set! hexdigits 1 (must-read-char port))
    (string-set! hexdigits 2 (must-read-char port))
    (string-set! hexdigits 3 (must-read-char port))
    (let ((i (string->number hexdigits 16)))
      (if i
          i
          (error (format "Invalid Unicode escape \\u~A" hexdigits))))))

(define json-literal-escape (char-set #\" #\\ #\/))
(define (parse-escaped port)
  (read-char port) ;;(must-read-this-char port #\\)
  (let ((c (must-read-char port)))
    (cond
     ((char-set-contains? json-literal-escape c) c)
     ((char=? c #\b) #\backspace)
     ((char=? c #\f) #\page)
     ((char=? c #\n) #\newline)
     ((char=? c #\r) #\return)
     ((char=? c #\t) #\tab)
     ((char=? c #\u)
      (let ((u (parse-unicode-escape port)))
        (cond
         ((and (>= u #xD800) (<= u #xDBFF))
          (must-read-this-char port #\\)
          (must-read-this-char port #\u)
          (let ((u2 (parse-unicode-escape port)))
            (if (and (>= u2 #xDC00) (<= u2 #xDFFF))
                (integer->char
                 (+ (* (- u #xD800) #x400)
                    (- u2 #xDC00)
                    #x10000))
                (error "Invalid surrogate pair" u u2))))
         ((and (>= u #xDC00) (<= u #xDFFF))
          (error "Low surrogate without high" u))
         (else
          (integer->char u)))))
     (else
      (error "Invalid escaped character" c)))))

(define (parse-chars* port accum)
  (let* ((c (must-peek-char port))
         (ci (char->integer c)))
    (cond
     ((char=? c #\") accum)
     ((char=? c #\\)
      (string-append! accum (parse-escaped port))
      (parse-chars* port accum))
     ((or
       (= ci #x20)
       (= ci #x21)
       (and (>= ci #x23) (<= ci #x5B))
       (and (>= ci #x5D) (<= ci #x10FFFF)))
      (string-append! accum (read-char port))
      (parse-chars* port accum))
     (else
      (error "Invalid character" c)))))

(define (parse-string port)
  (must-read-this-char port #\")
  (let ((s (make-string 0)))
    (parse-chars* port s)   
    (must-read-this-char port #\")
    s))

(define (parse-array port)
  (must-read-this-char port #\[)
  (skip-whitespace port)
  (let ((a (coll-array-list)))
    (let loop ((next (must-peek-char port)))
      (if (char=? next #\])
          (begin
            (read-char port)
            a)
          (begin
            (coll-add! a (parse-value port))
            (skip-whitespace port)
            (let ((c2 (must-peek-char port)))
              (cond
               ((char=? c2 #\,)
                (read-char port)
                (skip-whitespace port)
                (let ((c3 (must-peek-char port)))
                  (if (char=? c3 #\])
                      (error "Trailing comma at end of array!")
                      (loop c3))))
               ((char=? c2 #\])
                (loop c2))
               (else
                (error (format "Unexpected character ~A, was expecting , or ]!"
                               c2))))))))))

(define (parse-object port)
  (must-read-this-char port #\{)
  (skip-whitespace port)
  (let ((o (coll-hash-map)))
    (let loop ((next (must-peek-char port)))
      (cond
       ((char=? next #\})
        (read-char port)
        o)
       ((char=? next #\")
        (let ((key (parse-string port)))
          (skip-whitespace port)
          (must-read-this-char port #\:)
          (skip-whitespace port)
          (coll-map-put! o key (parse-value port))
          (skip-whitespace port)
          (let ((c2 (must-peek-char port)))
            (cond
             ((char=? c2 #\,)
              (read-char port)
              (skip-whitespace port)
              (let ((c3 (must-peek-char port)))
                (if (char=? c3 #\})
                    (error "Trailing comma at end of object!")
                    (loop c3))))
             ((char=? c2 #\})
              (loop c2))
             (else
              (error (format "Unexpected character ~A, was expecting , or }!"
                             c2)))))))
       (else
        (error (format "Unexpected character ~A, was expecting \" or }!" next)))))))

(define json-literal-start (char-set #\t #\f #\n))
(define json-digits- (char-set-adjoin json-digits #\-))
(define (parse-value port)
  (let ((c (must-peek-char port)))
    (cond
     ((char-set-contains? json-literal-start c)
      (make-json (parse-literal port)))
     ((char-set-contains? json-digits- c)
      (make-json (parse-number port)))
     ((char=? c #\")
      (make-json (gnu.lists.IString (parse-string port))))
     ((char=? c #\{)
      (make-json (coll->immutable (parse-object port))))
     ((char=? c #\[)
      (make-json (coll->immutable (parse-array port))))
     (else
      (error "Unexpected character " c)))))

(define (read-json #!optional
                   (port (current-input-port))
                   (force-one-value #f))
  (if (skip-whitespace port)
      (let ((json (parse-value port)))
        (when force-one-value
              (when (skip-whitespace port)
                    (error "Extra data in file!")))
        json)
      (if force-one-value
          (error "Empty file!")
          (eof-object))))

(define (string->json s::string)
  (call-with-input-string s read-json))
(define (bytevector->json u::u8vector)
  (call-with-encoded-input-bytevector u 'utf-8 read-json))

(define (json->native j::json)
  (cond
   ((json-object? j)
    (map! (lambda (elem)
            (cons (car elem) (json->native (cdr elem))))
          (coll-map->alist (%json-ref j))))
   ((json-string? j) (%json-ref j))
   ((json-array? j)
    (vector-map json->native (coll->vector (%json-ref j))))
   ((json-null? j)
    'null)
   (else
    (%json-ref j))))

(define (native->json obj) ::json
  (cond
   ((or (java-null? obj) (eq? obj 'null)) json-null)
   ((eq? obj #t) json-true)
   ((eq? obj #f) json-false)
   ((string? obj) (make-json obj))
   ((real? obj) (make-json obj))
   ((vector? obj)
    (make-json (coll-make-array-list (vector->listmap native->json obj))))
   ((list? obj)
    (make-json
     (coll-make-hash-map
      (map
       (lambda (elem)
         (if (string? (car elem))
             (cons (car elem) (native->json (cdr elem)))
             (error "Invalid object key" (car elem))))
       obj))))
   (else
    (error "Invalid JSON value" obj))))

(define (write-json-number num::real port)
  (if (integer? num)
      (write num port)
      (format port "~G" num)))

(define (write-json-string str::string port)
  (write-char #\" port)
  (string-for-each (lambda (c)
                     (case c
                       ((#\" #\\ #\/)
                        (write-char #\\ port)
                        (write-char c port))
                       ((#\tab) (display "\\t" port))
                       ((#\newline) (display "\\n" port))
                       ((#\return) (display "\\r" port))
                       ((#\page) (display "\\f" port))
                       ((#\backspace) (display "\\b" port))
                       (else
                        (write-char c port))))
                   str)
  (write-char #\" port))

(define (write-tabs n port)
  (display (make-string n #\tab) port))

(define (write-json-list arr::java.util.List port tw)
  (let ((it (arr:iterator)))
    (when (iter-has-next? it)
          (let loop ()
            (write-json (iter-next! it) port tw)
            (when (iter-has-next? it)
                  (display ", " port)
                  (loop))))))

(define (write-json-array arr::java.util.List port tw)
  (display "[ " port)
  (write-json-list arr port (+ tw 1))
  (display " ]" port))

(define (write-json-map obj::java.util.Map port tw)
  (let* ((entries (coll-map-entries obj))
         (it (entries:iterator)))
    (if (iter-has-next? it)
        (let loop ((entry (iter-next! it)))
          (write-tabs tw port)
          (write-json-string (%json-ref (coll-map-entry-key entry)) port)
          (display ": " port)
          (write-json (coll-map-entry-value entry) port (+ tw 1))
          (when (iter-has-next? it)
                (write-char #\, port)
                (newline port)
                (loop (iter-next! it)))))))

(define (write-json-object obj::java.util.Map port tw)
  (when (> tw 0)
        (newline port)
        (write-tabs tw port))
  (write-char #\{ port)
  (newline port)
  (write-json-map obj port (+ tw 1))
  (newline port)
  (write-tabs tw port)
  (write-char #\} port)
  (newline port))

(define (write-json j::json #!optional (port (current-output-port)) (tw 0))
  (cond
   ((json-string? j) (write-json-string (%json-ref j) port))
   ((json-number? j) (write-json-number (%json-ref j) port))
   ((json-bool? j)
    (if (%json-ref j)
        (display "true" port)
        (display "false" port)))
   ((json-null? j)
    (display "null" port))
   ((json-array? j)
    (write-json-array (%json-ref j) port tw))
   ((json-object? j)
    (write-json-object (%json-ref j) port tw))
   (else
    (error "Unknown json type!" j))))
  
(define (json->string j::json) ::string
  (call-with-output-string (cut write-json j <>)))

(define (json->bytevector j::json) ::u8vector
  (call-with-encoded-output-bytevector 'utf-8 (cut write-json j <>)))
