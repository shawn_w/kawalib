# Makefile for kawalib.

# Assume that java and kawa are in your path.

MODULE_SOURCES=bitset.sld collections.sld data-structures.sld		\
digest.sld endian.sld ideque.sld intset.sld json.sld			\
let-optionals.sld list-queues.sld list-utils.sld lseqs.sld		\
marshal.sld optional.sld priority-queue.sld queue.sld sets.sld		\
stack.sld topological-sort.sld unicode.sld uuid.sld uvector-utils.sld	\
vectors.sld

MODULE_SOURCES2=factor.scm io-utils.scm srfi/srfi-27.scm permutations.scm

RNRS_SOURCES=rnrs/arithmetic/fixnums.sld rnrs/arithmetic/flonums.sld	\
rnrs/enums.sld rnrs/files.sld

SRFI_SOURCES=srfi/srfi-66.sld srfi/srfi-74.sld srfi/srfi-106.sld	\
srfi/srfi-111.sld srfi/srfi-116.sld srfi/srfi-117.sld			\
srfi/srfi-124.sld srfi/srfi-126.sld srfi/srfi-127.sld			\
srfi/srfi-128.sld srfi/srfi-129.sld srfi/srfi-132.sld			\
srfi/srfi-133.sld srfi/srfi-141.sld srfi/srfi-143.sld			\
srfi/srfi-144.sld srfi/srfi-145.sld srfi/srfi-151.sld			\
srfi/srfi-158.sld srfi/srfi-189.sld srfi/srfi-194.sld

MODULE_CLASSES=$(patsubst %.sld, %.class, $(MODULE_SOURCES))
MODULE_CLASSES2=$(patsubst %.scm, %.class, $(MODULE_SOURCES2))
RNRS_CLASSES=$(patsubst %.sld, %.class, $(RNRS_SOURCES))
SRFI_CLASSES=$(patsubst %.sld, %.class, $(patsubst srfi-,, $(SRFI_SOURCES)))

.SUFFIXES: .scm .class .sld

.PHONY: all

all: $(MODULE_CLASSES) $(MODULE_CLASSES2) $(RNRS_CLASSES) $(SRFI_CLASSES)

%.class: %.scm
	kawa -C $<

%.class: %.sld
	kawa -C $<

srfi/%.class: srfi/srfi-%.sld
	kawa -C $<

srfi/%.class: srfi/srfi-%.scm
	kawa -C $<

# Dependencies

bitset.sld: bitset-impl.scm
bitset.class: srfi/128.class

collections.sld: collections-impl.scm
collections.class: srfi/158.class

data-structures.sld: data-structures-impl.scm
data-structures.class: topological-sort.class srfi/117.class srfi/128.class \
	srfi/132.class queue.class collections.class

digest.sld: digest-impl.scm

endian.class: data-structures.class srfi/74.class

ideque.sld: ideque-impl.scm
ideque.class: srfi/158.class

intset.sld: intset-impl.scm
intset.class: srfi/133.class

io-utils.class: data-structures.class

json.sld: json-impl.scm
json.class: io-utils.class list-utils.class collections.class

list-queues.sld: list-queues-impl.scm

list-utils.sld: list-utils-impl.scm
list-utils.class: srfi/117.class

lseqs.sld: lseqs-impl.scm

optional.sld: optional-impl.scm

permutations.class: srfi/117.class srfi/27.class srfi/132.class

priority-queue.class: srfi/128.class srfi/133.class

queue.class: collections.class

sets.sld: sets-impl.scm
sets.class: srfi/128.class srfi/126.class collections.class

stack.class: collections.class

topological-sort.sld: topological-sort-impl.scm
topological-sort.class: srfi/117.class queue.class srfi/126.class

unicode.sld: unicode-impl.scm
unicode.class: collections.class srfi/117.class list-utils.class \
	srfi/128.class

uuid.sld: uuid-impl.scm
uuid.class: srfi/128.class digest.class uvector-utils.class srfi/74.class \
	srfi/27.class collections.class

uvector-utils.sld: uvec-utils-impl.scm
uvector-utils.class: srfi/128.class

vectors.sld: vectors-impl.scm
vectors.class: let-optionals.class

rnrs/enums.sld: rnrs/enums-impl.scm
rnrs/enums.class: sets.class srfi/128.class

rnrs/arithmetic/fixnums.sld: rnrs/arithmetic/fixnums-impl.scm
rnrs/arithmetic/flonums.sld: rnrs/arithmetic/flonums-impl.scm

srfi/27.class: srfi/74.class marshal.class
srfi/68.class: uvector-utils.class
srfi/74.class: srfi/66.class
srfi/srfi-106.sld: srfi/srfi-106-impl.scm
srfi/113.class: sets.class
srfi/srfi-116.sld: srfi/ilists-base.scm srfi/ilists-impl.scm
srfi/117.class: list-queues.class
srfi/srfi-126.sld: srfi/126.body.scm
srfi/127.class: lseqs.class
srfi/132.class: srfi/27.class
srfi/133.class: vectors.class
srfi/143.class: rnrs/arithmetic/fixnums.class
srfi/srfi-144.sld: srfi/srfi-144-impl.scm
srfi/144.class: rnrs/arithmetic/flonums.class
srfi/srfi-158.sld: srfi/srfi-158-impl.scm
srfi/srfi-189.sld: srfi/srfi-189-impl.scm
srfi/189.class: srfi/145.class
srfi/srfi-194.sld: srfi/194-impl.scm srfi/zipf-zri.scm srfi/sphere.scm
srif/srfi-194.class: srfi/27.class srfi/133.class srfi/158.class
