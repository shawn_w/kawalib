(define hexdigits "0123456789abcdef")
(define (bytearray->hexstring bv::byte[])
  (let ((res (make-string (* bv:length 2) #\0)))
    (do ((i 0 (+ i 2))
         (n 0 (+ n 1)))
        ((= n bv:length) res)
      (let ((b (bitwise-and (bv n) #xFF)))
        (string-set! res i
                     (string-ref hexdigits
                                  (bitwise-arithmetic-shift-right b 4)))
        (string-set! res (+ i 1)
                     (string-ref hexdigits (bitwise-and b #xF)))))))

(define-alias md java.security.MessageDigest)

(define (make-digest algo) :: md
  (cond
   ((symbol? algo)
    (md:getInstance (symbol->string algo)))
   ((string? algo)
    (md:getInstance algo))
   (else
    (error "invalid argument" algo))))

(define digest-update!
  (make-procedure
   (lambda (dgst::md bv::u8vector offset::int len::int)
     (dgst:update (bv:getBuffer) offset len))
   (lambda (dgst::md bv::u8vector)
     (dgst:update (bv:getBuffer) 0 (u8vector-length bv)))
   (lambda (dgst::md s::string)
     (let ((v (string->utf8 s)))       
       (dgst:update (v:getBuffer) 0 (u8vector-length s))))))

(define digest->string!
  (make-procedure
   (lambda (dgst::md) :: string
           (bytearray->hexstring (dgst:digest)))
   (lambda (dgst::md bv::u8vector) :: string
           (digest-update! dgst bv)
           (bytearray->hexstring (dgst:digest)))
   (lambda (dgst::md s::string) :: string
           (digest-update! dgst s)
           (bytearray->hexstring (dgst:digest)))))

(define digest->bytevector!
  (make-procedure
   (lambda (dgst::md) :: u8vector
           (make <u8vector> (dgst:digest)))
   (lambda (dgst::md bv::u8vector) :: u8vector
           (digest-update! dgst bv)
           (make <u8vector> (dgst:digest)))
   (lambda (dgst::md s::string) :: u8vector
           (digest-update! dgst s)
           (make <u8vector> (dgst:digest)))))
   
(define (digest-reset! dgst::md)
  (dgst:reset))

(define (digest-name dgst::md) :: string
  (dgst:getAlgorithm))

(define (digest-length dgst::md) :: int
  (dgst:getDigestLength))

(define-syntax %make-digest%
  (syntax-rules ()
    ((_ name algo)
     (define name
       (make-procedure
        (lambda (bv::u8vector) :: string
          (digest->string! (make-digest algo) bv))
        (lambda (s::string) :: string
          (digest->string! (make-digest algo) s)))))))

(%make-digest% md2 "MD2")
(%make-digest% md5 "MD5")
(%make-digest% sha-1 "SHA-1")
(%make-digest% sha-224 "SHA-224")
(%make-digest% sha-256 "SHA-256")
(%make-digest% sha-384 "SHA-384")
(%make-digest% sha-512 "SHA-512")


