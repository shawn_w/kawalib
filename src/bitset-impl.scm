(define-alias bitset java.util.BitSet)

(define make-bitset
  (make-procedure
   (lambda () ::bitset (bitset))
   (lambda (size::int) ::bitset (bitset size))))
(define (bitset? bs) ::boolean (instance? bs bitset))
(define bitset-copy
  (make-procedure
   (lambda (bs::bitset) ::bitset (bs:clone))
   (lambda (bs::bitset from::int to::int) ::bitset
           (bs:get from to))))
(define (bitset-ref bs::bitset i::int) ::boolean (bs:get i))
(define bitset-set!
  (make-procedure
   (lambda (bs::bitset i::int v::boolean)
     (bs:set i v))
   (lambda (bs::bitset i::int)
     (bs:set i))))
(define bitset-fill!
  (make-procedure
   (lambda (bs::bitset from::int to::int val::boolean)
     (bs:set from to val))
   (lambda (bs::bitset from::int to::int)
     (bs:set from to))))
(define bitset-clear!
  (make-procedure
   (lambda (bs::bitset) (bs:clear))
   (lambda (bs::bitset i::int) (bs:clear i))
   (lambda (bs::bitset from::int to::int) (bs:clear from to))))
(define (bitset-empty? bs::bitset) ::boolean (bs:isEmpty))
(define (bitset-count bs::bitset) ::int (bs:cardinality))
(define (bitset-length bs::bitset) ::int (bs:length))
(define (bitset=? bs1::bitset bs2::bitset) ::boolean
  (bs1:equals bs2))
(define (bitset-hash bs::bitset) ::int (bs:hashCode))

(define bitset-not!
  (make-procedure
   (lambda (bs::bitset) (bs:flip 0 (bs:length)))
   (lambda (bs::bitset i::int) (bs:flip i))
   (lambda (bs::bitset from::int to::int) (bs:flip from to))))
(define (bitset-and! bs1::bitset bs2::bitset)
  (bs1:and bs2))
(define (bitset-andnot! bs1::bitset bs2::bitset)
  (bs1:andNot bs2))
(define (bitset-ior! bs1::bitset bs2::bitset)
  (bs1:or bs2))
(define (bitset-xor! bs1::bitset bs2::bitset)
  (bs1:xor bs2))

(define (bitset-intersects? bs1::bitset bs2::bitset) ::boolean
  (bs1:intersects bs2))

(define (bitset->bytevector bs::bitset) ::u8vector
  (make <u8vector> (bs:toByteArray)))
(define (bytevector->bitset bv::u8vector) ::bitset
  (bitset:valueOf (java.nio.ByteBuffer:wrap
                   (bv:getBuffer) 0 (u8vector-length bv))))

(define bitset-first-set-bit
  (make-procedure
   (lambda (bs::bitset) ::int (bs:nextSetBit 0))
   (lambda (bs::bitset i::int) ::int (bs:nextSetBit i))))

(define bitset-first-clear-bit
  (make-procedure
   (lambda (bs::bitset) ::int (bs:nextClearBit 0))
   (lambda (bs::bitset i::int) ::int (bs:nextClearBit i))))

(define bitset-last-set-bit
  (make-procedure
   (lambda (bs::bitset) ::int (bs:previousSetBit (bs:length)))
   (lambda (bs::bitset i::int) ::int (bs:previousSetBit i))))

(define bitset-last-clear-bit
  (make-procedure
   (lambda (bs::bitset) ::int (bs:previousClearBit (bs:length)))
   (lambda (bs::bitset i::int) ::int (bs:previousClearBit i))))

(define bitset-comparator (make-comparator bitset? bitset=? #f bitset-hash))

