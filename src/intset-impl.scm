(define-record-type inode (make-inode minval maxval left right)
  intset?
  (minval inode-minval)
  (maxval inode-maxval)
  (left inode-left)
  (right inode-right))

(define %empty-intset (make-inode #f #f #f #f))

(define (empty-intset) ::inode %empty-intset)
(define (intset-empty? i::inode) ::boolean
  (eq? i %empty-intset))

(define (range->intset min::integer max::integer) ::inode
  (if (< min max)
      (make-inode min max %empty-intset %empty-intset)
      (error "Invalid range")))

(define (intset-contains? i::inode n::integer) ::boolean
  (cond
   ((intset-empty? i)
    #f)
   ((and (>= n (inode-minval i)) (<= n (inode-maxval i)))
    #t)
   ((< n (inode-minval i))
    (intset-contains? (inode-left i) n))
   (else
    (intset-contains? (inode-right i) n))))
      
(define (%split-max i::inode)
  (if (intset-empty? (inode-right i))
      (values (inode-minval i) (inode-maxval i) (inode-left i))
      (let-values (((u v r) (%split-max (inode-right i))))
        (values u v (make-inode (inode-minval i) (inode-maxval i)
                                (inode-left i) r)))))

(define (%join-left i::inode) ::inode
  (if (intset-empty? (inode-left i))
      i
      (let-values (((x y l) (%split-max (inode-left i))))
        (if (= (+ y 1) (inode-minval i))
            (make-inode x (inode-maxval i) l (inode-right i))
            i))))

(define (%split-min i::inode)
  (if (intset-empty? (inode-left i))
      (values (inode-minval i) (inode-maxval i) (inode-right i))
      (let-values (((u v l) (%split-min (inode-left i))))
        (values u v (make-inode (inode-minval i) (inode-maxval i)
                                l (inode-right i))))))

(define (%join-right i::inode) ::inode
  (if (intset-empty? (inode-right i))
      i
      (let-values (((x y r) (%split-min (inode-right i))))
        (if (= (+ (inode-maxval i) 1) x)
            (make-inode (inode-minval i) y (inode-left i) r)
            i))))

(define (intset-add i::inode n::integer) ::inode
  (cond
   ((intset-empty? i)
    (make-inode n n %empty-intset %empty-intset))
   ((< n (inode-minval i))
    (if (= (+ n 1) (inode-minval i))
        (%join-left (make-inode n (inode-maxval i)
                               (inode-left i) (inode-right i)))
        (make-inode (inode-minval i) (inode-maxval i)
                    (intset-add (inode-left i) n) (inode-right i))))
   ((> n (inode-maxval i))
    (if (= n (+ (inode-maxval i) 1))
        (%join-right (make-inode (inode-minval i) n
                                (inode-left i) (inode-right i)))
        (make-inode (inode-minval i) (inode-maxval i)
                    (inode-left i) (intset-add (inode-right i) n))))
   (else i)))

(define (%merge l::inode r::inode) ::inode
  (cond
   ((intset-empty? l) r)
   ((intset-empty? r) l)
   (else
    (let-values (((x y lp) (%split-max l)))
      (make-inode x y lp r)))))

(define (intset-delete i::inode n::integer) ::inode
  (cond
   ((intset-empty? i) i)
   ((< n (inode-minval i))
    (make-inode (inode-minval i) (inode-maxval i)
                (intset-delete (inode-left i) n)
                (inode-right i)))
   ((> n (inode-maxval i))
       (make-inode (inode-minval i) (inode-maxval i)
                   (inode-left i) (intset-delete (inode-right i) n)))
   ((= n (inode-minval i))
    (if (= n (inode-maxval i))
        (%merge (inode-left i) (inode-right i))
        (make-inode (+ (inode-minval i) 1) (inode-maxval i)
                    (inode-left i) (inode-right i))))
   ((= n (inode-maxval i))
    (make-inode (inode-minval i) (- (inode-maxval i) i)
                (inode-left i) (inode-right i)))
   (else
    (make-inode (inode-minval i) (- n 1)
                (inode-left i)
                (make-inode (+ n 1) (inode-maxval i)
                            %empty-intset (inode-right i))))))
    
(define (vector->intset vec) ::inode
  (vector-fold intset-add %empty-intset vec))

(define (list->intset lst) ::inode
  (fold (lambda (n i) (intset-add i n)) %empty-intset lst))

(define (intset-minimum i::inode)
  (cond
   ((intset-empty? i)
    #f)
   ((intset-empty? (inode-left i))
    (inode-minval i))
   (else
    (intset-minimum (inode-left i)))))

(define (intset-maximum i::inode)
  (cond
   ((intset-empty? i)
    #f)
   ((intset-empty? (inode-right i))
    (inode-maxval i))
   (else
    (intset-maximum (inode-right i)))))

(define (intset-before i::inode n::integer) ::inode
  (cond
   ((intset-empty? i) i)
   ((<= n (inode-minval i))
    (intset-before (inode-left i) n))
   ((< n (inode-maxval i))
    (make-inode (inode-minval i) (- n 1)
                (inode-left i) %empty-intset))
   (else
    (make-inode (inode-minval i) (inode-maxval i)
                (inode-left i)
                (intset-before (inode-right i) n)))))

(define (intset-after i::inode n::integer) ::inode
  (cond
   ((intset-empty? i) i)
   ((<= n (inode-minval i))
    (make-inode (inode-minval i) (inode-maxval i)
                (intset-after (inode-left i) n)
                (inode-right i)))
   ((< n (inode-maxval i))
    (make-inode (+ n 1) (inode-maxval i)
                %empty-intset
                (inode-right i)))
   (else
    (intset-after (inode-right i) n))))

(define (intset-within-range i::inode min::integer max::integer) ::inode
  (intset-before (intset-after i min) max))

(define (%cons-range min::integer max::integer lst)
  (if (= min max)
      (cons max lst)
      (%cons-range min (- max 1) (cons max lst))))

(define (%intset->list i lst)
  (if
   (intset-empty? i)
   lst
   (%intset->list (inode-left i)
                  (%cons-range (inode-minval i) (inode-maxval i)
                               (%intset->list (inode-right i) lst)))))
(define (intset->list i::inode)
  (%intset->list i '()))
  
(define (intset->alist i::inode)
  (if (intset-empty? i)
      #f
      (list (cons 'minval (inode-minval i))
            (cons 'maxval (inode-maxval i))
            (cons 'left (intset->alist (inode-left i)))
            (cons 'right (intset->alist (inode-right i))))))

(define (alist->intset a)
  (if (null? a)
      %empty-intset
      (make-inode (cdr (assq 'minval a)) (cdr (assq 'maxval a))
                  (alist->intset (cdr (assq 'left a)))
                  (alist->intset (cdr (assq 'right a))))))

(define (balance i::inode) ::inode
  (cond
   ((intset-empty? i) i)
   ((and
     (intset-empty? (inode-left i))
     (not (intset-empty? (inode-right i)))
     (intset-empty? (inode-left (inode-right i))))
    (let ((r (inode-right i)))
      (balance (make-inode (inode-minval r) (inode-maxval r)
                  (make-inode (inode-minval i) (inode-maxval i)
                              %empty-intset %empty-intset)
                  (balance (inode-right r))))))
   ((and
     (intset-empty? (inode-right i))
     (not (intset-empty? (inode-left i)))
     (intset-empty? (inode-right (inode-left i))))
    (let ((l (inode-left i)))
      (balance (make-inode (inode-minval l) (inode-maxval l)
                  (balance (inode-left l))
                  (make-inode (inode-minval i) (inode-maxval i)
                              %empty-intset %empty-intset)))))
   (else
    (make-inode (inode-minval i) (inode-maxval i)
                (balance (inode-left i))
                (balance (inode-right i))))))
      
