;;; List extras library. -*- scheme -*-

(define-library (list-utils)
  (export
   string->listmap  string->vectormap
   vector->listmap list->vectormap

   filter-regex filter-regex!
   remove-regex remove-regex!

   minmax)
  (import
   (kawa base)
   (kawa regex)
   (srfi 1)
   (srfi 117)
   )
  (include "list-utils-impl.scm"))
