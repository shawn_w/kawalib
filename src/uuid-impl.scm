(define-alias uuid java.util.UUID)

(define (uuid? obj)
  (instance? obj java.util.UUID))

(define (make-uuid v::u64vector) ::uuid
  (if (= (u64vector-length v) 2)
      (uuid (u64vector-ref v 0) (u64vector-ref v 1))
      (error "make-uuid: vector wrong size, expects 2 elements" v)))

(define (generate-uuid) ::uuid (uuid:randomUUID))

(define make-type3-uuid
  (make-procedure
   (lambda (name::u8vector) ::uuid
           (if (= (name:getBuffer):length (u8vector-length name))
               (uuid:nameUUIDFromBytes (name:getBuffer))
               (let ((raw (byte[] (u8vector-length name))))
                 (java.lang.System:arraycopy (name:getBuffer) 0 raw 0 raw:length)
                 (uuid:nameUUIDFromBytes raw))))
   (lambda (namespace::uuid name::u8vector) :: uuid
           (let ((buf (make-u8vector (+ 16 (u8vector-length name)))))
             (blob-s64-set! (endianness big) buf 0 (namespace:getMostSignificantBits))
             (blob-s64-set! (endianness big) buf 8 (namespace:getLeastSignificantBits))
             (u8vector-copy! buf 16 name 0 (u8vector-length name))
             (if (= (buf:getBuffer):length (u8vector-length buf))
                 (uuid:nameUUIDFromBytes (buf:getBuffer))
                 (let ((raw (byte[] (u8vector-length buf))))
                   (java.lang.System:arraycopy (buf:getBuffer) 0 raw 0 raw:length)
                   (uuid:nameUUIDFromBytes raw)))))))

(define make-type5-uuid
  (make-procedure
   (lambda (name::u8vector) ::uuid
           (let* ((sha1 (make-digest 'sha-1))
                  (raw (digest->bytevector! sha1 name)))
             (u8vector-set! raw 6 (bitwise-ior
                                   (bitwise-and (u8vector-ref raw 6) #x0F)
                                   #x50))
             (u8vector-set! raw 8 (bitwise-ior
                                   (bitwise-and (u8vector-ref raw 8) #x3F)
                                   #x80))
             (uuid (blob-s64-ref (endianness big) raw 0) (blob-s64-ref (endianness big) raw 8))))
   (lambda (namespace::uuid name::u8vector) ::uuid
           (let ((buf (make-u8vector (+ 16 (u8vector-length name)))))
             (blob-s64-set! (endianness big) buf 0 (namespace:getMostSignificantBits))
             (blob-s64-set! (endianness big) buf 8 (namespace:getLeastSignificantBits))
             (u8vector-copy! buf 16 name 0 (u8vector-length name))
             (make-type5-uuid buf)))))

(define (intervals-since-epoch)
  (let* ((now ((java.time.Clock:systemUTC):instant))
         (now-intervals (+ (* (now:getEpochSecond) 10000000)
                           (quotient (now:getNano) 100))))
    (+ now-intervals #x01B21DD213814000)))

(define (get-host-interfaces)
  (let ((interfaces
         (enumeration->list (java.net.NetworkInterface:getNetworkInterfaces))))
    (coll-filter->list (lambda (i::java.net.NetworkInterface)
                         (and (not (i:isLoopback))
                              (i:isUp)
                              (not (eq? (i:getHardwareAddress) #!null))))
                       interfaces)))

(define (mac-address)
  (let ((interfaces (get-host-interfaces)))
    (if (coll-empty? interfaces)
        '#u8(0 0 0 0 0 0)
        (let ((iface ::java.net.NetworkInterface (coll-list-ref interfaces 0)))
          (make <u8vector> (iface:getHardwareAddress))))))

#;(define (mac-address)
  (let* ((ip (java.net.InetAddress:getLocalHost))
         (iface (java.net.NetworkInterface:getByInetAddress ip))
         (mac (iface:getHardwareAddress)))
    (if (eq? mac #!null)
        '#u8(0 0 0 0 0 0)
        (make <u8vector> mac))))

(define clock-seq #f)
(define prev-timestamp #f)
(define prev-node #f)

(define (make-type1-uuid #!optional (node (mac-address)))
  (let ((now (intervals-since-epoch))
        (buf (make-u8vector 16 0)))
    (when (or (not prev-timestamp) (not clock-seq) (not prev-node) (not (u8vector=? prev-node node)))
          (set! clock-seq (random-integer #b100000000000000)))
    (when (and prev-timestamp (> prev-timestamp now))
          (set! clock-seq (remainder (+ clock-seq 1) #b100000000000000)))
    (set! prev-timestamp now)
    (set! prev-node node)
    (blob-u32-set! (endianness big) buf 0 (bitwise-and now #xFFFFFFFF))
    (blob-u16-set! (endianness big) buf 4 (bitwise-and (bitwise-arithmetic-shift-right now 32)
                                                       #xFFFF))
    (blob-u16-set! (endianness big) buf 6 (bitwise-and (bitwise-arithmetic-shift-right now 48)
                                                       #x0FFF))
    (u8vector-set! buf 6 (bitwise-ior (u8vector-ref buf 6) #x10))
    (u8vector-set! buf 8 (bitwise-ior #x80
                                      (bitwise-arithmetic-shift-right
                                       (bitwise-and clock-seq #x3F00)
                                       8)))
    (u8vector-set! buf 9 (bitwise-and clock-seq #xFF))
    (u8vector-copy! buf 10 node 0 6)
    (uuid (blob-s64-ref (endianness big) buf 0) (blob-s64-ref (endianness big) buf 8))))

(define (uuid=? a::uuid b::uuid) ::boolean
  (a:equals b))

(define (uuid-compare a::uuid b::uuid) ::int
  (a:compareTo b))

(define (uuid<? a::uuid b::uuid) ::boolean
  (< (a:compareTo b) 0))

(define (uuid-hash u::uuid) (u:hashCode))

(define uuid-comparator (make-comparator
                         uuid? uuid=? uuid<? uuid-hash))

(define (uuid->string u::uuid) ::string
  (gnu.lists.IString (u:toString)))

(define (string->uuid s::string) ::uuid
  (uuid:fromString s))

(define (uuid-version u::uuid) ::int
  (u:version))

(define (uuid-variant u::uuid) ::int
  (u:variant))

(define (uuid-decompose u::uuid)
  (values (u:timestamp) (u:version) (u:variant) (u:clockSequence) (u:node)))

(define (uuid-split u::uuid) ::u64vector
  (u64vector (u:getMostSignificantBits) (u:getLeastSignificantBits)))

(define uuid-namespace-dns (string->uuid "6ba7b810-9dad-11d1-80b4-00c04fd430c8"))
(define uuid-namespace-url (string->uuid "6ba7b811-9dad-11d1-80b4-00c04fd430c8"))
(define uuid-namespace-oid (string->uuid "6ba7b812-9dad-11d1-80b4-00c04fd430c8"))
(define uuid-namespace-x500 (string->uuid "6ba7b814-9dad-11d1-80b4-00c04fd430c8"))

