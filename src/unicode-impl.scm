
(define-alias breaker java.text.BreakIterator)
(define-alias collator java.text.Collator)

(define (make-locale loc::symbol) ::java.util.Locale
  (java.util.Locale (symbol->string loc)))

(define (locales->list locales::java.lang.Object[])
    (coll-map->list
     (lambda (l::java.util.Locale)
       (let ((lang (l:getLanguage))
             (country (l:getCountry)))
         (if (= (string-length country) 0)
             (string->symbol lang)
             (string->symbol (format "~A_~A" lang country)))))
     (java.util.Arrays:stream locales)))

(define (break-locales)
  (locales->list (breaker:getAvailableLocales)))

(define (collate-locales)
  (locales->list (collator:getAvailableLocales)))

(define (build-breaks b::breaker s::string)
  (let ((breaks (list-queue)))
    (b:setText s)
    (let loop ((start (b:first))
               (end (b:next)))
      (if (or (= start breaker:DONE) (= end breaker:DONE))
          (list-queue-list breaks)
          (begin
            (list-queue-add-back! breaks (cons start end))
            (loop end (b:next)))))))

(define (char-breaker locale)
  (if locale
      (breaker:getCharacterInstance (make-locale locale))
      (breaker:getCharacterInstance)))

(define (word-breaker locale)
  (if locale
      (breaker:getWordInstance (make-locale locale))
      (breaker:getWordInstance)))

(define (sentence-breaker locale)
  (if locale
      (breaker:getSentenceInstance (make-locale locale))
      (breaker:getSentenceInstance)))

(define (string-char-breaks s::string #!optional (locale #f))
  (build-breaks (char-breaker locale) s))

(define (string-word-breaks s::string #!optional (locale #f))
  (build-breaks (word-breaker locale) s))

(define (string-sentence-breaks s::string #!optional (locale #f))
  (build-breaks (sentence-breaker locale) s))

(define (string-line-breaks s::string #!optional (locale #f))
  (map! cdr
        (if locale
            (build-breaks (breaker:getLineInstance (make-locale locale)) s)
            (build-breaks (breaker:getLineInstance) s))))

(define (make-char-break-generator str::string #!optional (locale #f))
  (let ((b (char-breaker locale)))
    (b:setText str)
    (let ((start (b:first))
          (end (b:next)))
      (lambda ()
        (if (or (= start breaker:DONE) (= end breaker:DONE))
            (eof-object)
            (let ((c (substring str start end)))
              (set! start end)
              (set! end (b:next))
              c))))))

(define (breaks->list breaks s::string)
  (map! (lambda (break)
         (substring s (car break) (cdr break))) breaks))

(define (string->characters s::string #!optional (locale #f))
  (breaks->list (string-char-breaks s locale) s))

(define letter (delay (regex "\\p{L}")))

(define (string->words s::string #!optional (locale #f) (strip-space #t))
  (let ((words (breaks->list (string-word-breaks s locale) s)))
    (if strip-space
        (filter-regex! (force letter) words)
        words)))

(define (make-word-break-generator str::string
                              #!optional (locale #f) (strip-space #t))
  (let ((b (word-breaker locale)))
    (b:setText str)
    (let ((start (b:first))
          (end (b:next)))
      (lambda ()
        (let loop ()
          (if (or (= start breaker:DONE) (= end breaker:DONE))
              (eof-object)
              (let ((w (substring str start end)))
                (set! start end)
                (set! end (b:next))
                (if (and strip-space (not (regex-match (force letter) w)))
                    (loop)
                    w))))))))

(define (string->sentences s::string #!optional (locale #f))
  (breaks->list (string-sentence-breaks s locale) s))


(define (make-sentence-break-generator str::string #!optional (locale #f))
  (let ((b (sentence-breaker locale)))
    (b:setText str)
    (let ((start (b:first))
          (end (b:next)))
      (lambda ()
        (if (or (= start breaker:DONE) (= end breaker:DONE))
            (eof-object)
            (let ((s (substring str start end)))
              (set! start end)
              (set! end (b:next))
              s))))))


(define (make-collator #!optional (locale #f)) ::collator
  (if locale
      (collator:getInstance (make-locale locale))
      (collator:getInstance)))

(define (collator? obj) ::boolean (instance? obj java.text.Collator))

(define (collate=? c::collator s1::string s2::string) ::boolean
  (c:equals s1 s2))

(define (collate-compare c::collator s1::string s2::string) ::int
  (c:compare s1 s2))

(define (make-collated-comparator #!optional (locale #f))
  (let ((c (make-collator locale)))
    (make-comparator string=? c:equals
                     (lambda (s1::string s2::string) ::boolean
                             (< (c:compare s1 s2) 0))
                     #f)))

(define-alias ckey java.text.CollationKey)

(define (collator->key c::collator s::string) ::ckey
  (c:getCollationKey s))

(define (collation-key? obj) ::boolean (ckey? obj))

(define (key-compare k1::ckey k2::ckey) ::int
  (k1:compareTo k2))

(define (key-source-string k::ckey) ::string
  (k:getSourceString))

(define (collation-key=? k1::ckey k2::ckey) ::boolean
  (= (k1:compareTo k2) 0))

(define (collation-key-hash k::ckey) ::int (k:hashCode))

(define collation-key-comparator
  (make-comparator collation-key? collation-key=?
                   (lambda (k1::ckey k2::ckey) ::boolean
                           (< (k1:compareTo k2) 0))
                   collation-key-hash))
