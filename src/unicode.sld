;;; Unicode functions -*- scheme -*-

(define-library (unicode)
  (export
   ;; breaks
   break-locales
   string-char-breaks string-word-breaks string-sentence-breaks
   make-char-break-generator make-word-break-generator make-sentence-break-generator
   string-line-breaks
   string->characters string->words string->sentences

   ;; collation
   collate-locales
   make-collator make-collated-comparator
   collator?
   collate=? collate-compare

   collator->key key-compare collation-key? key-source-string
   collation-key=? collation-key-hash
   collation-key-comparator

   )
  (import (kawa base)
          (kawa regex)
          (collections)
          (srfi 1)
          (list-utils)
          (srfi 117)
          (srfi 128))
  (include "unicode-impl.scm"))
