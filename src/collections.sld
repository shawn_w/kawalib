;;; -*- scheme -*-
(define-library (collections)
  (export
   ;; Predicates
   collection? coll-list?
   coll-queue? coll-deque? coll-priority-queue?
   coll-set? coll-sorted-set? coll-navigable-set?
   coll-map? coll-sorted-map? coll-navigable-map?
   coll-stream?
   java-null?

   ;; Collection creation functions
   coll-linked-list coll-make-linked-list
   coll-array-list coll-make-array-list
   coll-array-deque coll-make-array-deque
   coll-priority-queue coll-make-priority-queue
   coll-tree-set coll-make-tree-set
   coll-skiplist-set coll-make-skiplist-set
   coll-hash-set coll-make-hash-set
   coll-linkedhash-set coll-make-linkedhash-set
   coll-tree-map coll-make-tree-map
   coll-skiplist-map coll-make-skiplist-map
   coll-hash-map coll-make-hash-map
   coll-linkedhash-map coll-make-linkedhash-map
   coll->immutable coll-immut-list
   coll-single-list coll-single-set coll-single-map
   coll-empty-list coll-empty-set coll-empty-map
   enumeration->list coll-stream->list coll-array->list ; java.util.List, not scheme list
   coll-array->stream coll->stream ; java stream, not SRFI-41 stream

   ;; Iterator functions
   iterator? iter-has-next? iter-next! iter-remove!
   list-iterator? listiter-has-previous? listiter-previous!
   listiter-next-index listiter-previous-index
   listiter-set!

   ;; Generic collection routines that all types support
   coll-add! coll-add-all! coll-clear!
   coll-contains? coll-contains-all? coll-equals? coll-empty?
   coll-iterator
   coll-remove! coll-remove-all! coll-retain-all!
   coll-size coll->vector coll->list coll-for-each
   coll-count coll-copy coll->generator

   ;; Algorithms
   coll-min coll-max coll-disjoint?
   coll-filter->list coll-map->list coll-fold
   coll-filter->set coll-map->set
   coll-any coll-every

   ;; stream algorithms
   coll-stream-filter coll-stream-for-each coll-stream-drop
   coll-stream-any coll-stream-every
   coll-stream-fold

   ;; SortedSet specific functions (TreeSet, ConcurrentSkipLisSet)
   coll-set-first coll-set-last
   coll-sset-comparator
   coll-set-head-set coll-set-tail-set coll-set-sub-set

   ;; NavigableSet specific functions (All non-hashset sets)
   coll-set-head-seti coll-set-tail-seti coll-set-sub-seti
   coll-set-ceiling coll-set-floor
   coll-set-higher coll-set-lower
   coll-set-poll-first! coll-set-poll-last!
   coll-set-descending-iterator coll-set-descending-set

   ;; List specific functions (ArrayList, LinkedList)
   coll-list-add-at! coll-list-ref coll-list-index coll-list-index-right
   coll-list-remove-at! coll-list-set! coll-list-iterator
   coll-list-copy! coll-list-fill! coll-list-map! coll-list-reverse!
   coll-list-rotate! coll-list-swap! coll-list-replace-all!
   coll-list-sort! coll-list-binary-search

   ;; Queue specific functions (LinkedList, PriorityQueue)
   coll-queue-element coll-queue-offer!
   coll-queue-peek coll-queue-poll! coll-queue-remove!

   ;; Deque specific functions (ArrayDeque, LinkedList)
   coll-deque-first coll-deque-last
   coll-deque-add-first! coll-deque-add-last!
   coll-deque-remove-first! coll-deque-remove-last!
   coll-deque-peek-first coll-deque-peek-last
   coll-deque-offer-first! coll-deque-offer-last!
   coll-deque-poll-first! coll-deque-poll-last!
   coll-deque-remove-first-occurrence!
   coll-deque-remove-last-occurrence!
   coll-deque->stack

   ;; Map specific functions. Note: Maps cannot be used
   ;; with the generic coll functions.

   coll-map-clear! coll-map-compute! coll-map-compute-absent!
   coll-map-compute-present! coll-map-contains? coll-map-for-each
   coll-map-ref coll-map-empty? coll-map-keys coll-map-merge!
   coll-map-put! coll-map-put-absent! coll-map-put-all! coll-map-remove!
   coll-map-replace! coll-map-map! coll-map-size
   coll-map-values coll-map->alist coll-map-keys->list coll-map-values->list
   coll-map-entries coll-map-entry-key coll-map-entry-value

   ;; SortedMap functions (TreeMap, ConcurrentSkipListMap)
   coll-map-first-key coll-map-last-key
   coll-map-head-map coll-map-tail-map coll-map-sub-map

   ;; NavigableMap functions (TreeMap, ConcurrentSkipListMap)
   coll-map-ceiling coll-map-floor coll-map-higher coll-map-lower
   coll-map-descending-keys coll-map-descending-map
   coll-map-head-mapi coll-map-tail-mapi coll-map-sub-mapi

   ;; Utilities for boxing hashed objects with a custom
   ;; comparator.

   make-hash-boxer hash-unbox
   )

  (import
   (kawa base)
   (list-queues)
   (srfi 128)
   (srfi 158)
   )
  (include "collections-impl.scm"))
