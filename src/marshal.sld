;;; Serialize java objects -*- scheme -*-
(module-compile-options warn-unknown-member: #f)
(define-library (marshal)
  (export
   serialize serialized->object serialized->prim
   object->prim bytearray->bytevector univector->array)
  (import (scheme base) (kawa reflect))
  (begin
    ;; Convert a byte array to a bytevector
    (define-syntax bytearray->bytevector
      (syntax-rules ()
        ((_ ba) (make <u8vector> ba))))

    ;; Convert any uniform vector to a java array
    (define-syntax univector->array
      (syntax-rules ()
        ((_ uv) (uv:getBuffer))))

    ;; Try to turn a numeric object into a primitive type
    ;; Consider making this a macro with compile time dispatch?
    (define (object->prim o)
      (cond
       ((java.lang.Byte? o) (as byte o))
       ((gnu.math.UByte? o) (as ubyte o))
       ((java.lang.Short? o) (as short o))
       ((gnu.math.UShort? o) (as ushort o))
       ((java.lang.Integer? o) (as int o))
       ((gnu.math.UInt? o) (as uint o))
       ((java.lang.Long? o) (as long o))
       ((gnu.math.ULong? o) (as ulong o))
       ((java.lang.Float? o) (as float o))
       ((java.lang.Double? o) (as double o))
       ((java.lang.Character? o) (as char o))
       (else o)))

    ;; Some types like IString can't be unserialized. Convert those to
    ;; types that can before serializing them.
    (define (massage o)
      (cond
       ((string? o) (as java.lang.String o))
       (else o)))
    
    ;; Serialize an object to a u8vector
    (define (serialize obj::java.io.Serializable)
      (let* ((obj (massage obj))
             (byte-array-stream (java.io.ByteArrayOutputStream))
             (object-stream (java.io.ObjectOutputStream byte-array-stream)))
        (object-stream:writeObject obj)
        (object-stream:flush)
        (let ((ser
               (bytearray->bytevector (byte-array-stream:toByteArray))))
          (object-stream:close)
          ser)))

    ;; Reconstitue a serialized object.
    (define (serialized->object bv::bytevector)
      (let* ((ba-state (univector->array bv))
             (byte-array-stream (java.io.ByteArrayInputStream ba-state))
             (object-stream (java.io.ObjectInputStream byte-array-stream))
             (unser (object-stream:readObject)))
        (object-stream:close)
        unser))
    
    (define (serialized->prim bv::bytevector)
      (let ((o (serialized->object bv)))
        (object->prim o)))
    ))



                
