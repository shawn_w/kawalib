(define (symbol<? a b)
  (string<? (symbol->string a) (symbol->string b)))
(define symbol-compare (make-comparator symbol? eq? symbol<? symbol-hash))
(define (symbol-set symbols)
  (apply set symbol-compare symbols))

(define (universe-valid-symbols? uni symbols)
  (every (cut set-contains? uni <>) symbols))

(define-record-type enum-set
  (make-enum-set universe symbols)
  enum-set?
  (universe enum-set-universe-ref)
  (symbols enum-set-values)
  )

(define (make-enumeration symbols)
  (if
   (and (list? symbols) (every symbol? symbols))
   (let ((uni (symbol-set symbols)))
     (make-enum-set uni uni))
   (error "Invalid argument to make-enumeration" symbols)))

(define (enum-set-universe es)
  (let ((uni (enum-set-universe-ref es)))
    (make-enum-set uni uni)))

(define (enum-set->list es)
  (set->list (enum-set-values es)))

(define (enum-set-indexer es)
  (unless (enum-set? es) (error "Invalid argument to enum-set-indexer"))
  (let ((uni (enum-set-universe-ref es)))
    (lambda (sym) (list-index (cut eq? <> sym) uni))))

(define (enum-set-constructor es)
  (unless (enum-set? es)
          (error "Invalid argument to enum-set-constructor"))
  (let ((uni (enum-set-universe-ref es)))
    (lambda (syms)
      (unless (universe-valid-symbols? uni syms)
              (error "Invalid enumeration symbol"))
      (make-enum-set uni (symbol-set syms)))))
         
(define (enum-set-member? sym es)
  (set-contains? (enum-set-values es) sym))

(define (enum-set-subset? es1 es2)
  (and
   (set<=? (enum-set-universe-ref es1)
           (enum-set-universe-ref es2))
   (set<=? (enum-set-values es1) (enum-set-values es2))))

(define (enum-set=? es1 es2)
  (and
   (set=? (enum-set-universe-ref es1)
          (enum-set-universe-ref es2))
   (set=? (enum-set-values es1) (enum-set-values es2))))

(define (enum-set-universe=? es1 es2)
  (eq? (enum-set-universe-ref es1) (enum-set-universe-ref es2)))

(define (enum-set-union es1 es2)
  (unless (enum-set-universe=? es1 es2)
          (error "enum sets from different universes"))
  (make-enum-set (enum-set-universe-ref es1)
                 (set-union (enum-set-values es1)
                            (enum-set-values es2))))

(define (enum-set-intersection es1 es2)
  (unless (enum-set-universe=? es1 es2)
          (error "enum sets from different universes"))
  (make-enum-set (enum-set-universe-ref es1)
                 (set-intersection (enum-set-values es1)
                                   (enum-set-values es2))))

(define (enum-set-difference es1 es2)
  (unless (enum-set-universe=? es1 es2)
          (error "enum sets from different universes"))
  (make-enum-set (enum-set-universe-ref es1)
                 (set-difference (enum-set-values es1)
                                 (enum-set-values es2))))

(define (enum-set-complement es)
  (let ((uni (enum-set-universe-ref es)))
    (make-enum-set uni
                   (set-difference uni (enum-set-values es)))))

(define (enum-set-projection es1 es2)
  (let* ((uni (enum-set-universe-ref es2))
         (projected (set-intersection uni
                                      (enum-set-values es1))))
    (make-enum-set uni (set-union (enum-set-values es2) projected))))

(define-syntax define-enumeration
  (syntax-rules ()
    ((_ type-name (symbol ...) constructor-syntax)
     (begin
       (define the-enum (make-enumeration '(symbol ...)))
       (define-syntax type-name
         (lambda (s) 
           (syntax-case s ()
             ((type-name sym)
              (if (memq (syntax->datum #'sym) '(symbol ...))
                  #'(quote sym)
                  (report-syntax-error (symbol->string 'type-name) 
                                    "not a member of the set"
                                    #f))))))
       (define-syntax constructor-syntax
         (lambda (s)
           (syntax-case s ()
             ((_ sym (... ...))
              (let* ((universe '(symbol ...))
                     (syms (syntax->datum #'(sym (... ...))))
                     (quoted-universe 
                      (datum->syntax s (list 'quote universe)))
                     (quoted-syms (datum->syntax s (list 'quote syms))))
                (unless (every (cut memq <> universe) syms)
                    (report-syntax-error (symbol->string 'constructor-syntax)
                                      "not a subset of the universe"))
                #`((enum-set-constructor #,the-enum)
                   #,quoted-syms))))))))))
