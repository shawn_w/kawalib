;;; -*- scheme -*-
(define-library (rnrs enums)
  (export
   make-enumeration enum-set-universe enum-set-indexer
   enum-set-constructor enum-set->list enum-set-member?
   enum-set-subset? enum-set=?
   enum-set-union enum-set-intersection enum-set-union enum-set-difference
   enum-set-complement enum-set-projection define-enumeration

   enum-set?
   )

  (import (scheme base))
  (import (srfi 1))
  (import (srfi 26))
  (import (sets))
  (import (srfi 128))
  
   (include "enums-impl.scm"))
