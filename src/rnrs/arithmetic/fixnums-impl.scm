(define-alias fxnum int)

;; fixnum limits
(define-constant fx-width 32)
(define (fixnum-width) 32)
(define (greatest-fixnum) java.lang.Integer:MAX_VALUE)
(define (least-fixnum) java.lang.Integer:MIN_VALUE)

;; fixnum comparision functions
(define (fx=? a::fxnum b::fxnum #!rest nums::fxnum[]) (apply = a b nums))
(define (fx<? a::fxnum b::fxnum #!rest nums::fxnum[]) (apply < a b nums))
(define (fx<=? a::fxnum b::fxnum #!rest nums::fxnum[]) (apply <= a b nums))
(define (fx>? a::fxnum b::fxnum #!rest nums::fxnum[]) (apply > a b nums))
(define (fx>=? a::fxnum b::fxnum #!rest nums::fxnum[]) (apply >= a b nums))

;; fixnum predicates
(define (fixnum? n) (int? n))
(define (fxzero? n::fxnum) (= n 0))
(define (fxpositive? n::fxnum) (> n 0))
(define (fxnegative? n::fxnum) (< n 0))
(define (fxodd? n::fxnum) (= (bitwise-and n 1) 1))
(define (fxeven? n::fxnum) (= (bitwise-and n 1) 0))

;; fixnum min/max functions and misc. others
(define (fxmax a::fxnum b::fxnum #!rest nums::fxnum[]) :: fxnum (apply max a b nums))
(define (fxmin a::fxnum b::fxnum #!rest nums::fxnum[]) :: fxnum (apply min a b nums))

;; fixnum arithmetic functions
(define-syntax fxvalues
  (syntax-rules (swapped)
    ((_ f) (receive (v1 v2) f (values (as fxnum v1) (as fxnum v2))))
    ((_ f swapped) (receive (v1 v2) f (values (as fxnum v2) (as fxnum v1))))))
(define (fx+ a::fxnum b::fxnum) :: fxnum (java.lang.Math:addExact a b))
(define-procedure fx-
  (lambda (n::fxnum) :: fxnum (java.lang.Math:negateExact n))
  (lambda (a::fxnum b::fxnum) :: fxnum (java.lang.Math:subtractExact a b)))

(define (fx* a::fxnum b::fxnum) :: fxnum (java.lang.Math:multiplyExact a b))
(define (fxdiv-and-mod a::fxnum b::fxnum) (fxvalues (div-and-mod a b)))
(define (fxdiv a::fxnum b::fxnum) :: fxnum (div a b))
(define (fxmod a::fxnum b::fxnum) :: fxnum (mod a b))
(define (fxdiv0-and-mod0 a::fxnum b::fxnum) (fxvalues (div0-and-mod0 a b)))
(define (fxdiv0 a::fxnum b::fxnum) :: fxnum (div0 a b))
(define (fxmod0 a::fxnum b::fxnum) :: fxnum (mod0 a b))
(define (fx+/carry a::fxnum b::fxnum c::fxnum)
  (fxvalues (div0-and-mod0 (+ a b c) (expt 2 fx-width)) swapped))
(define (fx-/carry a::fxnum b::fxnum c::fxnum)
  (fxvalues (div0-and-mod0 (- a b c) (expt 2 fx-width)) swapped))
(define (fx*/carry a::fxnum b::fxnum c::fxnum)
  (fxvalues (div0-and-mod0 (+ (* a b) c) (expt 2 fx-width)) swapped))

;; fixnum bit manipulation functions
(define (fxnot n::fxnum) :: fxnum (bitwise-not n))
(define (fxand a::fxnum #!rest nums::fxnum[]) :: fxnum (apply bitwise-and a nums))
(define (fxior a::fxnum #!rest nums::fxnum[]) :: fxnum (apply bitwise-ior a nums))
(define (fxxor a::fxnum #!rest nums::fxnum[]) :: fxnum (apply bitwise-xor a nums))
(define (fxif mask::fxnum a::fxnum b::fxnum) :: fxnum
  (fxior (fxand (fxnot mask) a)
         (fxand mask b)))
(define (fxbit-count n::fxnum) :: fxnum (bitwise-bit-count n))
(define (fxlength n::fxnum) :: fxnum (bitwise-length n))
(define (fxfirst-bit-set n::fxnum) :: fxnum (bitwise-first-bit-set n))
(define (fxbit-set? a::fxnum b::fxnum) (bitwise-bit-set? a b))
(define (fxcopy-bit a::fxnum b::fxnum c::fxnum) :: fxnum (bitwise-copy-bit a b c))
(define (fxbit-field a::fxnum b::fxnum c::fxnum) :: fxnum (bitwise-bit-field a b c))
(define (fxcopy-bit-field a::fxnum b::fxnum c::fxnum d::fxnum) :: fxnum (bitwise-copy-bit-field a b c d))
(define (fxarithmetic-shift a::fxnum b::fxnum) :: fxnum (bitwise-arithmetic-shift a b))
(define (fxarithmetic-shift-left a::fxnum b::fxnum) :: fxnum (bitwise-arithmetic-shift-left a b))
(define (fxarithmetic-shift-right a::fxnum b::fxnum) :: fxnum (bitwise-arithmetic-shift-right a b))
(define (fxrotate-bit-field a::fxnum b::fxnum c::fxnum d::fxnum) :: fxnum (bitwise-rotate-bit-field a b c d))
(define (fxreverse-bit-field a::fxnum b::fxnum c::fxnum) :: fxnum (bitwise-reverse-bit-field a b c))
