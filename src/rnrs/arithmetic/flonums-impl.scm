;;; R6RS flonums library
;;; Uses 64-bit java native doubles.
;;;
;;; Written by Shawn Wagner <shawnw.mobile@gmail.com> unless otherwise noted.
;;;

;; flonum constructors.
(define (real->flonum n::real) :: double (as double n))
(define (fixnum->flonum n::int) :: double (as double n))
(define flcopysign java.lang.Math:copySign)
(define (flsgn n::double) :: double (flcopysign 1.0 n))

;; flonum comparision functions
(define (fl=? a::double b::double #!rest nums::double[]) (apply = a b nums))
(define (fl<? a::double b::double #!rest nums::double[]) (apply < a b nums))
(define (fl<=? a::double b::double #!rest nums::double[]) (apply <= a b nums))
(define (fl>? a::double b::double #!rest nums::double[]) (apply > a b nums))
(define (fl>=? a::double b::double #!rest nums::double[]) (apply >= a b nums))

;; flonum predicates
(define (flonum? n) (double? n))
(define (flinteger? n::double) (integer? n))
(define (flzero? n::double) (= n 0.0))
(define (flpositive? n::double) (> n 0.0))
(define (flnegative? n::double) (< n 0.0))
(define (flinfinite? n::double) (java.lang.Double:isInfinite n))
(define (flnan? n::double) (java.lang.Double:isNaN n))
(define (flfinite? n::double) (not (or (flnan? n) (flinfinite? n))))

(define (flodd? n::double)
  (if (flinteger? n)
      (= (mod (as long n) 2) 1)
      (error "flodd? called with a non-integeral value")))
(define (fleven? n::double)
  (if (flinteger? n)
      (= (mod (as long n) 2) 0)
      (error "fleven? called with a non-integeral value")))
;; flonum min/max functions and misc stuff.
(define-syntax flpick
  (syntax-rules ()
    ((_ pred? init nums)
     (let loop ((val init)
                (pos 0))
       (cond
        ((= pos nums:length) val)
        ((flnan? (nums pos)) +nan.0)
        ((pred? (nums pos) val) (loop (nums pos) (+ pos 1)))
        (else (loop val (+ pos 1))))))))
(define (flmax a::double #!rest nums::double[]) :: double (flpick fl>? a nums))
(define (flmin a::double #!rest nums::double[]) :: double (flpick fl<? a nums))

(define (flabs n::double) :: double (java.lang.Math:abs n))
(define (flnumerator n::double) :: double
  (if (and (flzero? n) (< (flsgn n) 0.0))
      n
      (numerator n)))
(define (fldenominator n::double) :: double (denominator n))
(define flsqrt java.lang.Math:sqrt)


;; flonum arithmetic functions
(define (fl+ a::double #!rest nums::double[]) :: double (apply + a nums))
(define (fl* a::double #!rest nums::double[]) :: double (apply * a nums))
(define-procedure fl-
  (lambda (n::double) :: double (- 0.0 n))
  (lambda (a::double b::double #!rest nums::double[]) :: double (apply - a b nums)))
(define-procedure fl/
  (lambda (n::double) :: double (/ 1.0 n))
  (lambda (a::double b::double #!rest nums::double[]) :: double (apply / a b nums)))

;; mod/div stuff
(define-syntax first-value (syntax-rules () ((_ f) (receive (v1 v2) f v1))))
(define-syntax second-value (syntax-rules () ((_ f) (receive (v1 v2) f v2))))
(define-syntax make-divmod
  (syntax-rules (as with and)
    ((_ real as name with realmod as mod and realdiv as div)
     (begin
       (define (name a::double b::double)
         (let-values (((r q) (real a b)))
                  (values (as double r) (as double q))))
       (define (mod a::double b::double) :: double (realmod a b))
       (define (div a::double b::double) :: double (realdiv a b))))))

(define-syntax flvalues
  (syntax-rules ()
    ((_ f) (receive (v1 v2) f (values (as double v1) (as double v2))))))
(define (fldiv-and-mod a::double b::double) (flvalues (div-and-mod a b)))
(define (fldiv a::double b::double) :: double (div a b))
(define (flmod a::double b::double) :: double (mod a b))
(define (fldiv0-and-mod0 a::double b::double) (flvalues (div0-and-mod0 a b)))
(define (fldiv0 a::double b::double) :: double (div0 a b))
(define (flmod0 a::double b::double) :: double (mod0 a b))

;; flonum rounding functions
(define flfloor java.lang.Math:floor)
(define flceiling java.lang.Math:ceil)
(define (fltruncate n::double) :: double (truncate n))
(define flround java.lang.Math:rint)

;; flonum exponential and log functions
(define flexpt java.lang.Math:pow)
(define flexp java.lang.Math:exp)
(define-procedure fllog
  (lambda (n::double) :: double (java.lang.Math:log n))
  (lambda (f1::double f2::double) :: double
          (/ (java.lang.Math:log f1)
             (java.lang.Math:log f2))))

;; flonum trig functions
(define flsin java.lang.Math:sin)
(define flcos java.lang.Math:cos)
(define fltan java.lang.Math:tan)
(define flasin java.lang.Math:asin)
(define flacos java.lang.Math:acos)
(define-procedure flatan
  (lambda (n::double) :: double (java.lang.Math:atan n))
  (lambda (a::double b::double) :: double (java.lang.Math:atan2 a b)))
