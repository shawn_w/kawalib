;;; R6RS fixnum library. -*- scheme -*-
;;; Uses 32-bit java native ints.
;;;
;;; Written by Shawn Wagner <shawnw.mobile@gmail.com>
;;;
;;; TODO: Consider adding fx64FOO versions for native longs? Maybe short and byte, too?

(define-library (rnrs arithmetic fixnums)
  (export
   fixnum?
   fixnum-width greatest-fixnum least-fixnum
   fx=? fx<? fx>? fx<=? fx>=?
   fxzero? fxpositive?  fxnegative? fxodd? fxeven?
   fxmax fxmin
   fx+ fx* fx-
   fxdiv fxmod fxdiv-and-mod fxdiv0-and-mod0 fxdiv0 fxmod0
   fx+/carry fx-/carry fx*/carry
   fxnot fxand fxior fxxor fxif
   fxbit-count fxlength  fxfirst-bit-set fxbit-set? fxcopy-bit
   fxbit-field fxcopy-bit-field
   fxarithmetic-shift fxarithmetic-shift-left fxarithmetic-shift-right
   fxrotate-bit-field fxreverse-bit-field)

  (import (kawa base))
  
  (include "fixnums-impl.scm"))

