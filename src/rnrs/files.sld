;;; -*- scheme -*-
(define-library (rnrs files)
  (export file-exists? delete-file)
  (import (only (files) file-exists? delete-file)))
