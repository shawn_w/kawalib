(define-library (json)
  (export
   json?
   json-null? json-bool? json-number? json-string?
   json-array? json-object?
   json-ref json-array-length
   json-null json-true json-false
   read-json string->json bytevector->json
   json->native native->json
   write-json json->string json->bytevector
   )
  (import (kawa base)
          (io-utils)
          (list-utils)
          (srfi 1)
          (srfi 14)
          (collections))
  (include "json-impl.scm"))
