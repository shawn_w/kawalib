;;; Intset library

(define-library (intset)
  (import (kawa base)
          (srfi 1)
          (srfi 133)
          )


  (export intset? intset-contains? intset-empty?
          empty-intset vector->intset list->intset range->intset
          intset->list
          intset-add intset-delete
          intset-before intset-after intset-within-range
          intset-minimum intset-maximum
          )

  (include "intset-impl.scm"))
