;;; max heap priority queues, API based on slib -*- scheme -*-
;;; https://www.cs.indiana.edu/scheme-repository/SCM/slib_2.html#SEC23
;;; 
(define-library (priority-queue)
  (export heap make-heap heap?
          heap-length heap-empty?
          heap-insert!
          heap-extract-max! heap-peek-max
          heap->list heap->sorted-list
          heap->vector heap->sorted-vector
          heap-sort heap-sort-vector!
          )
  (import
   (kawa base)
   (srfi 128)
   (srfi 133)
   (rename collections
    (coll-priority-queue? heap?)
    (coll-size heap-length)
    (coll-empty? heap-empty?)
    (coll-add! heap-insert!)
    (coll-queue-remove! heap-extract-max!)
    (coll-queue-element heap-peek-max)
    (coll->vector heap->vector)
    (coll->list heap->list))
    )
  (begin

    (define-alias pq java.util.PriorityQueue)

    (define (make-heap pred? #!key equals #!rest vals) :: pq
      (let ((pq
             (cond
              ((not (or (null? vals) (and (= (length vals) 1) (list? (car vals)))))
               (error "Invalid list of initial heap values."))
              ((comparator? pred?)
               (coll-priority-queue compare: pred?))
              ((not (procedure? pred?))
               (error "Invalid pred? argument to make-heap"))
              ((procedure? equals)
               (coll-priority-queue compare: (make-comparator #f equals pred? #f)))
              ((not (eq? equals #f))
               (error "Invalid equals argument to make-heap"))
              (else
               (let* ((equals? (lambda (a b) (not (or (pred? a b) (pred? b a)))))
                      (comp (make-comparator #f equals? pred? #f))
                      (pq (coll-priority-queue compare: comp)))
                 pq)))))
        (if (not (null? vals))
            (for-each (lambda (val) (heap-insert! pq val)) (car vals)))
        pq))
        
    (define (heap pred? #!key equals #!rest vals)
      (make-heap pred? equals: equals vals))
    
    (define (heap->sorted-list hp::pq)
      (let ((copy (pq hp)))
        (do ((l '() (cons (heap-extract-max! copy) l)))
            ((heap-empty? copy) l))))

    (define (heap->sorted-vector hp::pq)
      (let* ((copy (pq hp))
             (len (heap-length hp))
             (v (make-vector len)))
        (do ((i (- len 1) (- i 1)))
            ((heap-empty? copy) v)
          (vector-set! v i (heap-extract-max! copy)))))

    (define (heap-sort pred? lst)
      (let ((hp (make-heap pred? lst)))
        (heap->sorted-list hp)))

    (define (heap-sort-vector! pred? vec)
      (let* ((hp (make-heap pred?)))
        (vector-for-each (lambda (elem) (heap-insert! hp elem)) vec)
        (do ((i (- (vector-length vec) 1) (- i 1)))
            ((< i 0))
          (vector-set! vec i (heap-extract-max! hp)))))
    ))
    

    
  
