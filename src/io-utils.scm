(module-name io-utils)
(module-export
 available-encodings
 open-encoded-input-file open-encoded-output-file
 call-with-encoded-input-file call-with-encoded-output-file
 with-input-from-encoded-file with-output-to-encoded-file

 open-encoded-input-bytevector open-encoded-output-bytevector
 call-with-encoded-input-bytevector call-with-encoded-output-bytevector

 encoded-bytevector->string string->encoded-bytevector

 bytevector->base64 base64->bytevector
 make-base64-output-port make-base64-input-port
 
 read-lines stream-lines
 skip-leading-whitespace
 read-word stream-words read-words
 read-number stream-numbers read-numbers
 stream-sexprs read-sexprs

 port-for-each port-map port-fold copy-port
 
 write-list display-list
 print println print*

 ;; Oleg's input-parse routines
 peek-next-char assert-curr-char skip-until skip-while
 next-token next-token-of read-text-line read-string
 find-string-from-port?

 ;; java.util.Scanner interface
 scanner?
 open-input-scanner-file string->scanner input-port->scanner scanner-close
 scanner->generator scanner->stream
 
 scanner-next? scanner-next-bool? scanner-next-byte? scanner-next-double?
 scanner-next-float? scanner-next-short? scanner-next-int? scanner-next-long?
 scanner-next-line? 

 scanner-next scanner-next-bool scanner-next-byte scanner-next-double
 scanner-next-float scanner-next-short scanner-next-int scanner-next-long
 scanner-next-line 

 scanner-find-in-line scanner-find-in-input
 
 scanner-skip scanner-set-delimiter! scanner-set-radix!
 scanner-get-delimiter scanner-get-radix
 
 )

(import (kawa base)
        (srfi 41)
        (collections)
        (only data-structures identity)
        (srfi 158)
        )

(include "input-parse.scm")
(include "look-for-str.scm")

(define (available-encodings)
  (let ((encmap (java.nio.charset.Charset:availableCharsets)))
        (map string->symbol (coll->list (encmap:keySet)))))

(define-syntax with-encoding
  (syntax-rules ()
    ((_ enc body ...)
     (fluid-let ((port-char-encoding enc))
       body ...))))

(define (open-encoded-input-file filename encoding)
  (with-encoding encoding (open-input-file filename)))
(define (open-encoded-output-file filename encoding)
  (with-encoding encoding (open-output-file filename)))
(define (call-with-encoded-input-file filename encoding proc)
  (with-encoding encoding (call-with-input-file filename proc)))
(define (call-with-encoded-output-file filename encoding proc)
  (with-encoding encoding (call-with-output-file filename proc)))
(define (with-input-from-encoded-file filename encoding thunk)
  (with-encoding encoding (with-input-from-file filename thunk)))
(define (with-output-to-encoded-file filename encoding thunk)
  (with-encoding encoding (with-output-to-file filename thunk)))

(define (open-encoded-input-bytevector bv encoding)
  (with-encoding encoding (open-input-bytevector bv)))
(define (open-encoded-output-bytevector encoding)
  (with-encoding encoding (open-output-bytevector)))
(define (call-with-encoded-input-bytevector bv encoding proc)
  (proc (open-encoded-input-bytevector bv encoding)))
(define (call-with-encoded-output-bytevector encoding proc)
  (let ((bvp (open-encoded-output-bytevector encoding)))
    (proc bvp)
    (get-output-bytevector bvp)))

(define (encoded-bytevector->string bv::u8vector encoding)
  (gnu.lists.IString (java.lang.String (bv:getBuffer)
                                       0
                                       (u8vector-length bv)
                                       (symbol->string encoding))))
(define (string->encoded-bytevector str::string encoding)
  (let ((str (as String str)))
    (make <u8vector> (str:getBytes (symbol->string encoding)))))

(define (b64encode enc::java.util.Base64$Encoder ba::byte[]) ::string
  (gnu.lists.IString (enc:encodeToString ba)))
(define (b64decode enc::java.util.Base64$Decoder s::java.lang.String) ::u8vector
  (make <u8vector> (enc:decode s)))

(define (get-base64-encoder format) ::java.util.Base64$Encoder
  (case format
    ((basic) (java.util.Base64:getEncoder))
    ((url) (java.util.Base64:getUrlEncoder))
    ((mime) (java.util.Base64:getMimeEncoder))
    (else
     (error "Unknown Base64 encoder" format))))

(define (get-base64-decoder format) ::java.util.Base64$Decoder
  (case format
    ((basic) (java.util.Base64:getDecoder))
    ((url) (java.util.Base64:getUrlDecoder))
    ((mime) (java.util.Base64:getMimeDecoder))
    (else
     (error "Unknown Base64 encoder" format))))

(define (bytevector->base64 bv::u8vector #!optional (format 'basic)) ::string
  (let ((pruned ::byte[] (java.util.Arrays:copyOf (bv:getBuffer)
                                                  (u8vector-length bv)))
        (enc (get-base64-encoder format)))
    (b64encode enc pruned)))

(define (base64->bytevector s::string #!optional (format 'basic)) ::u8vector
  (b64decode (get-base64-decoder format) s))
(define make-base64-output-port
  (make-procedure
   (lambda (port::java.io.OutputStream
            #!optional (format `basic)) ::java.io.OutputStream
            (let ((enc (get-base64-encoder format)))
              (enc:wrap port)))
   (lambda (port::gnu.kawa.io.BinaryOutPort
            #!optional (format `basic)) ::java.io.OutputStream
            (let ((enc (get-base64-encoder format)))
              (enc:wrap (port:getOutputStream))))))

(define make-base64-input-port
  (make-procedure
   (lambda (port::java.io.InputStream
            #!optional (format `basic)) ::java.io.InputStream
            (let ((dec (get-base64-decoder format)))
              (dec:wrap port)))
   (lambda (port::gnu.kawa.io.BinaryInPort
            #!optional (format `basic)) ::java.io.InputStream
            (let ((dec (get-base64-decoder format)))
              (dec:wrap (port:getInputStream))))))

(define-syntax define-port-stream
  (syntax-rules ()
    ((_ ident func)
     (define (ident #!optional (port (current-input-port)))
       (stream-let loop ((token (func port)))
                   (if (eof-object? token)
                       stream-null
                       (stream-cons token (loop (func port)))))))))

(define (stream-lines #!optional (port (current-input-port)) (handle-newline 'trim))
  (stream-let loop ((line (read-line port handle-newline)))
              (if (eof-object? line)
                  stream-null
                  (stream-cons line (loop (read-line port handle-newline))))))

(define (skip-leading-whitespace #!optional (port (current-input-port)))
  (next-token-of (lambda (c)
                   (cond
                    ((eof-object? c) #f)
                    ((char-whitespace? c) c)
                    (else #f)))
                 port)
  (not (eof-object? (peek-char port))))
  
(define (read-word #!optional (port (current-input-port)))
  (if (skip-leading-whitespace port)
      (next-token-of (lambda (c)
                       (cond
                        ((eof-object? c) #f)
                        ((char-whitespace? c) #f)
                        (else c)))
                     port)
      (eof-object)))

(define-port-stream stream-words read-word)
(define-port-stream stream-sexprs read)
                           
(define (read-number #!optional (port (current-input-port)))
  (let ((token (read port)))
    (cond
     ((eof-object? token) token)
     ((number? token) token)
     (else
      (error "Invalid number" token)))))

(define (stream-numbers #!optional (port (current-input-port)))
  (stream-map (lambda (n)
                (if (number? n)
                    n
                    (error "invalid number" n)))
              (stream-sexprs port)))

;; Functions taken from chicken's ports library

(define (port-for-each fn thunk)
  (generator-for-each fn thunk))
(define (port-fold fn acc thunk)
  (generator-fold fn acc thunk))
(define (port-map fn thunk)
  (generator-map->list fn thunk))
(define (copy-port from to #!optional (read read-char) (write write-char))
  (port-for-each write read))

(define (read-lines #!optional (port (current-input-port)) (handle-newline 'trim))
  (port-map identity (lambda () (read-line port handle-newline))))

(define (read-numbers #!optional (port (current-input-port)))
  (port-map identity (lambda () (read-number port))))

(define (read-words #!optional (port (current-input-port)))
  (port-map identity (lambda () (read-word port))))

(define (read-sexprs #!optional (port (current-input-port)))
  (port-map identity (lambda () (read port))))

(define (output-list lst fn port sep)
  (unless (null? lst)
          (fn (car lst) port)
          (let loop ((lst (cdr lst)))
            (unless (null? lst)
                    (write-char sep port)
                    (fn (car lst) port)
                    (loop (cdr lst))))))

(define (write-list lst #!optional (port (current-output-port)) #!key (sep #\space))
  (output-list lst write port sep))

(define (display-list lst #!optional (port (current-output-port)) #!key (sep #\space))
  (output-list lst display port sep))

(define (println val #!optional (port (current-output-port)))
  (display val port)
  (newline port))

(define (print-helper vals finish)
  (let loop ((vals vals))
    (if (null? vals)
        (finish)
        (begin
          (display (car vals))
          (loop (cdr vals))))))
 
(define (print . args) (print-helper args newline))
(define (print* . args) (print-helper args flush-output-port))

(define-alias scanner java.util.Scanner)
(define-alias pattern java.util.regex.Pattern)

(define (scanner? obj) ::boolean (instance? obj java.util.Scanner))

(define open-input-scanner-file
  (make-procedure
   (lambda (name::string) ::scanner
           (scanner (java.io.File name)))
   (lambda (name::string encoding::symbol) ::scanner
           (scanner (java.io.File name) (symbol->string encoding)))))
(define (string->scanner s::string) ::scanner (scanner s))
(define input-port->scanner
  (make-procedure
   (lambda (port::gnu.kawa.io.BinaryInPort) ::scanner
           (scanner (port:getInputStream)))
   (lambda (port::gnu.kawa.io.BinaryInPort encoding::symbol) ::scanner
           (scanner (port:getInputStream) (symbol->string encoding)))
   (lambda (port::java.io.InputStream) ::scanner
           (scanner port))
   (lambda (port::java.io.InputStream encoding::symbol) ::scanner
           (scanner port (symbol->string encoding)))))

(define (scanner-close s::scanner) (s:close))

(define scanner-next?
  (make-procedure
   (lambda (s::scanner) ::boolean (s:hasNext))
   (lambda (s::scanner re::string) ::boolean (s:hasNext re))
   (lambda (s::scanner re::pattern) ::boolean (s:hasNext re))))
(define scanner-next
  (make-procedure
   (lambda (s::scanner) ::string (gnu.lists.IString (s:next)))
   (lambda (s::scanner re::string) ::string (gnu.lists.IString  (s:next re)))
   (lambda (s::scanner re::pattern) ::string (gnu.lists.IString  (s:next re)))))

(define-syntax make-number-scanner
  (syntax-rules ()
    ((_ pred? predm getter getm type)
     (begin
       (define pred?
         (make-procedure
          (lambda (s::scanner) ::boolean (s:predm))
          (lambda (s::scanner radix::int) ::boolean (s:predm radix))))
       (define getter
         (make-procedure
          (lambda (s::scanner) ::type (s:getm))
          (lambda (s::scanner radix::int) ::type (s:getm radix))))))))

(make-number-scanner scanner-next-byte? hasNextByte scanner-next-byte nextByte byte)
(make-number-scanner scanner-next-short? hasNextShort scanner-next-short nextShort short)
(make-number-scanner scanner-next-int? hasNextInt scanner-next-int nextInt int)
(make-number-scanner scanner-next-long? hasNextLong scanner-next-long nextLong long)

(define (scanner-next-bool? s::scanner) ::boolean (s:hasNextBoolean))
(define (scanner-next-bool s::scanner) ::boolean (s:nextBoolean))
(define (scanner-next-float? s::scanner) ::boolean (s:hasNextFloat))
(define (scanner-next-float s::scanner) ::float (s:nextFloat))
(define (scanner-next-double? s::scanner) ::boolean (s:hasNextDouble))
(define (scanner-next-double s::scanner) ::double (s:nextDouble))
(define (scanner-next-line? s::scanner) ::boolean (s:hasNextLine))
(define (scanner-next-line s::scanner) ::string (s:nextLine))

(define scanner-find-in-line
  (make-procedure
   (lambda (s::scanner re::string)
     (let ((res (s:findInLine re)))
       (if (eq? res #!null)
           #f
           (gnu.lists.IString res))))
   (lambda (s::scanner re::pattern)
     (let ((res (s:findInLine re)))
       (if (eq? res #!null)
           #f
           (gnu.lists.IString res))))))

(define scanner-find-in-input
  (make-procedure
   (lambda (s::scanner re::string #!optional (horizon ::int 0))
     (let ((res (s:findWithinHorizon re horizon)))
       (if (eq? res #!null)
           #f
           (gnu.lists.IString res))))
   (lambda (s::scanner re::pattern #!optional (horizon ::int 0))
     (let ((res (s:findWithinHorizon re horizon)))
       (if (eq? res #!null)
           #f
           (gnu.lists.IString res))))))

(define scanner-skip
  (make-procedure
   (lambda (s::scanner re::string) (s:skip re))
   (lambda (s::scanner re::pattern) (s:skip re))))

(define scanner-set-delimiter!
  (make-procedure
   (lambda (s::scanner re::string) (s:useDelimiter re))
   (lambda (s::scanner re::pattern) (s:useDelimiter re))))
(define (scanner-get-delimiter s::scanner) ::pattern (s:delimiter))
(define (scanner-get-radix s::scanner) ::int (s:radix))
(define (scanner-set-radix! s::scanner radix::int)
  (s:useRadix radix))

(define (scanner->generator s::scanner next? next)
  (lambda ()
    (if (next? s)
        (next s)
        (eof-object))))

(define (scanner->stream s::scanner next? next)
  (stream-let loop ((has-next (next? s)))
              (if has-next
                  (let ((val (next s)))
                    (stream-cons val (loop (next? s))))
                  stream-null)))
