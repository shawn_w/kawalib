(define (string->listmap f str::string)
  (let ((l (list-queue)))
    (string-for-each (lambda (ch)
                       (list-queue-add-back! l (f ch))) str)
    (list-queue-list l)))

(define (string->vectormap f str::string) ::vector
  (let ((v (make-vector (string-length str))))
    (do ((i (- (string-length str) 1) (- i 1)))
        ((< i 0) v)
      (vector-set! v i (f (string-ref str i))))))
             
(define (vector->listmap f vec::vector)
  (let ((l (list-queue)))
    (vector-for-each (lambda (ele)
                       (list-queue-add-back! l (f ele))) vec)
    (list-queue-list l)))

(define (list->vectormap f lst) ::vector
  (let ((v (gnu.lists.FVector)))
    (for-each (lambda (elt) (v:add (f elt))) lst)
    v))

(define (pair->values p) (values (car p) (cdr p)))

(define (minmax lst #!optional (less? <))
  (pair->values
   (fold (lambda (elem acc)
           (cons (if (less? elem (car acc)) elem (car acc))
                 (if (less? (cdr acc) elem) elem (cdr acc))))
         (cons (car lst) (car lst)) (cdr lst))))

(define (filter-regex pat lst)
  (let ((pat (if (regex? pat) pat (regex pat))))
    (filter (cut regex-match pat <>) lst)))

(define (filter-regex! pat lst)
  (let ((pat (if (regex? pat) pat (regex pat))))
    (filter! (cut regex-match pat <>) lst)))

(define (remove-regex pat lst)
  (let ((pat (if (regex? pat) pat (regex pat))))
    (remove (cut regex-match pat <>) lst)))

(define (remove-regex! pat lst)
  (let ((pat (if (regex? pat) pat (regex pat))))
    (remove! (cut regex-match pat <>) lst)))
