;;; uuid generator
(define-library (uuid)
  (export
   uuid? generate-uuid make-uuid make-type1-uuid make-type3-uuid make-type5-uuid
   uuid=? uuid-compare uuid-hash uuid-comparator
   uuid->string string->uuid uuid-split
   uuid-version uuid-variant uuid-decompose

   uuid-namespace-dns uuid-namespace-url uuid-namespace-oid uuid-namespace-x500


   mac-address intervals-since-epoch get-host-interfaces
   
   )
  (import (kawa base)
          (srfi 128)
          (digest)
          (uvector-utils)
          (srfi 74)
          (srfi 27)
          (collections))
  (include "uuid-impl.scm"))

          
