;;; Interface to java.util.BitSet

(define-library (bitset)
  (import (kawa base)
          (srfi 128))
  (export
   make-bitset bitset-copy bitset?
   bitset-ref bitset-set! bitset-fill! bitset-clear!
   bitset-empty? bitset-count bitset-length
   bitset=? bitset-hash bitset-comparator

   bitset-not!
   bitset-and! bitset-xor! bitset-ior! bitset-andnot!
   bitset-intersects?

   bitset->bytevector bytevector->bitset

   bitset-first-set-bit bitset-first-clear-bit
   bitset-last-set-bit bitset-last-clear-bit
   )

  
  (include "bitset-impl.scm"))
