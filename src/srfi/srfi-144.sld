;;; srfi 144 flonums -*- scheme -*-
(define-library (srfi 144)
  (export
    ;; r6rs arithmetic flonums symbols
   flonum? real->flonum
   fl=? fl<? fl<=? fl>=? fl>?
   flinteger? flzero? flpositive? flnegative? flodd? fleven? flfinite?
   flinfinite? flnan?
   flmin flmax
   fl+ fl* fl- fl/ flabs
   fldiv-and-mod fldiv flmod fldiv0-and-mod0 fldiv0 flmod0
   flnumerator fldenominator
   flfloor flceiling fltruncate flround
   flexp fllog flsin flcos fltan flasin flacos flatan flsqrt flexpt
   fixnum->flonum
 
   ;; SRFI-144 symbols
   fl-e fl-1/e fl-e-2 fl-e-pi/4 fl-log2-e fl-log10-e fl-log-2 fl-1/log-2 fl-log-3
   fl-log-pi fl-log-10 fl-1/log-10
   fl-pi fl-1/pi fl-2pi fl-pi/2 fl-pi/4 fl-pi-squared fl-degree fl-2/pi fl-2/sqrt-pi
   fl-sqrt-2 fl-sqrt-3 fl-sqrt-10 fl-1/sqrt-2 fl-cbrt-2 fl-cbrt-3 fl-4thrt-2
   fl-phi fl-log-phi fl-1/log-phi
   fl-euler fl-e-euler
   fl-sin-1 fl-cos-1
   fl-gamma-1/2 fl-gamma-1/3 fl-gamma-2/3
   fl-greatest fl-least fl-epsilon fl-fast-fl+*
   fl-integer-exponent-zero fl-integer-exponent-nan               
   flonum fladjacent flcopysign make-flonum 
   flinteger-fraction flexponent flinteger-exponent flnormalized-fraction-exponent
   flsign-bit
   flunordered? flnormalized? fldenormalized?
   fl+*
   flabsdiff flposdiff flsgn
   flexp2 flexp-1 flsquare flcbrt flhypot fllog1+ fllog2 fllog10 make-fllog-base
   flsinh flcosh fltanh flasinh flacosh flatanh
   flquotient flremainder flremquo
   flgamma flloggamma flfirst-bessel flsecond-bessel flerf flerfc
   )

  (import (kawa base)
          (rnrs arithmetic flonums))

  (include "srfi-144-impl.scm"))
