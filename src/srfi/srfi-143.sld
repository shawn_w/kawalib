;;; SRFI-143 fixnums -*- scheme -*-

(define-library (srfi 143)
  (export
   ;; r6rs arithmetic fixnums symbols
   fixnum?
   fixnum-width greatest-fixnum least-fixnum
   fx=? fx<? fx>? fx<=? fx>=?
   fxzero? fxpositive?  fxnegative? fxodd? fxeven?
   fxmax fxmin
   fx+ fx* fx-
   fxdiv fxmod fxdiv-and-mod fxdiv0-and-mod0 fxdiv0 fxmod0
   fx+/carry fx-/carry fx*/carry
   fxnot fxand fxior fxxor fxif
   fxbit-count fxlength  fxfirst-bit-set fxbit-set? fxcopy-bit
   fxbit-field fxcopy-bit-field
   fxarithmetic-shift fxarithmetic-shift-left fxarithmetic-shift-right
   fxrotate-bit-field fxreverse-bit-field
   ;; srfi-143 symbols
   fx-width fx-greatest fx-least
   fxneg fxabs fxsquare fxsqrt
   fxquotient fxremainder
   )

  (import (kawa base)
          (rnrs arithmetic fixnums))

  (begin
    (define-alias fxnum int)
    (define-constant fx-width (as int 32))
    (define-constant fx-greatest java.lang.Integer:MAX_VALUE)
    (define-constant fx-least java.lang.Integer:MIN_VALUE)
    (define-syntax fxvalues
      (syntax-rules (swapped)
        ((_ f) (receive (v1 v2) f (values (as fxnum v1) (as fxnum v2))))))
    (define (fxabs n::fxnum) ::fxnum (java.lang.Math:abs n))
    (define (fxneg n::fxnum) :: fxnum (fx- n))
    (define (fxsquare a::fxnum) :: fxnum (fx* a a))
    (define (fxsqrt a::fxnum) (fxvalues (exact-integer-sqrt a)))
    (define (fxquotient a::fxnum b::fxnum) :: fxnum (quotient a b))
    (define (fxremainder a::fxnum b::fxnum) :: fxnum (remainder a b))
    ))
