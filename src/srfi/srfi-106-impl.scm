

(define *af-unspec* '*af-unspec*)
(define *af-inet* '*af-unspec*)
(define *af-inet6* '*af-inet6*)
(define *sock-stream* '*sock-stream*)
(define *sock-dgram* '*sock-dgram*)
(define *ai-canonname* '*ai-canonname*)
(define *ai-numerichost* '*ai-numerichost*)
(define *ai-v4mapped* '*ai-v4mapped*)
(define *ai-all* '*ai-all*)
(define *ai-addrconfig* '*ai-addrconfig*)
(define *ipproto-ip* '*ipproto-ip*)
(define *ipproto-tcp* '*ipproto-tcp*)
(define *ipproto-udp* '*ipproto-tcp*)
(define *msg-peek* '*msg-peek*)
(define *msg-oob* '*msg-oob*)
(define *msg-waitall* '*msg-waitall*)
(define *shut-rd* '*shut-rd*)
(define *shut-wr* '*shut-wr*)
(define *shut-rdwr* '*shut-rdwr*)

(define socket-merge-flags list)
(define (socket-purge-flags flags . to-remove)
  (fold (lambda (fl curr) (delete curr fl eq?)) flags to-remove))

(define %address-family `((inet    ,*af-inet*)
                          (inet6   ,*af-inet6*)
                          (unspec  ,*af-unspec*)))

(define %address-info `((canoname     ,*ai-canonname*)
                        (numerichost  ,*ai-numerichost*)
                        (v4mapped     ,*ai-v4mapped*)
                        (all          ,*ai-all*)
                        (addrconfig   ,*ai-addrconfig*)))

(define %ip-protocol `((ip  ,*ipproto-ip*)
                       (tcp ,*ipproto-tcp*)
                       (udp ,*ipproto-udp*)))

(define %socket-domain `((stream   ,*sock-stream*)
                         (datagram ,*sock-dgram*)))

(define %message-types `((none 0)
                         (peek ,*msg-peek*)
                         (oob  ,*msg-oob*)
                         (wait-all ,*msg-waitall*)))

(define (lookup who sets name)
  (cond ((assq name sets) => cadr)
        (else (error who "no name defined" name))))

(define-syntax address-family
  (syntax-rules ()
    ((_ name)
     (lookup 'address-family %address-family 'name))))

(define-syntax address-info
  (syntax-rules ()
    ((_ names ...)
     (apply socket-merge-flags 
            (map (lambda (name) (lookup 'address-info %address-info name))
                 '(names ...))))))

(define-syntax socket-domain
  (syntax-rules ()
    ((_ name)
     (lookup 'socket-domain %socket-domain 'name))))

(define-syntax ip-protocol
  (syntax-rules ()
    ((_ name)
     (lookup 'ip-protocol %ip-protocol 'name))))

(define-syntax message-type
  (syntax-rules ()
    ((_ names ...)
     (apply socket-merge-flags 
            (map (lambda (name) (lookup 'message-type %message-types name))
                 '(names ...))))))

(define (%proper-method methods)
  (define allowed-methods '(read write))
  (define (check-methods methods)
    (let loop ((methods methods) (seen '()))
      (cond ((null? methods))
            ((memq (car methods) allowed-methods)
             => (lambda (m)
                  (if (memq (car m) seen)
                      (error 'shutdown-method
                                           "duplicate method" m)
                      (loop (cdr methods) (cons (car m) seen)))))
            (else (error 'shutdown-method
                                       "unknown method" (car methods))))))
  (check-methods methods)
  (if (null? (cdr methods))
      (case (car methods)
        ((read) *shut-rd*)
        ((write) *shut-wr*))
      *shut-rdwr*))

(define-syntax shutdown-method
  (syntax-rules ()
    ((_ methods ...)
     (%proper-method '(methods ...)))))

(define (socket? obj) ::boolean
  (or (instance? obj java.net.Socket)
      (instance? obj java.net.DatagramSocket)
      (instance? obj java.net.ServerSocket)))

(define (socket-accept s::java.net.ServerSocket) ::java.net.Socket
  (s:accept))

(define socket-send
  (make-procedure
   
   (lambda (s::java.net.Socket bv::bytevector
                               #!optional (flags (message-type none)))
     (if (or (eq? flags '*msg-oob*)
             (and (list? flags)
                  (memq '*msg-oob* flags)))
         (if (= (u8vector-length bv) 1)
             (begin
               (s:sendUrgentData (u8vector-ref bv 0))
               1)
             (error "Can only send 1 byte of OOB data"))                   
         (let ((os (s:getOutputStream)))
           (os:write (bv:getBuffer) 0 (u8vector-length bv))
           (u8vector-length bv))))

   (lambda (s::java.net.DatagramSocket bv::bytevector
                                       #!optional (flags (message-type none)))
     (let ((packet (java.net.DatagramPacket (bv:getBuffer)
                                            0
                                            (u8vector-length bv)
                                            (s:getRemoteSocketAddress))))
       (s:send packet)
       (packet:getLength)))))

(define socket-recv
  (make-procedure
   (lambda (s::java.net.Socket size::int
                               #!optional (flags (message-type none)))
     (let* ((raw (byte[] length: size))
            (is (s:getInputStream))
            (count (is:read raw)))
       (if (= count -1)
           (u8vector)
           (make <u8vector> raw 0 count))))

   (lambda (s::java.net.DatagramSocket size::int
                                      #!optional (flags (message-type none)))
     (let* ((raw (byte[] length: size))
            (packet (java.net.DatagramPacket raw size)))
       (s:receive packet)
       (make <u8vector> (packet:getData) 0 (packet:getLength))))))

(define (socket-shutdown s::java.net.Socket how)
  (case how
    ((*shut-rd*)
     (s:shutdownInput))
    ((*shut-wr*)
     (s:shutdownOutput))
    ((*shut-rdwr*)
     (s:shutdownInput)
     (s:shutdownOutput))))

(define socket-close
  (make-procedure
   (lambda (s::java.net.Socket) (s:close))
   (lambda (s::java.net.ServerSocket) (s:close))
   (lambda (s::java.net.DatagramSocket) (s:close))))

(define (socket-input-port s::java.net.Socket)
  (s:getInputStream))

(define (socket-output-port s::java.net.Socket)
  (s:getOutputStream))

(define (call-with-socket s proc)
  (let ((result (proc s)))
    (socket-close s)
    result))

(define (%service->port service) ::int
  (cond
   ((integer? service)
    service)
   ((string? service)
    (let ((asnum (string->number service)))
      (if asnum
          (if (integer? asnum)
              asnum
              (error "port numbers must be integers"))
          (let ((url (java.net.URL service "" "")))
            (url:getDefaultPort)))))
   (else
    (error "invalid service argument"))))

(define (%return-matching-addr addresses::java.net.InetAddress[] family n) ::java.net.InetAddress
  (cond
   ((>= n addresses:length)
    (error "No address found."))
   ((eq? family '*af-inet*)
    (if (instance? (addresses n) java.net.Inet4Address)
        (addresses n)
        (%return-matching-addr addresses family (+ n 1))))
   ((eq? family '*af-inet6*)
    (if (instance? (addresses n) java.net.Inet6Address)
        (addresses n)
        (%return-matching-addr addresses family (+ n 1))))))

(define (%host->address hostname::string family) ::java.net.InetAddress
  (case family
    ((*af-unspec*)
     (java.net.InetAddress:getByName hostname))
    ((*af-inet* *af-inet6*)
     (%return-matching-addr (java.net.InetAddress:getAllByName hostname)
                            family 0))
    (else
     (error "Unsupported socket family"))))

(define (make-client-socket node::string service
                            #!optional
                            (ai-family '*af-inet*)
                            (ai-socktype '*sock-stream*)
                            (ai-flags (socket-merge-flags '*ai-v4mapped* '*ai-addrconfig*))
                            (ai-protocol '*ipproto-ip*))
  (let ((addr (%host->address node ai-family))
        (port (%service->port service)))
    (cond
     ((eq? ai-socktype '*sock-stream*)
      (java.net.Socket addr port))
     ((eq? ai-socktype '*sock-dgram*)
      (let ((udp (java.net.DatagramSocket)))
        (udp:connect addr port)
        udp))
     (else
      (error "Unsupported socket type")))))

(define (%family->addr family) ::java.net.InetAddress
  (case family
    ((*af-inet*)
     (java.net.InetAddress:getByName "0.0.0.0"))
    ((*af-inet6*)
     (java.net.InetAddress:getByName "::"))
    (else
     (error "Unsupported socket family"))))

(define (make-server-socket service
                            #!optional
                            (ai-family '*af-inet*)
                            (ai-socktype '*sock-stream*)
                            (ai-protocol '*ipproto-ip*))
  (let ((port (%service->port service)))
    (cond
     ((eq? ai-socktype '*sock-stream*)
      (if (eq? ai-family '*af-unspec*)
          (java.net.ServerSocket port)
          (java.net.ServerSocket port 50 (%family->addr ai-family))))
     ((eq? ai-socktype '*sock-dgram*)
      (if (eq? ai-family '*af-unspec*)
          (java.net.DatagramSocket port)
          (java.net.DatagramSocket port (%family->addr ai-family))))
     (else
      (error "Unsupported socket type")))))

