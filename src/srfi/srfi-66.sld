;; u8vector extensions. -*- scheme -*-

;; Copyright (C) Michael Sperber (2005). All Rights Reserved.  Permission
;; is hereby granted, free of charge, to any person obtaining a copy of
;; this software and associated documentation files (the "Software"), to
;; deal in the Software without restriction, including without limitation
;; the rights to use, copy, modify, merge, publish, distribute,
;; sublicense, and/or sell copies of the Software, and to permit persons
;; to whom the Software is furnished to do so, subject to the following
;; conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
;; LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;; OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;; WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(define-library (srfi 66)
  (export
   ;; existing u8vector functions
   u8vector? make-u8vector u8vector u8vector->list list->u8vector
   u8vector-length u8vector-ref u8vector-set!
   ;; new functions
   u8vector-copy u8vector-copy!
   u8vector=? u8vector-compare)

  (import (scheme base))
  (import (only uniform
                u8vector? make-u8vector u8vector u8vector->list
                list->u8vector u8vector-length u8vector-ref u8vector-set!))
  (import (only uvector-utils
                u8vector=? u8vector-copy))
  (import (only bytevectors
                bytevector-copy!))

  (define (u8vector-copy! from from-start to to-start n)
    (bytevector-copy! to to-start from from-start n))
  
  #;(define (u8vector=? u8vector-1 u8vector-2)
    (let ((size (u8vector-length u8vector-1)))
      (and (= size (u8vector-length u8vector-2))
           (let loop ((i 0))
             (or (>= i size)
                 (and (= (u8vector-ref u8vector-1 i)
                         (u8vector-ref u8vector-2 i))
                      (loop (+ 1 i))))))))
  
  (define (u8vector-compare u8vector-1 u8vector-2)
    (let ((length-1 (u8vector-length u8vector-1))
          (length-2 (u8vector-length u8vector-2)))
      (cond
       ((< length-1 length-2) -1)
       ((> length-1 length-2)  1)
       (else
        (let loop ((i 0))
          (if (= i length-1)
              0
              (let ((elt-1 (u8vector-ref u8vector-1 i))
                    (elt-2 (u8vector-ref u8vector-2 i)))
                (cond ((< elt-1 elt-2) -1)
                      ((> elt-1 elt-2)  1)
                      (else (loop (+ i 1)))))))))))
  )
