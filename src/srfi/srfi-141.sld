;;; srfi-141 integer division -*- scheme -*-

(define-library (srfi 141)
  (export
   floor/ floor-quotient floor-remainder
   ceiling/ ceiling-quotient ceiling-remainder
   truncate/ truncate-quotient truncate-remainder
   round/ round-quotient round-remainder
   euclidean/ euclidean-quotient euclidean-remainder
   balanced/ balanced-quotient balanced-remainder)

  (import
   (rename (kawa base)
           (div-and-mod euclidean/)
           (div euclidean-quotient)
           (mod euclidean-remainder)
           (div0-and-mod0 round/)
           (div0 round-quotient)
           (mod0 round-remainder)))

   (begin
     (define (ceiling/ x::real y::real)
       (let* ((q (ceiling (/ x y)))
             (r (- x (* q y))))
         (values q r)))
     (define (ceiling-quotient x::real y::real) :: real
       (ceiling (/ x y)))
     (define (ceiling-remainder x::real y::real) :: real
       (let-values (((q r) (ceiling/ x y))) r))

     (define (balanced/ (x :: real) (y :: real))
       (let-values (((q r) (round/ x y)))
         (cond ((< r (abs (/ y 2)))
                (values q r))
               ((> y 0)
                (values (+ q 1) (- x (* (+ q 1) y))))
               (else
                (values (- q 1) (- x (* (- q 1) y)))))))
     (define (balanced-quotient (x :: real) (y :: real))
       (let-values (((q r) (balanced/ x y))) q))
     (define (balanced-remainder (x :: real) (y :: real))
       (let-values (((q r) (balanced/ x y))) r))
     ))
