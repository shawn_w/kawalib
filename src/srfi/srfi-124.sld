;;; -*- scheme -*-

(define-library (srfi 124)
  (export
   ephemeron? make-ephemeron ephemeron-broken?
   ephemeron-key ephemeron-datum)
  (import (scheme base))
  (begin
    
    (define-record-type weak-ref
      (make-weak-ref key datum)
      ephemeron?
      (key weak-key)
      (datum weak-datum))
    
    (define (make-ephemeron k d)
      (make-weak-ref (java.lang.ref.WeakReference k)
                     (java.lang.ref.WeakReference d)))
    
    (define (ephemeron-broken? em)
      (let ((k :: java.lang.ref.WeakReference (weak-key em))
            (d :: java.lang.ref.WeakReference (weak-datum em)))
        (or (null? (k:get)) (null? (d:get)))))
    
    (define (ephemeron-key em)
      (let* ((kr :: java.lang.ref.WeakReference (weak-key em))
             (k (kr:get)))
        (if (null? k) #f k)))
  
    (define (ephemeron-datum em)
      (let* ((dr :: java.lang.ref.WeakReference (weak-datum em))
             (d (dr:get)))
        (if (null? d) #f d)))

  ))
