;;; SRFI-111: Boxes -*- scheme -*-

;; Code by Shawn

(module-compile-options warn-unknown-member: #f)
(define-library (srfi 111)
  (export box? unbox
          box sbox? sbox-ref set-box! 
          ibox ibox? ibox-ref
          wbox wbox? wbox-ref wbox-set!)
  (import (scheme base))
  
  (begin   
    (define-record-type sbox-type
      (box value)
      sbox?
      (value sbox-ref set-box!))

    (define-record-type ibox-type
      (ibox value)
      ibox?
      (value ibox-ref))

    (define-record-type wbox-type
      (make-wbox value)
      wbox?
      (value real-wbox-ref real-wbox-set!))

    (define (box? val)
      (or (sbox? val) (ibox? val) (wbox? val)))

    (define (unbox val)
      (cond
       ((sbox? val) (sbox-ref val))
       ((ibox? val) (ibox-ref val))
       ((wbox? val) (wbox-ref val))
       (else val)))
  
    (define (wbox val)
      (make-wbox (java.lang.ref.WeakReference val)))

    (define (wbox-ref bx)
      (let* ((val :: java.lang.ref.WeakReference (real-wbox-ref bx))
             (stored (val:get)))
        (if (null? stored)
            #f
            stored)))

    (define (wbox-set! bx val)
      (real-wbox-set! bx (java.lang.ref.WeakReference val)))

    ))
