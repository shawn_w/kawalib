;;; Implementation of SRFI-27: Sources of Random Bits, for kawa scheme.
;;;
;;; See SRFI document at https://srfi.schemers.org/srfi-27/srfi-27.html
;;;
;;; Written by Shawn Wagner <shawnw.mobile@gmail.com> except where otherwise noted.
;;;

(module-name (srfi 27))
(module-export random-integer random-real default-random-source make-random-source
    random-source? random-source-state-ref random-source-state-set!
    random-source-randomize! random-source-pseudo-randomize! random-source-make-integers
    random-source-make-reals default-cs-random-source random-source-make-reals2 random-real2
    random-source-make-normals random-normal)
(require <kawa.lib.prim_imports>)
(require <kawa.lib.std_syntax>)
(require <kawa.lib.syntax>)
(require <kawa.lib.case_syntax>)
(require <kawa.lib.uniform>)
(require <kawa.lib.vectors>)
(require <kawa.lib.lists>)
(require <kawa.lib.exceptions>)
(require <kawa.lib.numbers>)
(require <kawa.lib.rnrs.arithmetic.bitwise>)

(import (srfi 74) (marshal))

;; Interface for all random-source objects
(define-simple-class random-source ()
  ((next-int (n ::int)) :: int #!abstract)
  ((next-long) :: long #!abstract)
  ((next-long (n :: long)) :: long (build-random-long (this) n))
  ((next-bytes (n :: int)) :: byte[] #!abstract)
  ((next-double) :: double #!abstract)
  ((next-float) :: float #!abstract)
  ((seed-length) :: int #!abstract)
  ((seed! (seed ::s64vector)) :: #!void #!abstract)
  ((state-ref) ::u8vector #!abstract)
  ((state-set! (state ::u8vector)) :: #!void #!abstract))
(define-alias rsi random-source)

; Return a long in the range [0,maxval)
(define (build-random-long rs::rsi maxval::long) :: long
  (let ((limit (- java.lang.Long:MAX_VALUE (modulo java.lang.Long:MAX_VALUE maxval))))
      (do ((val (rs:next-long) (rs:next-long)))
          ((and (> val 0) (< val limit)) (modulo val maxval)))))
  
;; random source wrapping java.util.Random
(define-alias juR java.util.Random)
(define-simple-class juRandom (random-source)
    (gen type: juR)
    ((*init* (rng ::juR)) (set! gen rng))
    ((*init*) (set! gen (juR)))
    ((*init* (seed ::long)) (set! gen (juR seed)))
    ((next-int (n ::int)) :: int (gen:nextInt n))
    ((next-long) :: long (gen:nextLong))
    ((next-bytes (n :: int)) :: byte[]
        (let ((arr (byte[] length: n)))
            (gen:nextBytes arr)
            arr))
    ((next-double) :: double (gen:nextDouble))
    ((next-float) :: float  (gen:nextFloat))
    ((seed-length) :: int 1) 
    ((seed! (s ::s64vector)) (gen:setSeed (s64vector-ref s 0)))
    ((state-ref) :: u8vector (serialize gen))
    ((state-set! (state ::u8vector))
        (set! gen (as juR (serialized->object state)))))

;; Return a new instance of a java.util.Random source
(define (make-native-random-source #!optional seed) :: rsi
        (if seed
            (juRandom (as long seed))
            (juRandom)))

;; Wrapper for java.util.SplittableRandom
(define-alias juSR java.util.SplittableRandom)
(define-simple-class juSplitRandom (random-source)
  (gen type: juSR)
  ((*init* (rng ::juSR)) (set! gen rng))
  ((*init*) (set! gen (juSR)))
  ((*init* (seed ::long)) (set! gen (juSR seed)))
  ((next-int (n ::int)) :: int (gen:nextInt n))
  ((next-long) :: long (gen:nextLong))
  ((next-long (n ::long)) :: long (gen:nextLong n))
  ((next-bytes (n :: int)) :: byte[]
   (let ((arr (make-u8vector n)))
     (do ((i 0 (+ i 4))
          (bytes-left n (- bytes-left 4)))
         ((< bytes-left 4)
          (unless (= bytes-left 0)
                  (let ((last (make-u8vector 4)))
                    (blob-s32-native-set! last 0 (gen:nextInt))
                    (case bytes-left
                      ((1) (u8vector-set! arr (- n 1) (u8vector-ref last 0)))
                      ((2)
                       (u8vector-set! arr (- n 1) (u8vector-ref last 0))
                       (u8vector-set! arr (- n 2) (u8vector-ref last 1)))
                      ((3)
                       (u8vector-set! arr (- n 1) (u8vector-ref last 0))
                       (u8vector-set! arr (- n 2) (u8vector-ref last 1))
                       (u8vector-set! arr (- n 3) (u8vector-ref last 2)))))
                  ))
       (blob-s32-native-set! arr i (gen:nextInt)))
     (univector->array arr)))
  ((next-double) :: double (gen:nextDouble))
  ((next-float) :: float  (as float (gen:nextDouble)))
  ((seed-length) :: int 1) 
  ((seed! (s ::s64vector)) (set! gen (juSR (s64vector-ref s 0))))
  ((state-ref) :: u8vector
   (error "java.util.SplittableRandom is not serializable!"))
  ((state-set! (state ::u8vector))
   (error "java.util.SplittableRandom is not serializable!")))

;; Return a new instance of a java.util.SplittableRandom source
(define (make-splitmix-random-source #!optional seed) :: rsi
        (if seed
            (juSplitRandom (as long seed))
            (juSplitRandom)))

;; Random integer generator helper functions.

; Return an N-byte big integer made from a byte array. mask is anded against the first
; element of the array.
(define-syntax u ; Coerce a byte into an unsigned integer
    (syntax-rules ()
        ((_ n) (bitwise-and #xFF (as ubyte n)))))
(define (build-int-from-bytes bv::byte[] mask) :: integer
     (let ((first-val (bitwise-and
                        (as ubyte (bv 0))
                        (bitwise-bit-field #xFF 0 (if (= mask 0) 8 mask)))))
            (do ((n 1 (+ n 1))
                 (val first-val (+ (bitwise-arithmetic-shift-left val 8) (u (bv n)))))
                 ((= n bv:length) val))))

; Return an integer in the range [0,maxval), with no limit on the size of maxval
(define (build-random-integer rs::rsi maxval::integer) :: integer
      (let*-values (((bits) (bitwise-length maxval))
                    ((quot mask) (truncate/ bits 8))
                    ((len) (+ quot (if (> mask 0) 1 0)))
                    ((maxbitnum) (- (expt 2 bits) 1)) ; Number with bits bits, all set to 1
                    ((limit) (- maxbitnum (modulo maxbitnum maxval)))
                    ((gen-num) (lambda () (build-int-from-bytes (rs:next-bytes len) mask))))
          (do ((val (gen-num) (gen-num)))
              ((< val limit) (modulo val maxval)))))

;; Support for generators that don't natively produce floating point numbers

; Turn a 32 bit int into a float
;(define (int->float i::int) :: float
;    (java.lang.Math:scalb (as float (abs i)) -32))

; Turn a 64 bit long into a double
;(define (long->double i::long) :: double
;    (java.lang.Math:scalb (as double (abs i)) -64))

;; Cryptographically secure RNG from java.util.SecureRandom works with the 
;; SRFI-27 interface.

(define-alias jsSR java.security.SecureRandom)
(define-simple-class jsSRandom (juRandom)
  ((*init*) (invoke-special juRandom (this) '*init* (jsSR)))
  ((*init* (algo ::string))
   (invoke-special juRandom (this) '*init* (jsSR:getInstance algo)))
  ((*init* (algo ::string) (prov ::string))
   (invoke-special juRandom (this) '*init* (jsSR:getInstance algo prov)))
  ((seed-length) :: int 2)
  ((seed! s ::s64vector) ; SecureRandom setSeed adds to randomness, doesn't reset it
   (gen:setSeed (s64vector-ref s 0))
   (gen:setSeed (s64vector-ref s 1)))
  ((generate-seed (n ::int)) :: byte[]
   (let ((secgen (as jsSR gen)))
     (secgen:generateSeed n))))

;; Return a new instance of java.security.SecureRandom. See java docs
;; for description of the algorithm and provider arguments.
(define (make-cs-random-source #!optional algorithm provider)
        (cond
            ((and (eq? algorithm #f) (eq? provider #f))
                (jsSRandom))
            ((and (string? algorithm) (eq? provider #f))
                (jsSRandom algorithm))
            ((and (string? algorithm) (string? provider))
                (jsSRandom algorithm provider))
            (else
                (error "Invalid arguments to make-cs-random-source" algorithm provider))))

;; The default cryptographically secure generator, used by random-source-randomize!
(define default-cs-random-source :: jsSRandom (jsSRandom))

;; Seed helper functions.

; Securely generate N 64-bit seeds and return them in a u64vector.
(define (gen-seeds n crs::jsSRandom) :: s64vector
  (let ((seeds (make-s64vector n 0))
        (raw-bytes (bytearray->bytevector (crs:generate-seed (* n 8)))))
    (do ((i 0 (+ i 1)))
        ((= i n) seeds)
      (s64vector-set! seeds i
                      (blob-s64-native-ref raw-bytes (* i 8))))))

; Try to combine 2 32 bit ints into one 64 bit long
(define (mix i j) :: long
  (if (and (<= i java.lang.Integer:MAX_VALUE)
           (<= j java.lang.Integer:MAX_VALUE))
      #;(let ((blob (make-blob 8)))
        (blob-u32-native-set! blob 0 i)
        (blob-u32-native-set! blob 4 j)
        (blob-s64-native-ref blob 0))
      (+ (bitwise-arithmetic-shift-left (as long i) 32) (as long j))

      (as long (+ i j))))
            
;;; SRFI-27 functions

;; Given a random source, returns a function that takes one argument N and returns
;; an integer in the range [0,N)
(define (random-source-make-integers rs::rsi)
    (lambda (n::integer)
        (cond
            ((<= n 0)
                (error "random-source-make-integers called with negative bound" n))
            ((<= n java.lang.Integer:MAX_VALUE) ; Special case values that fit in 32bit integers
                (rs:next-int n))
            ((<= n java.lang.Long:MAX_VALUE) ; Same for 64bit ones.
                (rs:next-long n))
            (else ; Deal with arbitrary precision integers
                (build-random-integer rs n))))) 

;; Given a random source, returns a function that takes no arguments and
;; returns a real in the range (0,1).
;; The optional units argument can be: 
;;  `double to make it return doubles. This is the default.
;;  `float to make it return floats
;;  A number in the range (0,1) such that "the numbers created by rand are of
;;  the same numerical type as unit and the potential output values are spaced
;;  by at most unit." In practice, returns a double unless given a float arg.

(define-syntax skip-zero (syntax-rules ()
((_ rng)
    (do ((num (rng) (rng)))
        ((> num 0.0) num)))))
(define (random-source-make-reals rs::rsi #!optional (units 'double))
   (cond
    ((or (eq? units 'float)
         (and (float? units) (> units 0.0) (< units 1.0)))
        (lambda () (skip-zero rs:next-float)))
    ((or (eq? units 'double)
         (and (real? units) (> units 0.0) (< units 1.0)))
        (lambda () (skip-zero rs:next-double)))
    (else (error "invalid units argument to random-source-make-reals" units))))

;; Return the state of a random source in a serialized form.
;; The state can be used to recreate a generator of the same type when used with
;; random-source-set-state!
(define (random-source-state-ref rs::rsi) (rs:state-ref))

;; Reset the state of a random source to a previously saved one. The state
;; must have been returned by random-source-state-ref with a random source of the
;; same type or behavior is undefined.
(define (random-source-state-set! rs::rsi s::u8vector) (rs:state-set! s))

;; (random-source-randomize! rs)
;;  Seed the random-source with a high quality random seed, using
;;  java.security.SecureRandom 
(define (random-source-randomize! rs::rsi #!optional (crs default-cs-random-source))
    (rs:seed! (gen-seeds (rs:seed-length) crs)))

;  Changes the state of the random source s into the initial state of the (i,
;  j)-th independent random source, where i and j are non-negative integers. This
;  procedure provides a mechanism to obtain a large number of independent random
;  sources (usually all derived from the same backbone generator), indexed by two
;  integers. In contrast to random-source-randomize!, this procedure is entirely
;  deterministic.           
(define (random-source-pseudo-randomize! rs::rsi i::integer j::integer)
    (if (or (< i 0) (< j 0)) (error "random-source-pseudo-randomze! seeds must be positive"))
    (case (rs:seed-length)
        ((1) (rs:seed! (s64vector (mix i j))))
        ((2) (rs:seed! (s64vector i j)))
        (else
            (error "random-source-psuedo-randomize! is not compatible with this random source"))))

;; Make a new random source
;; (make-random-source), (make-random-source 'native) -> new java.util.Random with default constructor
;; (make-random-source ['native] SEED) -> new Random with given seed, which must be an integer.
;; (make-random-source 'splitmix [SEED]) - new java.util.SplittableRandom with given seed, which must be an integer.
;; (make-random-source 'crypto ["algorithm" ["provider"]]) -> new java.security.SecureRandom source
;; Other RNG engines might be added later.

(define-syntax maybe-seed
  (syntax-rules ()
    ((_ gen seed)
     (if (null? seed)
         (gen)
         (apply gen seed)))))
(define (make-random-source . args) :: rsi
  (cond
   ((null? args) (make-native-random-source))
   ((and (integer? (car args)) (null? (cdr args)))
    (make-native-random-source (car args)))
   ((eq? (car args) 'native)
    (maybe-seed make-native-random-source (cdr args)))
   ((eq? (car args) 'splitmix)
    (maybe-seed make-splitmix-random-source (cdr args)))
   ((eq? (car args) 'crypto)
    (maybe-seed make-cs-random-source (cdr args)))
   (else
    (error "unknown arguments to make-random-source" args))))

;; The default random source.
(define default-random-source (make-native-random-source))

;; (random-integer N) returns a random integer from [0,N) using the default
;; source.
(define random-integer (random-source-make-integers default-random-source))

;; (random-real N) returns a random double in the range (0,1) using the
;; default source.
(define random-real (random-source-make-reals default-random-source))

;; True if a variable holds a random source object
(define (random-source? rs) :: boolean (instance? rs random-source))

;;; Non SRFI-27 routines:

;; Given a random source, returns a function that takes no arguments and
;; returns a real in the range [0,1).
;; The optional units argument can be: 
;;  `double to make it return doubles. This is the default.
;;  `float to make it return floats
;;  A number in the range (0,1) such that "the numbers created by rand are of
;;  the same numerical type as unit and the potential output values are spaced
;;  by at most unit." In practice, returns a double unless given a float arg.
(define (random-source-make-reals2 rs::rsi #!optional (units 'double))
   (cond
    ((or (eq? units 'float)
         (and (float? units) (> units 0.0) (< units 1.0)))
        (lambda () (rs:next-float)))
    ((or (eq? units 'double)
         (and (real? units) (> units 0.0) (< units 1.0)))
        (lambda () (rs:next-double)))
    (else (error "invalid units argument to random-source-make-reals" units))))

;; (random-real2 N) returns a random double in the range [0,1) using the
;; default source.
(define random-real2 (random-source-make-reals2 default-random-source))

;; Following functions are from the SRFI's 'recommended usage' section

;  Copyright (C) Sebastian Egner (2002). All Rights Reserved.

;  Permission is hereby granted, free of charge, to any person obtaining a copy of
;  this software and associated documentation files (the "Software"), to deal in
;  the Software without restriction, including without limitation the rights to
;  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
;  of the Software, and to permit persons to whom the Software is furnished to do
;  so, subject to the following conditions:

;  The above copyright notice and this permission notice shall be included in all
;  copies or substantial portions of the Software.

;  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;  SOFTWARE.

;; Returns a generator that takes two optional arguments mu and sigma and returns
;; N(mu, sigma)-distributed reals. mu and sigma default to 0.0 and 0.5 if not
;; provided. (Making the arguments optional was added by Shawn)
(define (random-source-make-normals s . unit)
(let ((rand (apply random-source-make-reals s unit))
  (next #f))
  (lambda (#!optional (mu 0.0) (sigma 0.5))
    (if next
    (let ((result next))
      (set! next #f)
      (+ mu (* sigma result)))
    (let loop ()
      (let* ((v1 (- (* 2 (rand)) 1))
         (v2 (- (* 2 (rand)) 1))
         (s (+ (* v1 v1) (* v2 v2))))
        (if (>= s 1)
        (loop)
        (let ((scale (sqrt (/ (* -2 (log s)) s))))
          (set! next (* scale v2))
          (+ mu (* sigma scale v1))))))))))

;; (random-normal [mu sigma]) returns a N(mu, sigma)-distributed double.
;; mu and sigma default to 0.0 and 0.5 if not provided.
(define random-normal (random-source-make-normals default-random-source))

