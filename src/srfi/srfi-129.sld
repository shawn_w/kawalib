;;; -*- scheme -*-
;;; These are already in scheme; wrap in a library for cond-expand, import,
;;; etc.

(define-library (srfi 129)
  (export char-title-case? char-titlecase string-titlecase)
  (import (only (rnrs unicode)
                char-title-case? char-titlecase string-titlecase)))
                
