;; Copyright (C) 2020 Wolfgang Corcoran-Mathe
;; Sections (C) 2022 Shawn Wagner

;; Permission is hereby granted, free of charge, to any person obtaining a
;; copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:

;; The above copyright notice and this permission notice shall be included
;; in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;; TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


;; Maybe is implemented in terms of java.util.Optional
;; Either uses the reference implementation.


;; Utilities
(define-syntax const
  (syntax-rules ()
    ((_ obj) (lambda _ obj))))

(define (singleton? lis)
  (and (pair? lis) (null? (cdr lis))))

(define-syntax fast-apply
  (syntax-rules ()
    ((_ proc args)
     (if (singleton? args) (proc (car args)) (apply proc args)))))

(define-syntax fast-list->values
  (syntax-rules ()
    ((_ vals)
     (if (singleton? vals) (car vals) (apply values vals)))))


;; Maybe

(define (maybe? obj) ::boolean (java.util.Optional? obj))
(define (nothing? obj) ::boolean
  (and (java.util.Optional? obj)
       (let ((opt::java.util.Optional (->java.util.Optional obj)))
         (opt:isEmpty))))
(define (just? obj) ::boolean
    (and (java.util.Optional? obj)
         (let ((opt::java.util.Optional (->java.util.Optional obj)))
           (opt:isPresent))))

(define (nothing) ::java.util.Optional (java.util.Optional:empty))
(define (just . args) ::java.util.Optional (java.util.Optional:of args))
(define (list->just lst::list) ::java.util.Optional
    (java.util.Optional:of lst))

(define (maybe= equal::procedure . maybes) ::boolean
  (assume (pair? maybes))
  (let ((maybe1 (car maybes)))
    (every (lambda (maybe2) (%maybe=2 equal maybe1 maybe2))
           (cdr maybes))))

;; Compare two Maybes.
(define (%maybe=2 equal::procedure maybe1::java.util.Optional maybe2::java.util.Optional) ::boolean
  (or (and (maybe1:isEmpty) (maybe2:isEmpty))
      (and (maybe1:isPresent)
           (maybe2:isPresent)
           (list= equal (maybe1:orElseThrow) (maybe2:orElseThrow)))))

(define (maybe-ref maybe::java.util.Optional failure::procedure
                   #!optional (success::procedure values))
  (if (maybe:isPresent)
      (fast-apply success (maybe:orElseThrow))
      (failure)))

(define (maybe-ref/default maybe::java.util.Optional . default)
  (fast-apply values (maybe:orElse default)))

(define (maybe-join maybe::java.util.Optional) ::java.util.Optional
  (if (maybe:isPresent)
      (maybe-ref maybe (lambda () maybe)
                 (lambda (val)
                   (if (maybe? val)
                       val
                       (error "Argument must be nested maybe"))))
      maybe))

;; Call the first mproc on the payload of a Maybe, producing a Maybe.
;; Repeat the operation with this Maybe and the remaining mprocs.
;; Return a Nothing immediately.
(define (maybe-bind maybe::java.util.Optional mproc::procedure . mprocs)
  (if (null? mprocs)
      (maybe-ref maybe nothing mproc)  ; fast path
      (let lp ((m maybe) (mp mproc) (mprocs mprocs))
        (maybe-ref m
                   nothing
                   (lambda objs
                     (if (null? mprocs)
                         (fast-apply mp objs) ; tail-call last
                         (lp (fast-apply mp objs)
                             (car mprocs)
                             (cdr mprocs))))))))

(define (maybe-compose . mprocs)
  (assume (pair? mprocs))
  (lambda args
    (let lp ((args args) (mproc (car mprocs)) (rest (cdr mprocs)))
      (if (null? rest)
          (fast-apply mproc args)             ; tail-call last
          (maybe-ref (fast-apply mproc args)
                     nothing
                     (lambda objs
                       (lp objs (car rest) (cdr rest))))))))


;; Traverse a container of Maybes with cmap, collect the payload
;; objects with aggregator, and wrap the new collection in a Just.
;; If a Nothing is encountered while traversing, return it
;; immediately.
(define maybe-sequence
  (case-lambda
   ((container cmap) (maybe-sequence container cmap list))
   ((container cmap aggregator)
    (call-with-current-continuation
     (lambda (return)
       (just (cmap (lambda (m)
                     (maybe-ref m (lambda () (return m)) aggregator))
                   container)))))))

(define (maybe-length maybe::java.util.Optional) ::int
  (if (maybe:isPresent) 1 0))

(define (maybe-filter pred::procedure maybe::java.util.Optional) ::java.util.Optional
  (maybe:filter (lambda (vals) (fast-apply pred vals))))

(define (maybe-remove pred::procedure maybe::java.util.Optional) ::java.util.Optional
  (maybe:filter (lambda (vals) (not (fast-apply pred vals)))))


(define (maybe->list maybe::java.util.Optional) ::list
  (if (maybe:isPresent)
      (maybe:orElseThrow)
      (list)))

(define (list->maybe lst::list) ::java.util.Optional
  (if (null? lst)
      (java.util.Optional:empty)
      (java.util.Optional:of lst)))

(define (maybe->truth maybe::java.util.Optional)
  (if (maybe:isPresent)
      (car (maybe:orElseThrow))
      #f))

(define (truth->maybe obj) ::java.util.Optional
  (if obj
      (java.util.Optional:of (list obj))
      (java.util.Optional:empty)))

(define (maybe->list-truth maybe::java.util.Optional) (maybe:orElse #f))

(define (list-truth->maybe lst) ::java.util.Optional
  (assume (or (eq? lst #f) (pair? lst)))
  (if (eq? lst #f)
      (java.util.Optional:empty)
      (java.util.Optional:of lst)))

(define (maybe->generation maybe::java.util.Optional)
  (if (maybe:isPresent)
      (car (maybe:orElseThrow))
      (eof-object)))

(define (generation->maybe obj) ::java.util.Optional
  (if (eof-object? obj)
      (java.util.Optional:empty)
      (java.util.Optional:of (list obj))))

;;; These procedures interface between the Maybe/Either protocols and
;;; the values protocol, which returns one or more values to represent
;;; success and zero values to represent failure.

(define (maybe->values maybe::java.util.Optional)
  (maybe-ref maybe values values))

(define (values->maybe producer::procedure)
  (call-with-values
   producer
   (lambda objs
     (if (null? objs) (nothing) (list->just objs)))))

;;; The following procedures interface between the Maybe protocol and
;;; the two-values protocol, which returns |#f, #f| to represent
;;; failure and |<any object>, #t| to represent success.

;; If maybe is Just containing a single value, return that value and #t.
;; If it's a Nothing, return |#f, #f|.
(define (maybe->two-values maybe::java.util.Optional)
  (if (nothing? maybe)
      (values #f #f)
      (begin
       (values (car (maybe:orElseThrow)) #t))))

(define (two-values->maybe producer::procedure)
  (call-with-values
   producer
   (lambda (obj success)
     (if success (just obj) (nothing)))))

(define (maybe-map proc::procedure maybe::java.util.Optional) ::java.util.Optional
  (maybe:map (lambda (objs)
               (call-with-values
                   (lambda () (fast-apply proc objs))
                 (lambda newobjs newobjs)))))

(define (maybe-for-each proc::procedure maybe::java.util.Optional)
  (maybe:ifPresent (lambda (objs) (fast-apply proc objs))))

;; If maybe is a Just, apply kons to its payload values and nil.
;; Otherwise, return nil.
(define (maybe-fold kons::procedure nil maybe::java.util.Optional)
  (if (maybe:isEmpty)
      nil
      (let ((objs (maybe:orElseThrow)))
        (if (singleton? objs)
            (kons (car objs) nil)
            (apply kons (append objs (list nil)))))))

;; If the seeds satisfy stop?, return Nothing.  Otherwise, call
;; successor on seeds and apply stop? to the results; if stop? returns
;; true, apply mapper to seeds and return the results wrapped in a Just.
(define (maybe-unfold stop?::procedure mapper::procedure successor . seeds) ::java.util.Optional
  (if (singleton? seeds)
      (let ((seed (car seeds)))  ; fast path
        (if (stop? seed)
            (nothing)
            (begin
             ;; successor might return multiple seeds.
             (assume (call-with-values (lambda () (successor seed)) stop?))
             (call-with-values (lambda () (mapper (car seeds))) just))))
      (if (apply stop? seeds)
          (java.util.Optional:empty)
          (begin
           (assume (call-with-values (lambda () (apply successor seeds))
                                     stop?))
           (call-with-values (lambda () (apply mapper seeds)) just)))))



;; Either

(define-record-type <left>
  (raw-left objs)
  left?
  (objs left-objs))

(define-record-type <right>
  (raw-right objs)
  right?
  (objs right-objs))

(define unspecified #!void)

;;;; Constructors

(define (left . objs)
  (raw-left objs))

(define (right . objs)
  (raw-right objs))

;;; List -> <container> equivalents of the basic constructors.

;;; Here and elsewhere, we copy caller-provided list arguments when
;;; passing them to raw-just/left/right, so that later mutation of the
;;; list argument doesn't affect the payloads of any Maybes/Eithers.

(define (list->right lis)
  (assume (or (null? lis) (pair? lis)))
  (raw-right (list-copy lis)))

(define (list->left lis)
  (assume (or (null? lis) (pair? lis)))
  (raw-left (list-copy lis)))

(define (maybe->either maybe::java.util.Optional . default-objs)
  (if (nothing? maybe)
      (raw-left default-objs)
      (raw-right (maybe:orElseThrow))))

(define (either->maybe either)
  (assume (either? either))
  (if (left? either)
      (java.util.Optional:empty)
      (java.util.Optional:of (right-objs either))))

(define (either-swap either)
  (assume (either? either))
  (if (right? either)
      (raw-left (right-objs either))
      (raw-right (left-objs either))))

;;;; Predicates


(define (either? obj)
  (or (left? obj) (right? obj)))

;; True if the eithers are all Lefts/all Rights containing the same
;; number of values which are element-wise equal in the sense of `equal'.
(define (either= equal . eithers)
  (assume (procedure? equal))
  (assume (pair? eithers))
  (let ((either1 (car eithers)))
    (every (lambda (either2) (%either=2 equal either1 either2))
           (cdr eithers))))

;; Compare two Eithers.
(define (%either=2 equal either1 either2)
  (let ((e= (lambda (acc) (list= equal (acc either1) (acc either2)))))
    (or (eqv? either1 either2)
        (and (left? either1) (left? either2) (e= left-objs))
        (and (right? either1) (right? either2) (e= right-objs)))))

;;;; Accessors

(define (%either-ref-single either accessor cont)
  (let ((objs (accessor either)))
    (fast-apply cont objs)))

;; Unwrap an Either, calling failure on the payload of a Left and the
;; optional success continuation (default: values) on that of a Right.
(define (either-ref either failure . %opt-args)
  (assume (either? either))
  (assume (procedure? failure))
  (if (right? either)
      (%either-ref-single either right-objs (if (pair? %opt-args)
                                                (car %opt-args)
                                                values))
      (%either-ref-single either left-objs failure)))

;; Unwrap an Either.  If it's a Left, return the default objects.
(define (either-ref/default either . default-objs)
  (assume (either? either))
  (if (right? either)
      (let ((objs (right-objs either)))
        (fast-list->values objs))
      (fast-list->values default-objs)))

;;;; Join and bind

;; If either is a Right containing a single Either, return that Either.
(define (either-join either)
  (assume (either? either))
  (if (left? either)
      either
      (let ((objs (right-objs either)))
        (assume (and (singleton? objs) (either? (car objs)))
                "either-join: invalid payload")
        (car objs))))

;; Call the first mproc on the payload of a Either, producing a Either.
;; Repeat the operation with this Either and the remaining mprocs.
;; Return a Left immediately.
(define (either-bind either mproc . mprocs)
  (assume (either? either))
  (if (null? mprocs)
      (either-ref either (const either) mproc)  ; fast path
      (let lp ((e either) (mp mproc) (mprocs mprocs))
        (either-ref e
                    (const e)
                    (lambda objs
                      (if (null? mprocs)
                          (fast-apply mp objs)  ; tail-call last
                          (lp (fast-apply mp objs)
                              (car mprocs)
                              (cdr mprocs))))))))

;; Compose the argument mprocs and return the resulting monadic procedure.
(define (either-compose . mprocs)
  (assume (pair? mprocs))
  (lambda args
    (let lp ((args args) (mproc (car mprocs)) (rest (cdr mprocs)))
      (if (null? rest)
          (fast-apply mproc args)              ; tail-call last
          (either-ref (fast-apply mproc args)
                      left
                      (lambda objs
                        (lp objs (car rest) (cdr rest))))))))

;;;; Sequence operations


(define (either-length either)
  (assume (either? either))
  (if (right? either) 1 0))

;; Return either if its payload satisfies pred; otherwise, return
;; a Left of the default objects.
(define (either-filter pred either . default-objs)
  (assume (procedure? pred))
  (assume (either? either))
  (if (and (right? either) (fast-apply pred (right-objs either)))
      either
      (raw-left default-objs)))

(define (either-remove pred either . default-objs)
  (assume (procedure? pred))
  (assume (either? either))
  (if (and (right? either)
           (not (fast-apply pred (right-objs either))))
      either
      (raw-left default-objs)))

;; Traverse a container of Eithers with cmap, collect the payload
;; objects with aggregator, and wrap the new collection in a Right.
;; If a Left is encountered while traversing, return it immediately.
(define either-sequence
  (case-lambda
   ((container cmap) (either-sequence container cmap list))
   ((container cmap aggregator)
    (assume (procedure? cmap))
    (assume (procedure? aggregator))
    (call-with-current-continuation
     (lambda (return)
       (right (cmap (lambda (e)
                      (either-ref e (const (return e)) aggregator))
                    container)))))))

;;;; Protocol conversion

(define (either->list either)
  (assume (either? either))
  ((if (right? either) right-objs left-objs) either))

(define (list->either lis . default-objs)
  (assume (or (null? lis) (pair? lis)))
  (if (null? lis)
      (raw-left default-objs)
      (raw-right (list-copy lis))))


;; If either is a Right, return its payload; otherwise, return false.
(define (either->truth either)
  (assume (either? either))
  (if (left? either)
      #f
      (begin
       (assume (singleton? (right-objs either))
               "either->truth: invalid payload")
       (car (right-objs either)))))

(define (truth->either obj . default-objs)
  (if obj (right obj) (raw-left default-objs)))

;;; These procedures interface between the Maybe protocol and the
;;; list-truth protocol, which uses #f to represent failure and a
;;; list to represent success.

(define (either->list-truth either)
  (assume (either? either))
  (if (right? either) (right-objs either) #f))

;; If list-or-false is #f, return a Left of the default objects.
;; If it's a list, return a Right containing its elements.
(define (list-truth->either list-or-false . default-objs)
  (if list-or-false
      (begin
       (assume (or (null? list-or-false) (pair? list-or-false)))
       (raw-right (list-copy list-or-false)))
      (raw-left default-objs)))

;;; The following procedures interface between the Maybe protocol and
;;; the generation protocol, which uses an EOF object to represent
;;; failure and any other value to represent success.

;; If either is a Right whose payload is a single value, return that
;; value.  If it's a Left, return an eof-object.
(define (either->generation either)
  (assume (either? either))
  (if (left? either)
      (eof-object)
      (begin
       (assume (singleton? (right-objs either))
               "either->generation: invalid payload")
       (car (right-objs either)))))

(define (generation->either obj . default-objs)
  (if (eof-object? obj) (raw-left default-objs) (right obj)))

;;; These procedures interface between the Maybe/Either protocols and
;;; the values protocol, which returns one or more values to represent
;;; success and zero values to represent failure.

;;; The following procedures interface between the Maybe protocol and
;;; the two-values protocol, which returns |#f, #f| to represent
;;; failure and |<any object>, #t| to represent success.

(define (either->values either)
  (either-ref/default either))

(define (values->either producer . default-objs)
  (assume (procedure? producer))
  (call-with-values
   producer
   (lambda objs
     (if (null? objs) (raw-left default-objs) (raw-right objs)))))

(define (exception->either pred thunk)
  (assume (procedure? pred))
  (assume (procedure? thunk))
  (guard (obj ((pred obj) (left obj)))
    (call-with-values thunk right)))

;;;; Map, fold, and unfold


;; If either is a Right, apply proc to its payload and wrap the result
;; in a Right.  Otherwise, return either.
(define (either-map proc either)
  (assume (procedure? proc))
  (assume (either? either))
  (if (left? either)
      either
      (call-with-values (lambda () (fast-apply proc (right-objs either)))
                        right)))

(define (either-for-each proc either)
  (assume (procedure? proc))
  (either-ref either (const #f) proc)
  unspecified)

;; If either is a Right, apply kons to its payload values and nil.
;; Otherwise, return nil.
(define (either-fold kons nil either)
  (assume (procedure? kons))
  (assume (either? either))
  (if (left? either)
      nil
      (let ((objs (right-objs either)))
        (if (singleton? objs)
            (kons (car objs) nil)
            (apply kons (append objs (list nil)))))))

;; If the seeds satisfy stop?, return a Left of seeds.  Otherwise, call
;; successor on seeds and apply stop? to the results; if stop? returns
;; true, apply mapper to seeds and return the results wrapped in a Right.
(define (either-unfold stop? mapper successor . seeds)
  (assume (procedure? stop?))
  (assume (procedure? mapper))
  (if (singleton? seeds)
      (let ((seed (car seeds)))  ; fast path
        (if (stop? seed)
            (raw-left seeds)
            (begin
             ;; successor might return multiple values.
             (assume (call-with-values (lambda () (successor seed)) stop?))
             (call-with-values (lambda () (apply mapper seeds)) right))))
      (if (apply stop? seeds)
          (raw-left seeds)
          (begin
           (assume (call-with-values (lambda () (apply successor seeds))
                                     stop?))
           (call-with-values (lambda () (apply mapper seeds)) right)))))

;;;; Syntax

(define-syntax maybe-if
  (syntax-rules ()
    ((_ maybe-expr just-expr nothing-expr)
     (let ((mval maybe-expr))
       (unless (maybe? mval)
         (error "maybe-if: ill-typed value"))
       (if (just? mval) just-expr nothing-expr)))))

;; Return the value of expr if it satisfies pred.
(define-syntax %guard-value
  (syntax-rules ()
    ((_ pred expr)
     (let ((val expr))
       (unless (pred val)
         (error "ill-typed value" pred val))
       val))))

;; Maybe analog of and.  Evaluate the argument expressions in order.
;; If any expression evaluates to Nothing, return it.  Otherwise,
;; return the last Just.
(define-syntax maybe-and
  (syntax-rules ()
    ((_) (just unspecified))
    ((_ maybe-expr) (%guard-value maybe? maybe-expr))
    ((_ maybe-expr maybe-expr* ...)
     (let ((maybe maybe-expr))
       (cond ((just? maybe) (maybe-and maybe-expr* ...))
             ((nothing? maybe) (java.util.Optional:empty))
             (else
              (error "maybe-and: ill-typed value" maybe? maybe)))))))

;; Maybe analog of or.  Evaluate the argument expressions in order.
;; If any expression evaluates to a Just, return it immediately.
;; Otherwise, return Nothing.
(define-syntax maybe-or
  (syntax-rules ()
    ((_) (nothing))
    ((_ maybe-expr) (%guard-value maybe? maybe-expr))
    ((_ maybe-expr maybe-expr* ...)
     (let ((maybe maybe-expr))
       (cond ((just? maybe) maybe)
             ((nothing? maybe) (maybe-or maybe-expr* ...))
             (else
              (error "maybe-or: ill-typed value" maybe? maybe)))))))

;; Maybe analog of SRFI 2's and-let*.  Each claw evaluates an expression
;; or bound variable to a Maybe, or binds the payload of the value of a
;; Maybe expression to a name in later claws and the body.  If any claw
;; gives a Nothing, the whole expression evaluates to Nothing.
(define-syntax maybe-let*
  (syntax-rules ()
    ((_ () expr1 expr2 ...)
     (call-with-values (lambda () expr1 expr2 ...) just))
    ((_ ((id maybe-expr) . claws) . body)
     (let ((maybe::java.util.Optional maybe-expr))
       (cond ((and (just? maybe) (singleton? (maybe:orElseThrow)))
              (let ((id (car (maybe:orElseThrow))))
                (maybe-let* claws . body)))
             ((nothing? maybe) (java.util.Optional:empty))
             (else (error "ill-typed value" maybe? maybe)))))
    ((_ ((maybe-expr) . claws) . body)
     (let ((maybe maybe-expr))
       (cond ((just? maybe) (maybe-let* claws . body))
             ((nothing? maybe) (java.util.Optional:empty))
             (else (error "ill-typed value" maybe? maybe)))))
    ((_ (id . claws) . body)
     (cond ((just? id) (maybe-let* claws . body))
           ((nothing? id) (java.util.Optional:empty))
           (else (error "ill-typed value" maybe? id))))
    ((_ . _)
     (syntax-error "ill-formed maybe-let* form"))))

;; Like maybe-let*, but a claw of the form (<formals> <maybe-expr>)
;; binds the payload of the value of <maybe-expr> to <formals> in the
;; manner of let-values.
(define-syntax maybe-let*-values
  (syntax-rules ()
    ((_ () expr1 expr2 ...)
     (call-with-values (lambda () expr1 expr2 ...) just))
    ((_ (((id id* ...) maybe-expr) . claws) . body)
     (maybe-bind (%guard-value maybe? maybe-expr)
                 (lambda (id id* ...)
                   (maybe-let*-values claws . body))))
    ((_ ((ids maybe-expr) . claws) . body)
     (maybe-bind (%guard-value maybe? maybe-expr)
                 (lambda ids
                   (maybe-let*-values claws . body))))
    ((_ ((maybe-expr) . claws) . body)
     (let ((maybe maybe-expr))
       (cond ((just? maybe) (maybe-let*-values claws . body))
             ((nothing? maybe) (java.util.Optional:empty))
             (else (error "ill-typed value" maybe? maybe)))))
    ((_ (id . claws) . body)
     (cond ((just? id) (maybe-let*-values claws . body))
           ((nothing? id) (java.util.Optional:empty))
           (else (error "ill-typed value" maybe? id))))
    ((_ . _)
     (syntax-error "ill-formed maybe-let*-values form"))))

;; Either analog of and.  Evaluate the argument expressions in order.
;; If any expression evaluates to a Left, return it immediately.
;; Otherwise, return the last Right.
(define-syntax either-and
  (syntax-rules ()
    ((_) (right unspecified))
    ((_ either-expr) (%guard-value either? either-expr))
    ((_ either-expr either-expr* ...)
     (let ((either either-expr))
       (cond ((right? either) (either-and either-expr* ...))
             ((left? either) either)
             (else (error "ill-typed value" either? either)))))))

;; Either analog of or.  Evaluate the argument expressions in order.
;; If any expression evaluates to a Right, return it immediately.
;; Otherwise, return the last Left.
(define-syntax either-or
  (syntax-rules ()
    ((_) (left unspecified))
    ((_ either-expr) (%guard-value either? either-expr))
    ((_ either-expr either-expr* ...)
     (let ((either either-expr))
       (cond ((right? either) either)
             ((left? either) (either-or either-expr* ...))
             (else (error "ill-typed value" either? either)))))))

;; Either analog of SRFI 2's and-let*.  Each claw evaluates an expression
;; or bound variable to a Maybe, or binds the payload of the value of an
;; Either expression to a name in later claws and the body.  If any claw
;; gives a Left, then the whole expression evaluates to that Left.
(define-syntax either-let*
  (syntax-rules ()
    ((_ () expr1 expr2 ...)
     (call-with-values (lambda () expr1 expr2 ...) right))
    ((_ ((id either-expr) . claws) . body)
     (let ((either either-expr))
       (cond ((and (right? either) (singleton? (right-objs either)))
              (let ((id (car (right-objs either))))
                (either-let* claws . body)))
             ((left? either) either)
             (else (error "ill-typed value" either? either)))))
    ((_ ((either-expr) . claws) . body)
     (let ((either either-expr))
       (cond ((right? either) (either-let* claws . body))
             ((left? either) either)
             (else (error "ill-typed value" either? either)))))
    ((_ (id . claws) . body)
     (cond ((right? id) (either-let* claws . body))
           ((left? id) id)
           (else (error "ill-typed value" either? either))))
    ((_ . _)
     (syntax-error "ill-formed either-let* form"))))

;; Like either-let*, but a claw of the form (<formals> <either-expr>)
;; binds the payload of the value of <either-expr> to <formals> in the
;; manner of let-values.
(define-syntax either-let*-values
  (syntax-rules ()
    ((_ () expr1 expr2 ...)
     (call-with-values (lambda () expr1 expr2 ...) right))
    ((_ (((id id* ...) either-expr) . claws) . body)
     (either-bind (%guard-value either? either-expr)
                  (lambda (id id* ...)
                    (either-let*-values claws . body))))
    ((_ ((ids either-expr) . claws) . body)
     (either-bind (%guard-value either? either-expr)
                  (lambda ids
                    (either-let*-values claws . body))))
    ((_ ((either-expr) . claws) . body)
     (let ((either either-expr))
       (cond ((right? either) (either-let*-values claws . body))
             ((left? either) either)
             (else (error "ill-typed value" either? either)))))
    ((_ (id . claws) . body)
     (cond ((right? id) (either-let*-values claws . body))
           ((left? id) id)
           (else (error "ill-typed value" either? either))))
    ((_ . _)
     (syntax-error "ill-formed either-let*-values form"))))

(define-syntax either-guard
  (syntax-rules ()
    ((_ pred-expr expr1 expr2 ...)
     (exception->either pred-expr (lambda () expr1 expr2 ...)))))

;;;; Trivalent logic

;;; In the following procedures, (just #f) is considered to be false.
;;; All other Just values are taken to be true.

(define (just->boolean maybe::java.util.Optional) ::boolean
  (not (equal? (maybe:orElse #f) '(#f))))

(define (tri-not maybe::java.util.Optional)
  (maybe-bind maybe (lambda (x) (just (not x)))))

;; Returns #t if all arguments are true or all false.  If any argument
;; is Nothing or if any two arguments have different (tri-)truth values,
;; #f is returned.
(define (tri=? maybe::java.util.Optional . ms)
  (let ((make-pred
         (lambda (b)
           (lambda (m::java.util.Optional)
             (and (just? m) (eqv? (just->boolean m) b))))))
    (if (nothing? maybe)
        (just #f)
        (let ((tri-same? (make-pred (just->boolean maybe))))
          (if (every tri-same? ms) (just #t) (just #f))))))

;; Returns #t if all arguments are true.  If any argument is false or
;; Nothing, return the first such object.
(define (tri-and . maybes)
  (or (find (lambda (m::java.util.Optional)
              (or (nothing? m) (not (just->boolean m))))
            maybes)
      (just #t)))

;; Returns #f if all arguments are false.  If any argument is true or
;; Nothing, return the first such object.
(define (tri-or . maybes)
  (or (find (lambda (m::java.util.Optional)
              (or (nothing? m) (just->boolean m)))
            maybes)
      (just #f)))

;; If all arguments are Nothing, then return Nothing.  Otherwise,
;; return the first Just value.
(define (tri-merge . maybes)
  (or (find just? maybes) (java.util.Optional:empty)))
