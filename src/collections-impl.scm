;;; scheme interface to java collection classes


(define-alias coll java.util.Collection)
(define-alias clist java.util.List)
(define-alias cqueue java.util.Queue)
(define-alias cdeque java.util.Deque)
(define-alias cpque java.util.PriorityQueue)
(define-alias cset java.util.Set)
(define-alias csset java.util.SortedSet)
(define-alias cnset java.util.NavigableSet)
(define-alias cmap java.util.Map)
(define-alias csmap java.util.SortedMap)
(define-alias cnmap java.util.NavigableMap)
(define-alias it java.util.Iterator)
(define-alias lit java.util.ListIterator)
(define-alias stream java.util.stream.Stream)

;; Predicates
(define (collection? obj) (coll? obj))
(define (coll-list? obj) (clist? obj))
(define (coll-queue? obj) (cqueue? obj))
(define (coll-deque? obj) (cdeque? obj))
(define (coll-priority-queue? obj) (cpque? obj))
(define (coll-set? obj) (cset? obj))
(define (coll-sorted-set? obj) (csset? obj))
(define (coll-navigable-set? obj) (cnset? obj))
(define (coll-map? obj) (cmap? obj))
(define (coll-sorted-map? obj) (csmap? obj))
(define (coll-navigable-map? obj) (cnmap? obj))
(define (java-null? obj) (eq? obj #!null))
(define (coll-stream? obj) (java.util.stream.Stream? obj))

;; Iterator functions
(define (iterator? obj) (it? obj))
(define (iter-has-next? i::it) (i:hasNext))
(define (iter-next! i::it) (i:next))
(define (iter-remove! i::it) (i:remove))

;; List ierator functions
(define (list-iterator? obj) (lit? obj))
(define (listiter-has-previous? i::lit) (i:hasPrevious))
(define (listiter-previous! i::lit) (i:previous))
(define (listiter-next-index i::lit) (i:nextIndex))
(define (listiter-previous-index i::lit) (i:previousIndex))
(define (listiter-set! i::lit v) (i:set v))

(define (coll->generator c::coll)
  (let ((iter (c:iterator)))
    (lambda ()
      (if (iter-has-next? iter)
          (iter-next! iter)
          (eof-object)))))

(define-class myComparator (java.util.Comparator) interface: #f
  (cmp)
  ((*init* c) (set! cmp c))
  ((*init*) (set! cmp (make-default-comparator)))
  ((compare a b) :: int (comparator-if<=> cmp a b -1 0 1)))

;; General collections functions

(define (coll-for-each proc c::coll)
  (c:forEach (lambda (a) (proc a))))

(define (coll-copy obj)
  (cond
   ((java.util.LinkedList? obj)
    ((as java.util.LinkedList obj):clone))
   ((java.util.ArrayList? obj)
    ((as java.util.ArrayList obj):clone))
   ((java.util.ArrayDeque? obj)
    ((as java.util.ArrayDeque obj):clone))
   ((java.util.TreeSet? obj)
    ((as java.util.TreeSet obj):clone))
   ((java.util.concurrent.ConcurrentSkipListSet? obj)
    ((as java.util.concurrent.ConcurrentSkipListSet obj):clone))
   ((java.util.HashSet? obj)
    ((as java.util.HashSet obj):clone))
   ((java.util.TreeMap? obj)
    ((as java.util.TreeMap obj):clone))
   ((java.util.concurrent.ConcurrentSkipListMap? obj)
    ((as java.util.concurrent.ConcurrentSkipListMap obj):clone))
   ((java.util.HashMap? obj)
    ((as java.util.HashMap obj):clone))
   (else
    (error "un-cloneable collection type"))))

(define (coll-add! c::coll e) (c:add e))
(define (coll-add-all! c::coll c2::coll) (c:addAll c2))
(define (coll-clear! c::coll) (c:clear))
(define (coll-contains? c::coll e) (c:contains e))
(define (coll-contains-all? c::coll c2::coll) (c:containsAll c2))
(define (coll-equals? c::coll c2::coll) (c:equals c2))
(define (coll-empty? c::coll) (c:isEmpty))
(define (coll-iterator c::coll) :: it (c:iterator))
(define (coll-remove! c::coll e) (c:remove e))
(define (coll-remove-all! c::coll e::coll) (c:removeAll e))
(define (coll-retain-all! c::coll e::coll) (c:retainAll e))
(define (coll-size c::coll) ::int (c:size))
(define (coll-count c::coll obj) ::int (java.util.Collections:frequency c obj))

(define (coll->vector c::coll)
  (let ((v (make-vector (coll-size c)))
        (it (coll-iterator c)))
    (do ((i 0 (+ i 1)))
        ((not (iter-has-next? it)) v)
      (vector-set! v i (iter-next! it)))))

(define (coll->list c::coll)
  (let ((l (list-queue)))
    (coll-for-each (lambda (elem) (list-queue-add-back! l elem)) c)
    (list-queue-list l)))

(define (fix-string val)
  (if (or (java.lang.String? val) (gnu.lists.FString? val))
      (gnu.lists.IString val)
      val))

;; General set functions

;; Sorted set functions
(define (coll-set-first c::csset) (c:first))
(define (coll-set-last c::csset) (c:last))
(define (coll-sset-comparator c::csset) (c:comparator))
(define (coll-set-head-set c::csset e) :: csset (c:headSet (fix-string e)))
(define (coll-set-sub-set c::csset a b) :: csset (c:subSet (fix-string a) (fix-string b)))
(define (coll-set-tail-set c::csset e) :: csset (c:tailSet (fix-string e)))

;; Navigable set functions
(define (coll-set-ceiling c::cnset e) (c:ceiling (fix-string e)))
(define (coll-set-floor c::cnset e) (c:floor (fix-string e)))
(define (coll-set-higher c::cnset e) (c:higher (fix-string e)))
(define (coll-set-lower c::cnset e) (c:lower (fix-string e)))
(define (coll-set-head-seti c::cnset e i::boolean) :: cnset (c:headSet (fix-string e) i))
(define (coll-set-sub-seti c::cnset a i1::boolean b i2::boolean) :: cnset
  (c:subSet (fix-string a) i1 (fix-string b) i2))
(define (coll-set-tail-seti c::cnset e i::boolean) :: cnset (c:tailSet e i))
(define (coll-set-descending-iterator c::cnset) :: it (c:descendingIterator))
(define (coll-set-descending-set c::cnset) :: cnset (c:descendingSet))
(define (coll-set-poll-first! c::cnset) (c:pollFirst))
(define (coll-set-poll-last! c::cnset) (c:pollLast))


;; List functions
(define (coll-list-add-at! c::clist i val) (c:add i val))
(define (coll-list-ref c::clist i::int) (c:get i))
(define (coll-list-index c::clist val) (c:indexOf val))
(define (coll-list-index-right c::clist val) (c:lastIndexOf val))
(define (coll-list-remove-at! c::clist i::int) (c:remove i))
(define (coll-list-set! c::clist i::int val) (c:set i val))
(define (coll-list-iterator c::clist #!optional where) :: lit
  (cond
   ((integer? where)
    (c:listIterator (as int where)))
   (where
    (error "coll-list-iterator: invalid argument" where))
   (else
    (c:listIterator))))

;; general algorithms
(define (coll-disjoint? a::coll b::coll) ::boolean (java.util.Collections:disjoint a b))

(define (coll-max c::coll #!optional (compare #f))
  (if (comparator? compare)
      (java.util.Collections:max c (myComparator compare))
      (java.util.Collections:max c)))
(define (coll-min c::coll #!optional (compare #f))
  (if (comparator? compare)
      (java.util.Collections:min c (myComparator compare))
      (java.util.Collections:min c)))

(define (coll-array->stream arr::java.lang.Object[]) ::stream
  (java.util.Arrays:stream arr))
   #|
   (lambda (arr::byte[]) ::stream
           (java.util.Arrays:stream arr))
   (lambda (arr::short[]) ::stream
           (java.util.Arrays:stream arr))
   (lambda (arr::int[]) ::stream
           (java.util.Arrays:stream arr))
   (lambda (arr::long[]) ::stream
           (java.util.Arrays:stream arr))
   (lambda (arr::float[]) ::stream
              (java.util.Arrays:stream arr))
   (lambda (arr::double[]) ::stream
           (java.util.Arrays:stream arr))
   |#

(define (coll-stream->list s::stream) ::clist
  (s:collect (java.util.stream.Collectors:toList)))

(define (coll->stream c::coll) ::stream (c:stream))

(define coll-filter->list
  (make-procedure
   (lambda (pred? c::coll) ::clist
           (let* ((s (c:stream))
                  (f (s:filter (lambda (a) (pred? a)))))
             (coll-stream->list f)))
   (lambda (pred? s::stream) ::clist
           (let ((f (s:filter (lambda (a) (pred? a)))))
             (coll-stream->list f)))))

(define coll-map->list
  (make-procedure
   (lambda (fun c::coll) ::clist
           (let* ((s (c:stream))
                  (f (s:map (lambda (a) (fun a)))))
             (coll-stream->list f)))
   (lambda (fun s::stream) ::clist
           (let ((f (s:map (lambda (a) (fun a)))))
             (coll-stream->list f)))))

(define (stream->coll s::stream out::coll)
  (s:collect (java.util.stream.Collectors:toCollection (lambda () (out)))))

(define (stream->set s::stream c::coll #!key (type #f) (compare #f))
  (cond
   ((eq? type 'hash)
    (s:collect (java.util.stream.Collectors:toSet)))
   ((eq? type 'linkedhash)
    (stream->coll s (coll-linkedhash-set)))
   ((eq? type 'tree)
    (stream->coll s (coll-tree-set compare: compare)))
   ((eq? type 'skiplist)
    (stream->coll s (coll-skiplist-set compare: compare)))
   ((java.util.LinkedHashSet? c)
    (stream->coll s (coll-linkedhash-set)))
   ((java.util.HashSet? c)
    (stream->coll s (coll-hash-set)))
   ((java.util.concurrent.ConcurrentSkipListSet? c)
    (stream->coll s (coll-skiplist-set compare: (if compare
                                                    compare
                                                    ((as java.util.SortedSet c):comparator)))))
   ((java.util.TreeSet? c)
    (stream->coll s (coll-tree-set compare: (if compare
                                                compare
                                                ((as java.util.SortedSet c):comparator)))))
   (else
    (s:collect (java.util.stream.Collectors:toSet)))))

(define coll-filter->set
  (make-procedure
   (lambda (pred? c::coll #!key (type #f) (compare #f)) ::cset
           (let* ((s (c:stream))
                  (f (s:filter (lambda (a) ::boolean (pred? a)))))
             (stream->set f c type: type compare: compare)))
   (lambda (pred? s::stream #!key (type #f) (compare #f)) ::cset
           (let ((f (s:filter (lambda (a) ::boolean (pred? a)))))
             (stream->set f #f type: type compare: compare)))))

(define coll-map->set
  (make-procedure
   (lambda (fun c::coll #!key (type #f) (compare #f)) ::cset
           (let* ((s (c:stream))
                  (f (s:map (lambda (a) (fun a)))))
             (stream->set f c type: type compare: compare)))
   (lambda (fun s::stream #!key (type #f) (compare #f)) ::cset
           (let ((f (s:map (lambda (a) (fun a)))))
             (stream->set f #f type: type compare: compare)))))

(define (coll-fold f ident c::coll)
  (coll-stream-fold f ident (coll->stream c)))

(define (coll-any pred? c::coll) ::boolean
  (coll-stream-any pred? (coll->stream c)))

(define (coll-every pred? c::coll) ::boolean
  (coll-stream-every pred? (coll->stream c)))

;; stream algorithms

(define (coll-stream-filter pred? s::stream) ::stream
  (s:filter (lambda (a) ::boolean (pred? a))))

(define (coll-stream-for-each f s::stream)
  (s:forEach (lambda (a) (f a))))

(define (coll-stream-drop n::long s::stream) ::stream
  (s:skip n))

(define (coll-stream-any pred? s::stream) ::boolean
  (s:anyMatch (lambda (a) ::boolean (pred? a))))

(define (coll-stream-every pred? s::stream) ::boolean
  (s:allMatch (lambda (a) ::boolean (pred? a))))

(define (coll-stream-fold f ident s::stream)
  (s:reduce ident (lambda (a b) (f a b))))

;; list algorithms
(define (coll-list-copy! dest::clist src::clist) (java.util.Collections:copy dest src))
(define (coll-list-fill! c::clist obj) (java.util.Collections:fill c obj))
(define (coll-list-replace-all! c::clist old new)
  (java.util.Collections:replaceAll c old new))
(define (coll-list-map! proc c::clist)
  (c:replaceAll (lambda (a) (proc a))))
(define (coll-list-reverse! c::clist) (java.util.Collections:reverse c))
(define (coll-list-rotate! c::clist dist::int) (java.util.Collections:rotate c dist))
(define (coll-list-swap! c::clist i::int j::int)
  (java.util.Collections:swap c i j))
(define (coll-list-sort! c::clist #!optional (compare #f))
  (if (comparator? compare)
      (java.util.Collections:sort c (myComparator compare))
      (java.util.Collections:sort c)))
(define (coll-list-binary-search c::clist obj #!optional (compare #f))
  (if (comparator? compare)
      (java.util.Collections:binarySearch c obj (myComparator compare))
      (java.util.Collections:binarySearch c obj)))
         
(define (coll-queue-element c::cqueue) (c:element))
(define (coll-queue-offer! c::cqueue e) (c:offer e))
(define (coll-queue-peek c::cqueue) (c:peek))
(define (coll-queue-poll! c::cqueue) (c:poll))
(define (coll-queue-remove! c::cqueue) (c:remove))

(define (coll-deque-add-first! c::cdeque e) (c:addFirst e))
(define (coll-deque-add-last! c::cdeque e) (c:addLast e))
(define (coll-deque-first c::cdeque) (c:getFirst))
(define (coll-deque-last c::cdeque) (c:getLast))
(define (coll-deque-remove-first! c::cdeque) (c:removeFirst))
(define (coll-deque-remove-last! c::cdeque) (c:removeLast))
(define (coll-deque-offer-first! c::cdeque e) (c:offerFirst e))
(define (coll-deque-offer-last! c::cdeque e) (c:offerLast e))
(define (coll-deque-peek-first c::cdeque) (c:peekFirst))
(define (coll-deque-peek-last c::cdeque) (c:peekLast))
(define (coll-deque-poll-first! c::cdeque) (c:pollFirst))
(define (coll-deque-poll-last! c::cdeque) (c:pollLast))
(define (coll-deque-remove-first-occurrence! c::cdeque e) (c:removeFirstOccurrence e))
(define (coll-deque-remove-last-occurrence! c::cdeque e) (c:removeLastOccurrence e))
(define (coll-deque->stack c::cdeque) :: cqueue
  (java.util.Collections:asLifoQueue c))

(define (coll-map-clear! c::cmap) (c:clear))
(define (coll-map-compute! c::cmap key proc)
  (c:compute key (lambda (a b) (proc a b))))
(define (coll-map-compute-absent! c::cmap key proc)
  (c:computeIfAbsent (fix-string key) (lambda (a) (proc a))))
(define (coll-map-compute-present! c::cmap key proc)
  (c:computeIfPresent (fix-string key) (lambda (a b) (proc a b))))
(define (coll-map-contains? c::cmap key) (c:containsKey (fix-string key)))
(define (coll-map-for-each proc c::cmap)
  (c:forEach (lambda (a b) (proc a b))))
(define (coll-map-ref c::cmap key) (c:get (fix-string key)))
(define (coll-map-empty? c::cmap) (c:isEmpty))
(define (coll-map-keys c::cmap) (c:keySet))
(define (coll-map-merge! c::cmap key val proc)
  (c:merge (fix-string key) val (lambda (a b) (proc a b))))
(define (coll-map-put! c::cmap key val) (c:put (fix-string key) val))
(define (coll-map-put-absent! c::cmap key val)
  (c:putIfAbsent (fix-string key) val))
(define (coll-map-put-all! to::cmap from::cmap) (to:putAll from))
(define (coll-map-remove! c::cmap key) (c:remove (fix-string key)))
(define (coll-map-replace! c::cmap key newval) (c:replace (fix-string key) newval))
(define (coll-map-map! proc c::cmap)
  (c:replaceAll (lambda (a b) (proc a b))))
(define (coll-map-size c::cmap) ::int (c:size))
(define (coll-map-values c::cmap) (c:values))

(define (coll-map-entries c::cmap) ::cset (c:entrySet))

(define-alias emap java.util.Map$Entry)

(define (coll-map-entry-key e::emap) (e:getKey))
(define (coll-map-entry-value e::emap) (e:getValue))

(define (coll-map->alist c::cmap)
  (let ((l (list-queue)))
    (coll-map-for-each (lambda (k v) (list-queue-add-back! l (cons k v))) c)
    (list-queue-list l)))
(define (coll-map-keys->list c::cmap)
  (coll->list (coll-map-keys c)))
(define (coll-map-values->list c::cmap)
  (coll->list (coll-map-values c)))

;; Sorted maps
(define (coll-map-first-key c::csmap) (c:firstKey))
(define (coll-map-last-key c::csmap) (c:lastKey))
(define (coll-map-head-map c::csmap key) (c:headMap (fix-string key)))
(define (coll-map-tail-map c::csmap key) (c:tailMap (fix-string key)))
(define (coll-map-sub-map c::csmap start end) (c:subMap start end))

;; Navigable maps
(define (coll-map-ceiling c::cnmap key) (c:ceilingKey (fix-string key)))
(define (coll-map-floor c::cnmap key) (c:floorKey (fix-string key)))
(define (coll-map-higher c::cnmap key) (c:higherKey (fix-string key)))
(define (coll-map-lower c::cnmap key) (c:lowerKey (fix-string key)))
(define (coll-map-descending-keys c::cnmap) (c:descendingKeySet))
(define (coll-map-descending-map c::cnmap) (c:descendingMap))
(define (coll-map-head-mapi c::cnmap key inclusive::boolean)
  (c:headMap (fix-string key) inclusive))
(define (coll-map-tail-mapi c::cnmap key inclusive::boolean)
  (c:tailMap (fix-string key) inclusive))
(define (coll-map-sub-mapi c::cnmap start starti::boolean end endi::boolean)
  (c:subMap start starti end endi))

(define-class hashbox (java.lang.Comparable) interface: #f
  (comp)
  (val)
  ((*init* c v) (set! comp c) (set! val v))
  ((equals b) ::boolean (=? comp val b))
  ((hashCode) ::int (comparator-hash comp val))
  ((compareTo b) ::int (comparator-if<=> comp val b -1 0 1)))

(define (make-hash-boxer cmp)
  (lambda (val) (hashbox cmp val)))

(define (hash-unbox hb::hashbox)
  (hb:val))

;;; Constructors

(define (coll-list->coll! c::coll lst)
  (cond
   ((collection? lst)
    (coll-add-all! c lst)
    c)
   ((list? lst)
    (do ((lst lst (cdr lst)))
        ((null? lst) c)
      (coll-add! c (car lst))))
   (else
    (error "Invalid argument to coll-list->coll"))))

(define (coll-list->deque! c::cdeque lst)
  (cond
   ((collection? lst)
    (coll-add-all! c lst)
    c)
   ((list? lst)
    (do ((lst lst (cdr lst)))
        ((null? lst) c)
      (coll-deque-add-last! c (car lst))))
   (else
    (error "Invalid argument to coll-list->deque!"))))

(define (coll-make-linked-list lst)
  (cond
   ((collection? lst)
    (java.util.LinkedList (as coll lst)))
   ((list? lst)
    (coll-list->deque! (java.util.LinkedList) lst))
   (else
    (error "invalid argument to coll-make-linked-list" lst))))
(define (coll-linked-list . vals)
  (coll-make-linked-list vals))

(define (coll-make-array-list lst) ::clist
  (cond
   ((collection? lst)
    (java.util.ArrayList (as coll lst)))
   ((list? lst)
    (coll-list->coll! (java.util.ArrayList) lst))
   (else
    (error "invalid argument to coll-make-array-list" lst))))
(define (coll-array-list . vals) ::clist
  (coll-make-array-list vals))

(define (coll-make-array-deque lst)::cdeque
  (cond
   ((collection? lst)
    (java.util.ArrayDeque (as coll lst)))
   ((list? lst)
    (coll-list->deque! (java.util.ArrayDeque) lst))
   (else
    (error "invalid argument to coll-make-array-deque" lst))))    
(define (coll-array-deque . vals) ::cdeque
  (coll-make-array-deque vals))

(define (coll-make-priority-queue lst #!key (compare #f))::cqueue
  (cond
   ((and (not compare) (list? lst))
    (coll-list->coll! (java.util.PriorityQueue) lst))
   ((and (not compare) (collection? lst))
    (java.util.PriorityQueue (as coll lst)))
   ((and (comparator? compare) (or (list? lst) (collection? lst)))
    (coll-list->coll! (java.util.PriorityQueue (myComparator compare)) lst))
   (else
    (error "Invalid argument to make-priority-queue"))))  
(define (coll-priority-queue #!key (compare #f) #!rest vals) ::cqueue
  (coll-make-priority-queue vals compare: compare))

(define (coll-make-tree-set lst #!key (compare #f)) ::csset
  (cond
   ((and (not compare) (list? lst))
    (coll-list->coll! (java.util.TreeSet) lst))
   ((and (not compare) (coll-sorted-set? lst))
    (java.util.TreeSet (as csset lst)))
   ((and (not compare) (collection? lst))
    (java.util.TreeSet (as coll lst)))
   ((and (comparator? compare) (or (list? lst) (collection? lst)))
    (coll-list->coll! (java.util.TreeSet (myComparator compare)) lst))
   (else
    (error "Invalid argument to coll-make-tree-set"))))
(define (coll-tree-set #!key (compare #f) #!rest vals) ::csset
  (coll-make-tree-set vals compare: compare))

(define (coll-make-skiplist-set lst #!key (compare #f)) ::csset
  (cond
   ((and (not compare) (list? lst))
    (coll-list->coll! (java.util.concurrent.ConcurrentSkipListSet) lst))
   ((and (not compare) (coll-sorted-set? lst))
    (java.util.concurrent.ConcurrentSkipListSet (as csset lst)))
   ((and (not compare) (collection? lst))
    (java.util.concurrent.ConcurrentSkipListSet (as coll lst)))
   ((and (comparator? compare) (or (list? lst) (collection? lst)))
    (coll-list->coll!
     (java.util.concurrent.ConcurrentSkipListSet (myComparator compare)) lst))
   (else
    (error "Invalid argument to coll-make-tree-set"))))
(define (coll-skiplist-set #!key (compare #f) #!rest vals) ::csset
  (coll-make-skiplist-set vals compare: compare))

(define (coll-make-hash-set lst #!key (capacity 16) (load 0.75)) ::cset
  (if (collection? lst)
      (java.util.HashSet (as coll lst))
      (coll-list->coll! (java.util.HashSet capacity load) lst)))
(define (coll-hash-set #!key (capacity 16) (load 0.75) #!rest vals) ::cset
  (coll-make-hash-set vals capacity: capacity load: load)) 

(define (coll-make-linkedhash-set lst #!key (capacity 16) (load 0.75)) ::cset
  (if (collection? lst)
      (java.util.LinkedHashSet (as coll lst))
      (coll-list->coll! (java.util.LinkedHashSet capacity load) lst)))
(define (coll-linkedhash-set #!key (capacity 16) (load 0.75) #!rest vals) ::cset
  (coll-make-linkedhash-set vals capacity: capacity load: load))

(define (coll-immut-list n::int obj) ::clist
  (java.util.Collections:nCopies n obj))

(define (coll-alist->map! c::cmap alist)
  (do ((alist alist (cdr alist)))
      ((null? alist) c)
    (coll-map-put! c (caar alist) (cdar alist))))

(define (coll-make-tree-map lst #!key (compare #f)) ::csmap
  (cond
   ((and (not compare) (list? lst))
    (coll-alist->map! (java.util.TreeMap) lst))
   ((and (not compare) (coll-sorted-map? lst))
    (java.util.TreeMap (as csmap lst)))
   ((and (not compare) (coll-map? lst))
    (java.util.TreeMap (as cmap lst)))
   ((and (comparator? compare) (list? lst) (coll-sorted-map? lst))
    (coll-alist->map! (java.util.TreeMap (myComparator compare)) lst))
   (else
    (error "Invalid argument to coll-make-tree-set"))))
(define (coll-tree-map #!key (compare #f) #!rest vals) ::csmap
  (coll-make-tree-map vals compare: compare))

(define (coll-make-hash-map vals) ::cmap
  (cond
   ((coll-map? vals)
    (java.util.HashMap (as cmap vals)))
   ((list? vals)
    (coll-alist->map! (java.util.HashMap (as int (min 16 (length vals)))) vals))
   (else
    (error "Invalid arguments to coll-make-hash-map"))))
(define (coll-hash-map . vals) ::cmap
  (let ((len (length vals)))
    (cond
     ((= len 0)
      (java.util.HashMap))
     ((and (= len 1) (integer? (car vals)))
      (java.util.HashMap (as int (car vals))))
     ((and (= len 2)
           (integer (car vals))
           (real? (cadr vals)))
      (java.util.HashMap (as int (car vals)) (as float (cadr vals))))
     (else
      (coll-make-hash-map vals)))))

(define (coll-make-linkedhash-map vals) ::cmap
  (cond
   ((coll-map? vals)
    (java.util.LinkedHashMap (as cmap vals)))
   ((list? vals)
    (coll-alist->map! (java.util.LinkedHashMap (as int (min 16 (length vals)))) vals))
   (else
    (error "Invalid arguments to coll-make-linkedhash-map"))))
(define (coll-linkedhash-map . vals) ::cmap
  (let ((len (length vals)))
    (cond
     ((= len 0)
      (java.util.LinkedHashMap))
     ((and (= len 1) (integer? (car vals)))
      (java.util.LinkedHashMap (as int (car vals))))
     ((and (= len 2)
           (integer (car vals))
           (real? (cadr vals)))
      (java.util.LinkedHashMap (as int (car vals)) (as float (cadr vals))))
     (else
      (coll-make-linkedhash-map vals)))))

(define (coll-make-skiplist-map vals #!key (compare #f)) ::csmap
  (cond
   ((coll-sorted-map? vals)
    (java.util.concurrent.ConcurrentSkipListMap (as csmap vals)))
   ((coll-map? vals)
    (java.util.concurrent.ConcurrentSkipListMap (as cmap vals)))
   ((and (comparator? compare) (list? vals))
    (coll-alist->map!
     (java.util.concurrent.ConcurrentSkipListMap (myComparator compare)) vals))
   ((list? vals)
    (coll-alist->map! (java.util.concurrent.ConcurrentSkipListMap) vals))
   (else
    (error "Invalid arguments to coll-make-skiplist-map"))))
(define (coll-skiplist-map #!key (compare #f) #!rest vals) ::csmap
  (coll-make-skiplist-map vals compare: compare))
  
(define coll->immutable
  (make-procedure
   (lambda (lst::clist) (java.util.Collections:unmodifiableList lst))
   (lambda (set::csset) (java.util.Collections:unmodifiableSortedSet set))
   (lambda (set::cset) (java.util.Collections:unmodifiableSet set))
   (lambda (map::csmap) (java.util.Collections:unmodifiableSortedMap map))
   (lambda (map::cmap) (java.util.Collections:unmodifiableMap map))
   (lambda (c::coll) (java.util.Collections:unmodifiableCollection c))))

(define (enumeration->list e::java.util.Enumeration) ::clist 
  (java.util.Collections:list e))

(define (coll-array->list arr) ::clist
  (coll-stream->list (coll-array->stream arr)))

(define (coll-empty-list) ::clist (java.util.Collections:EMPTY_LIST))
(define (coll-empty-set) ::cset (java.util.Collections:EMPTY_SET))
(define (coll-empty-map) ::cmap (java.util.Collections:EMPTY_MAP))
 
(define (coll-single-list obj) ::clist (java.util.Collections:singletonList obj))
(define (coll-single-set obj) ::cset (java.util.Collections:singleton obj))
(define (coll-single-map key val) ::cmap (java.util.Collections:singletonMap key val))
