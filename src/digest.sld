;;; Message digetss

(define-library (digest)
  (import (kawa base))
  (export
   md2 md5 sha-1 sha-224 sha-256 sha-384 sha-512
   make-digest digest-update! digest->string! digest->bytevector!
   digest-reset! digest-name digest-length)
  (include "digest-impl.scm"))


   
   
