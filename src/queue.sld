;;; Simple deques, API based on slib. -*- scheme -*-
;;; https://www.cs.indiana.edu/scheme-repository/SCM/slib_2.html#SEC24

(define-library (queue)
  (export
   make-queue queue? queue-empty?
   queue-push! enqueue!
   queue-front queue-rear
   queue-pop! dequeue!)
  (import (kawa base)
          (rename (collections)
                  (coll-array-deque make-queue)
                  (coll-deque? queue?)
                  (coll-empty? queue-empty?)
                  (coll-deque-add-first! queue-push!)
                  (coll-deque-add-last! enqueue!)
                  (coll-deque-first queue-front)
                  (coll-deque-last queue-rear)
                  (coll-deque-remove-first! queue-pop!)))
  (begin
    (define (dequeue! q) (queue-pop! q))))

  
