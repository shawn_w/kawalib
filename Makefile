# Makefile for kawalib.
# Assumes that java and kawa are in your path.

# Try to use fastjar if availabe
FASTJAR=$(shell which fastjar)
JAR?=$(if ${FASTJAR}, ${FASTJAR}, jar)

kawalib.jar: src/classfiles
	cd src && ${JAR} cf ../kawalib.jar @classfiles

src/classfiles: rebuild
	@(cd src && find . -name "*.class" > classfiles.new)
	@(cmp -s src/classfiles src/classfiles.new; \
	if test $$? -ne 0; then \
		mv src/classfiles.new src/classfiles; \
	else \
		rm src/classfiles.new; \
	fi)

.PHONY: force
force:
	rm -f src/classfiles
	$(MAKE) kawalib.jar

.PHONY: rebuild
rebuild:
	cd src && $(MAKE)

.PHONY: clean
clean:
	rm -f kawalib.jar src/*.stamp src/classfiles
	find src/ -name "*.class" -delete
