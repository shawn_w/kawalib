# kawalib

A library of useful scheme routines for use with Kawa scheme. Some of
these might end up being submitted into the main kawa source tree at
some point.

Everything is tested with and intended to work with Kawa 3.0, and
requires Java 8 or better.

## To use:

Build kawalib.jar with `make`. Stick kawalib.jar in your **CLASSPATH**, launch
kawa, and import what you want.

## Included:

### R6RS libraries

* [R6RS Enums](http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-15.html#node_chap_14) - `(import (rnrs enums))`
* [R6RS Files](http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-10.html#node_chap_9) - `(import (rnrs files))`
* [R6RS Fixnums](http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-12.html#node_sec_11.2) - `(import (rnrs arithmetic fixnums))`.
* [R6RS Flonums](http://www.r6rs.org/final/html/r6rs-lib/r6rs-lib-Z-H-12.html#node_sec_11.3) - `(import (rnrs arithmetic flonums))`.

### SRFIs

* [SRFI-27: Source Of Random Bits](https://srfi.schemers.org/srfi-27/) - `(import (srfi 27))`.
* [SRFI-66: Octet Vectors](https://srfi.schemers.org/srfi-66/srfi-66.html) - `(import (srfi 66))`.
* [SRFI-74: Octet-Addressed Binary Blocks](https://srfi.schemers.org/srfi-74/srfi-74.html) - `(import (srfi 74))`.
* [SRFI-106: Sockets](https://srfi.schemers.org/srfi-106/srfi-106.html#address-family) - `(import (srfi 106))`
* [SRFI-111: Boxes](https://srfi.schemers.org/srfi-111/srfi-111.html) - `(import (srfi 111))`
* [SRFI-113: Bags and Sets](https://srfi.schemers.org/srfi-113/srfi-113.html) -
  `(import sets)` or `(import (srfi 113))`. Note: Everywhere the docs
  mention SRFI-114, mentally replace it with SRFI-128.
* [SRFI-116: Immutable Lists](https://srfi.schemers.org/srfi-116/srfi-116.html) - `(import (srfi 116))`.
* [SRFI-117: Queues](https://srfi.schemers.org/srfi-117/srfi-117.html) - `(import (srfi 117))` or `(import list-queues)`.
* [SRFI-124: Ephemerons](https://srfi.schemers.org/srfi-124/srfi-124.html)
  aka weak references. `(import (srfi 124))`.
* [SRFI-126: R6RS Based Hashtables](https://srfi.schemers.org/srfi-126/srfi-126.html) - `(import (srfi 126))`
* [SRFI-127: Lazy Sequences](https://srfi.schemers.org/srfi-127/srfi-127.html) - `(import (srfi 127))` or `(import lseqs)`
* [SRFI-128: Comparators](https://srfi.schemers.org/srfi-128/srfi-128.html) - `(import (srfi 128))`
* [SRFI-129: Titlecase procedures](https://srfi.schemers.org/srfi-129/srfi-129.html) - `(import (srfi 129))`
* [SRFI-132: Sorting Library](https://srfi.schemers.org/srfi-132/srfi-132.html) - `(import (srfi 132))`.
* [SRFI-133: Vector Library](https://srfi.schemers.org/srfi-133/srfi-133.html) - `(import vectors)` or `(import (srfi 133))`.
* [SRFI-134: Immutable Deques](https://srfi.schemers.org/srfi-134/srfi-134.html) - `(import ideque)` or `(import (srfi 134))`.
* [SRFI-141: Integer Divison](https://srfi.schemers.org/srfi-141/srfi-141.html) - `(import (srfi 141))`.
* [SRFI-143: Expanded Fixnums](https://srfi.schemers.org/srfi-143/srfi-144.html) - `(import (srfi 143))`.
* [SRFI-144: Expanded Flonums](https://srfi.schemers.org/srfi-144/srfi-144.html) - `(import (srfi 144))`.
* [SRFI-145: Assumptions](https://srfi.schemers.org/srfi-145/srfi-145.html) - `(import (srfi 145))`.
* [SRFI-151: Bitwise operators](https://srfi.schemers.org/srfi-151/srfi-151.html) - `(import (srfi 151))`.
* [SRFI-158: Accumulators and Generators](https://srfi.schemers.org/srfi-158/srfi-158.html) - `(import (srfi 158))`.
* [SRFI-189: Maybe and Either](https://srfi.schemers.org/srfi-189/srfi-189.html) - `(import (srfi 189))`.
* [SRFI-194: Random data generators](https://srfi.schemers.org/srfi-194/srfi-194.html) - `(import (srfi 194))`

### SLIB libraries

Usually original code following the same API.

* [prime numbers](https://people.csail.mit.edu/jaffer/slib/Prime-Numbers.html). `(import factor)`.
* [priority queues](http://people.csail.mit.edu/jaffer/slib/Priority-Queues.html). `(import priority-queue)`.
* [queues](http://people.csail.mit.edu/jaffer/slib/Queues.html). Double
  ended, can be used as a FIFO queue or LIFO stack. `(import queue)`.
* [topological sort](http://people.csail.mit.edu/jaffer/slib/Topological-Sort.html). `(import tsort)`.

### Libraries from other Scheme implementations

#### Chicken

* [data-structures](http://wiki.call-cc.org/man/4/Unit%20data-structures) -
  `(import data-structures)`

### Original libraries

* Bitset: Interface with java.util.BitSet class. `(import bitset)`.
* Collections: Low level interface with Java Collections Framework. `(import
  collections)`.
* Digest: Message Digest hashes (MD5, SHA-1, etc). See
  docs/digest.md. `(import digest)`.
* Endian: Convert between big and little endian fixnums. `(import
  endian)`.
* IO-Utils: Convenience functions for opening text files in various
  character encodings, and other handy I/O routines. See
  docs/io-utils.md. `(import io-utils)`.
* JSON: Basic JSON parser and printer. See docs/json.md for
  details. `(import json)`.
* List Utilities: Useful functions for working on lists. See
  docs/list-utils.md for details. `(import list-utils)`.
* Marshal: Serialized values. `(import marshal)`.
* Optional: Immutable objects that can hold a value or be empty. See
  docs/optionals.md for details. `(import optional)`.
* Permutations: Permutations of vectors. See
  docs/permutations.md. `(import permutations)`.
* Stacks: Simple LIFO stacks. See docs/stack.md `(import stack)`.
* Unicode: Support unicode breaking algorithms and collation. See
  docs/unicode.md for details.`(import unicode)`.
* Uniform Vector Utilities: Useful functions for working with SRFI-4
  uniform vectors. See docs/uvector-utils.md for details. `(import
  uvector-utils)`.
* UUID: Support for universally unique identifiers. See docs/uuid.md
  for details. `(import uuid)`.

### Other random stuff

* let-optional* macro. `(import let-optional)`.

## Useful(?) Notes:

* `factor`: See docs/factor.md for extensions.
* fixnums - 32 bit ints. fx+, fx-, fx* will raise exceptions on overflow.
* `priority-queue`: See docs/priority-queue.md for extensions to the SLIB api.
* `queue`: Uses collections, specifically java.util.ArrayDeque.
* `(rnrs enums)` - Currently written in terms of srfi 113. Consider
  rewriting to use java enums.
* `(srfi 27)` `(make-random-source [SEED])` uses java.util.Random,
  `(make-random-source 'splitmix [SEED])` uses
  java.util.SplittableRandom, `(make-random-source 'crypto)` uses
  java.security.SecureRandom.
* `(srfi 106)` - See docs/srfi-106.md for caveats.
* `(srfi 111)` - See docs/srfi-111.md for extensions made to this
  SRFI. They might end up being moved to a different library.
* `(srfi 113)` - Currently using the reference
  implementation. Consider rewriting to use java collections
* `(srfi 124)` - See wboxes from `(srfi 111)` for single-valued weak references.
* `(srfi 126)` - Consider adding support for weak reference tables.
* `(srfi 158)` - make-coroutine-generator doesn't work due to call/cc
  limitations. Same for functions that depend on it:
  make-for-each-generator and make-unfold-generator. gtake used to but
  has been rewritten in this implementation.
* `(srfi 189)` - the Maybe type is implemented using
  `java.util.Optional`. Syntax forms have issues; I think it's how
  they interact with kawa modules.
