Most of the source in kawalib is taken from SRFI reference
implementations and other sources with permissive licenses allowing
copying and modification. Any changes needed to those files to make
them compatible with kawa or for efficiency's sake are done under the
same terms as the respective library's license. See those source files
for details.


All other code is public domain.
