% JSON parser.

Introduction
============

Implements RFC-8259 JSON, which specifies that JSON used out in the
open needs to be encoded in UTF-8. The functions below read from and
write to ports using Java/Kawa UTF-16 characters. It's the
responsibility of whomever creates those ports to make sure they're
translating to/from UTF-8. See **io-utils** for some relevant useful
functions.

JSON values are represented internally in the following ways:

true, false

: `#t` and `#f`

null

: `#!null`

numbers

: as the appropriate numeric type, either real or integer.

strings

: as scheme strings.

objects

: as a **Map**. Use the `(collections)` library to access these.

arrays

: as a **List**. Use the `(collections)` library to access these.

This parser properly passes all the mandatory tests from
<https://github.com/nst/JSONTestSuite>, and fails many of the
indeterminate tests related to invalid surrogate pairs and floating
point numbers that are out of range. (Large integers work fine). This
seems desirable. Most of the other indeterminate tests that it accepts have
character encoding gotchas that Java's conversion of UTF-8 catches and
deals with in a non-error fashion (Replacing invalid sequences with
question marks, etc.)

Functions
=========

Predicates
----------

### json? obj

True if obj is a json value.

### json-null? j

True if j is null.

### json-bool? j

True if j is a boolean.

### json-string? j

True if j is a string.

### json-number? j

True if j is a number.

### json-array? j

True if j is an array.

### json-object? j

True if j is an object.

Input/Output
------------

### read-json [port]

Read a JSON value from the port (Or *current-input-port* if absent).

### string->json str

Read a JSON value from a string.

### bytevector->json bv

Read a JSON value from a utf-8 encoded bytevector.

### write-json json [port]

Writes the JSON value to the given port (Or
*current-output-port*). Makes a slight effort to pretty-print the
results.

### json->string json

Write a JSON value to a string.

### json->bytevector json

Write a JSON value to a utf-8 encoded bytevector.

### json->native

Return a copy of the JSON value with uses of Java Collections
Framework classes replaced with native scheme types. Objects become
alists, arrays become vectors, and `#!null` becomes `'null`.

### native->json

Turns a scheme value into a JSON one. vectors turn into json arrays,
alists with string keys turn into json objects.

Accessors
---------

### json-ref j

Returns the underlying scheme value of a JSON value.

### json-ref jarray n

Returns the Nth JSON value in the JSON array.

### json-ref jobject key

Returns the JSON value associated with key, or `#f` if not present.


### json-array-length jarray

Returns the number of elements in the JSON array.

Variables
=========

### json-null

The JSON null value.

### json-true

The JSON true value.

### json-false

The JSON false value.

