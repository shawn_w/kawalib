# Prime Numbers

Includes all functions from SLIB [factor](https://people.csail.mit.edu/jaffer/slib/Prime-Numbers.html) module, plus a few more described below.

The factor and jacobi-symbol functions are taken directly from the
SLIB implementation; others written by Shawn building on
java.math.BigInteger prime number related methods. The `prime:prngs`
variable from SLIB does nothing in this implementation.

## Extra functions

### Procedure: next-prime n

Returns the first prime number greater than *n*.

### Variable: primes

An infinite SRFI-41 stream of prime numbers.

### Procedure: primes-before n

Returns a finite stream of all prime numbers less than *n*.

### Procedure: primes-after n

Returns an infinite stream of all prime numbers greater than *n*.

