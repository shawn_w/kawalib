# Stack library

Very simple LIFO stacks.

## Mutable stacks

### stack val ...

Return a new stack with optional initial values.

### make-stack [vals]

Return a new empty stack with optional list of initial values.

### stack? obj

Return `#t` if obj is a stack.

### stack-empty? st

Return `#t` if st is empty.

### stack-size st

Return the number of items in the stack.

### stack-push! st obj

Add a new element to the stack.

### stack-pop! st

Remove the top element from the stack and return it.

### stack-peek st

Return the top element from the stack without popping it.

## Immutable stack

Represented as a list.

### istack val ...

Return a new stack with optional initial values.

### make-istack [vals]

Return a new empty stack with optional list of initial values.

### istack? obj

Return `#t` if obj is a stack.

### istack-empty? ist

Return `#t` if ist is empty.

### istack-size ist

Return the number of items in the stack.

### istack-push! ist obj

Return a new tack with obj added to the top. 

### istack-pop ist

Returns two values: The top of the stack, and a new stack with the top removed.

### istack-peek ist

Return the top element from the stack without popping it.
