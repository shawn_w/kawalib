bitsets
=======

Basically a vector where each bit is individually addressed. Grows as
needed. Based on `java.util.BitSet`.

A set bit is represented by `#t` and a cleared, or zero bit by `#f`.

Functions
---------

### Constructors

#### make-bitset [size]

Returns a new bitset. If a size is given, allocates enough space to
store indexes 0 through `size - 1`.

#### copy-bitset bs
#### copy-bitset bs from to

Returns a new copy of a bitset, or of a range of its indexes.

#### bitset->bytevector bs

Returns a bytevector representing the bitset.

#### bytevector->bitset

And the other way around.

### Predicates

#### bitset? obj

Returns true if obj is a bitset.

#### bitset=? bs1 bs2

Compares two bitsets.

#### bitset-empty? bs

Returns true if there are no set bits in the bitset.

#### bitset-intersects? bs1 bs2

Returns true if both bitsets have a bit set in common.

#### Setting and clearing bits

#### bitset-set! bs i
#### bitset-set! bs i val

Sets the bit at index `i` to `#t` or the given value.

#### bitset-fill! bs from to
#### bitset-fill! bs from to val

Sets every bit in a range to true or to `val`.

#### bitset-clear! bs
#### bitset-clear! bs i
#### bitset-clear! bs from to

Clears all set bits, or a specific one, or a range.

### Queries

#### bitset-ref bs i

Returns the status of the bit at index `i` - either `#t` or `#f`.

#### bitset-count bs

Returns the number of set bits.

#### bitset-length bs

Returns the highest set bit plus one.

#### bitset-hash bs

Returns a hash value for a bitset.

### Logical operations

#### bitset-not! bs
#### bitset-not! bs i
#### bitset-not! bs from to

Flips the state of the given bit or range.

#### bitset-and! bs1 bs2

ANDs bits in bs1 with bs2.

#### bitset-andnot! bs1 bs2

Clears all bits in bs1 that are set in bs2.

#### bitset-ior! bs1 bs2

ORs all bits in bs1 and bs2.

#### bitset-xor! bs1 bs2

XORs all bits in bs1 with bs2.

### Searching for bits

#### bitset-first-set-bit bs [i]
#### bitset-first-clear-bit bs [i]

Returns the index of the first set or clear bit on or after
i. Defaults to 0.

#### bitset-last-set-bit bs [i]
#### bitset-last-clear-bit bs [i]

Returns the index of the first set or clear bit before i. Defaults to
`(bitset-length bs)`.

### Other

#### (variable) bitset-comparator

A SRFI-128 comparator for bitsets. Does **NOT** include an ordering predicate.
