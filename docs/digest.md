# Message Digest Hashing

Interface to `java.security.MessageDigest`.

## High level interface

The following functions either take a `u8vector` or a string (Which is
turned into a `u8vector` with `string->utf8`). They return the base-16
encoded hash of their arguments according to the specified algorithm.

* md2
* md5
* sha-1
* sha-224
* sha-384
* sha-512

## Low level interface

The update and finalization functions can take a `u8vector` or a
string. If given a string, it's turned into a `u8vector` with
`string->utf8`.

## make-digest ALGORITHM

Returns a new digest object using the given algorithm, which can be
one of: md2 md5 sha-1 sha-224 sha-256 sha-384 sha-512.

## digest-update! digest string-or-bytevector

Adds the given bytes to the digest.

## digest->string! digest

Returns the base-16 encoded hash and resets the digest for new data.

## digest->bytevector! digest

Returns the hash as a series of bytes in a `u8vector`, and resets the
digest for new data.

## digest-reset! digest

Discards the current hash and resets the digest for new data.

## digest-name digest

Returns the name of the hash algorithm for this digest.

## digest-length digest

Returns the number of bytes used for the digest's output. The string
length is twice this.

