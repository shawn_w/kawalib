% Unicode utility functions

The optional locale arguments are a symbol representing the locale to
use (Like `'en_US`, or `#f` to use the default locale.

Breaks
======

### break-locales

Returns a `java.util.List` of all locales accepted by the other functions.

### string-char-breaks str [locale]

Return a list of pairs of the indexes of character breaks in the string.

### string->characters str [locale]

Return a list of strings, one extended grapheme cluster of str per element. For example:

    (string->characters "A\u030Ang") => ("A\u030A" "n" "g")

### make-char-break-generator str [locale]

Returns a SRFI-158 generator procedure that returns a new EGC each
time it's invoked.

### string-word-breaks str [locale]

Return a list of pairs of the indexes of character breaks in the string.

### string->words str [locale] [strip-whitespace]

Returns a list of strings, one word according to unicode word breaking
algorithm per element. Whitespace sequences between words are included
too as seperate if the optional `strip-whitespace` argument is false.

### make-word-break-generator str [locale] [strip-whitespace]

Returns a SRFI-158 generator function that returns a word at a time
from str.

### string-sentence-breaks str [locale]

Returns a list of pairs of the indexes of sentence breaks in the string.

### string->sentences str [locale]

Returns a list of strings, one sentence according to the unicode
sentence breaking algorithm per element.

### make-sentence-break-generator str [locale]

Returns a SRFI-158 generator function that returns a sentence at a
time.

### string-line-breaks str [locale]

Returns a list of indexes of possible places to add newlines when word
wrapping.

String comparison
=================

Locale dependant string collation.

### collate-locales

Return a list of locales usable for collation.

Collators
---------

### make-collator [locale]

Returns a collator object.

### collator? obj

Returns true if obj is a collator.

### make-collated-comparator [locale]

Returns a SRFI-128 comparator that does equality and ordering
comparisons using the given locale. No hash function is provided.

### collate=? coll str1 str2

Returns true if the two strings compare equal according to the collator.

### collate-compare coll str1 str2

Returns less than 0, 0, or greater than 0 depending on how the two
strings compare according to the collator.

Collation Keys
--------------

If doing a lot of comparisions on the same group of strings, they can
be pre-processed by turning them into collation keys to provide
speedups. When comparing two collation key objects, they should both
have been created by the same collator.

### collate-key coll str

Returns a new collation key form of str.

### collation-key? obj

Returns true if obj is a collation key.

### key-source-string ck

Returns the string used to create the collation key.

### key-compare ck1 ck2

Compares two collation keys and returns less than 0, 0, or greater
than 0.

### collation-key=? ck1 ck2

Returns true if the two keys are equal.

### collation-key-hash ck

Hashes the key.

### (variable) collation-key-comparator

A SRFI-128 comparator for collation strings.

