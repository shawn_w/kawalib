I/O utilities
=============

Various functions to make reading data from files easier.

All functions that take an optional port argument default to
`(current-input-port)` or `(current-output-port)`.

File encodings
--------------

Java supports opening text files and converting from different
character encodings to Java UTF-16 and back. Kawa supports this with
the `port-char-encoding` variable.

This library makes using that variable trivial by adding file port
creation functions that take a second argument after the filename that
describes the format to use.

The following encodings are supported at the minimum: `'us-ascii`,
`'iso-8859-1`, `'utf-8`, `'utf-16`, `'utf-16le` and `'utf-16be`.

A complete list of supported character encodings can be obtained with
`(available-encodings)`.

If the encoding is `#t`, a locale-dependant encoding is selected by java.

If the encoding is `#f`, it is treated as a binary file with no character
encoding conversion.

The provided functions:

* open-encoded-input-file filename encoding
* open-encoded-output-file filename encoding
* with-input-from-encoded-file filename encoding thunk
* with-output-to-encoded-file filename encoding thunk
* call-with-encoded-input-file filename encoding proc
* call-with-encoded-output-file filename encoding proc
* open-encoded-input-bytevector bv encoding
* open-encoded-output-bytevector encoding
* call-with-encoded-input-bytevector bv encoding proc
* call-with-encoded-output-bytevector encoding proc
* available-encodings

String encodings
----------------

There are also functions for converting strings to and from
bytevectors in a given encoding (kawa only has built in support for
utf8 and utf16 variants; the following two functions can be used with
any supported character encoding.)

### string->encoded-bytevector str encoding
### encoded-bytevector->string bv encoding

Base64 encoding of bytevectors and binary ports is also supported:

### bytevector->base64 bv [format]
### base64->bytevector str [format]

These functions convert between bytevectors and Base64 encoded
strings. Supported formats are `'basic` (The default), `'url` and
`'mime`.
See
[java.util.Base64](https://docs.oracle.com/javase/8/docs/api/java/util/Base64.html)
documentation for details on the meanings of these.

### make-base64-output-port output-port [format]

Wrap a binary output port in a Base64 encoding layer.

Example:

```
(define out (make-base64-output-port (open-binary-output-file "data.txt")))

(write-bytevector bv out)
```

### make-base64-input-port input-port [format]

Wrap a binary input port in a Base64 decoding layer.

Input functions
---------------

### Ports

#### read-lines [port] [handle-newline]

Return a list of all lines read from port.

#### stream-lines [port] [handle-newline]

Return a SRFI-41 stream that reads lines from port.

#### read-word [port]

Read a single space-seperated word from port and return it as a string.
Returns an eof-object at end of file.

#### stream-words [port]

Return a stream of words read from port.

#### read-words [port]

Return a list of words read from port.

#### read-number [port]

Reads a single space-seperated number from port and returns it, or
raises an error if the first word of the string couldn't be converted
to a number.

Returns an eof-object at end of file.

#### stream-numbers [port]

Return a stream of numbers read from port.

#### read-numbers [port]

Return a list of numbers read from port.

#### stream-sexprs [port]

Return a stream of sexp tokens read from port.

#### read-sexprs [port]

Return a list of sexp tokens read from port.

#### skip-leading-whitespace [port]

Read and discard characters from port until a non-whitespace character
is encountered. Returns `#f` at end of file.

### Scanners

These functions wrap `java.util.Scanner`. As a result the number
reading ones use Java syntax, not Scheme syntax.

#### scanner? obj

Returns true if obj is a scanner.

#### open-input-scanner-file name [encoding]

Open a scanner for the file.

#### input-port->scanner port [encoding]

Create a scanner from an open file.

#### string->scanner str

Create a scanner that reads from a string.

#### scanner-next? scanner [pattern]

True if the scanner will match a token.

#### scanner-next scanner [pattern]

Returns the next token.

#### scanner-next-bool? scanner

Returns true if the next token can be interpeted as a boolean ("true" or "false").

#### scanner-next-bool scanner

Returns the next boolean token.

#### scanner-next-byte? scanner [radix]
#### scanner-next-byte scanner [radix]
#### scanner-next-short? scanner [radix]
#### scanner-next-short scanner [radix]
#### scanner-next-int? scanner [radix]
#### scanner-next-int scanner [radix]
#### scanner-next-long? scanner [radix]
#### scanner-next-long scanner [radix]
#### scanner-next-float? scanner
#### scanner-next-float scanner
#### scanner-next-double? scanner
#### scanner-next-double scanner

Tests the next token and returns it for their respective types.

#### scanner-next-line? scanner
#### scanner-next-line scanner

Tests for and returns the next line from the input.

#### scanner-find-in-line scanner pattern

Returns text matching pattern in the current line, or false.

#### scanner-find-in-input scanner pattern [horizon]

Returns text matching pattern in the next horizon code points, or till end of input.

#### scanner-skip scanner pattern

Skips text matching pattern. Pattern must match the head of the input stream.

#### scanner-get-radix scanner
#### scanner-set-radix! scanner r

Get and set the default radix for integer types.

#### scanner-get-delimiter scanner
#### scanner-set-delimiter! scanner pattern

Get and set the regular expression used as the default token delimiter for
`scanner-next?` and `scanner-next`.

#### scanner->generator scanner token? token

Returns a generator function that uses `token` to scan input until
`token?` returns false. The two functions should be for the same token
type. Example: `(scanner->generator s scanner-next-int?
scanner-next-int)`.

#### scanner->string scanner token? token

Does the same, but returns a SRFI-41 stream.

### input-parse

Oleg Kiselyov's [input-parse](http://okmij.org/ftp/Scheme/parsing.html) and
`find-string-from-port?` routines are included as well.

Output functions
----------------

### print val ...

Outputs the optional arguments val ... using display and writes a
newline character to the port that is the value of
(current-output-port). From chicken.

### print* val ...

Similar to print, but does not output a terminating newline character
and performs a flush-output-port after writing its arguments. From chicken.

### println val [port]

Output val as with display, followed by a newline. From racket.

### write-list list [port] [sep: char]

Output the elements of list using write, without displaying the
enclosing list parenthesis. The optional sep keyword specifies a
character to put between elements (Defaults to space).

Thus, to output a list with each element on its own line: `(write-list
'(1 2 3) sep: #\newline)`.

### display-list list [port] [sep: char]

Output the elements of list using display, without displaying the
enclosing list parenthesis. The optional sep keyword specifies a
character to put between elements (Defaults to space).

Port iterators
--------------

Ideas and API for these came from chicken scheme's ports
library. THUNK can be any SRFI-158 generator, not just an input
function.

### port-for-each fn thunk

Apply FN to successive results of calling the zero argument procedure
THUNK (typically read) until it returns EOF, discarding the results.

### port-map fn thunk

Apply FN to successive results of calling the zero argument procedure
THUNK (typically read) until it returns EOF, returning a list of the
collected results.

### port-fold fn acc thunk

Apply FN to successive results of calling the zero argument procedure
THUNK, (typically read) passing the ACC value as the second
argument. The FN result becomes the new ACC value. When THUNK returns
EOF, the last FN result is returned.

### copy-port from to [[read] [write]]

Reads all remaining data from port FROM using the reader procedure
READ and writes it to port TO using the writer procedure WRITE. READ
defaults to read-char and WRITE to write-char. Note that this
procedure does not check FROM and TO for being ports, so the reader
and writer procedures may perform arbitrary operations as long as they
can be invoked as (READ FROM) and (WRITE X TO),
respectively. copy-port returns an undefined value.
