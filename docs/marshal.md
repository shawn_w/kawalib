# Marshalling library

Serialize and deserialize objects. Only works with types that
implement java.lang.Serializable.

## Functions

### serialize obj

Serializes obj and returns it as a `bytevector`

### serialized->object bv

Takes a `bytevector` that represents a serialized object and returns a
scheme object.

### serialized->prim bv

Takes a `bytevector` that represents a serialized object and returns a
scheme object or a primitive type depending on the object.

### bytewarray->bytevector ba

Converts a `byte[]` object to a `bytevector`.

### univector->array univec

Converts a uniform vector to a java array of the proper type.

### object->prim obj

If object is a class that wraps a primitive numeric type, return it as
that type. Otherwise return the object.
