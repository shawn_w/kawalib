# Priority Queue and Heaps

API from
[SLIB](http://people.csail.mit.edu/jaffer/slib/Priority-Queues.html#Priority-Queues)
with some extensions.

The implementation uses `java.util.PriorityQueue`.

## make-heap pred? [equal: equal-pred?] [values-list]

Returns a new heap using the provided ordering and optional equality
predicates. If given a list of values, all of them are added to the
heap. `pred?` should return true if its first argument is higher
priority than its second. The optional equal: keyword predicate should
return true if they have the same prority.

## heap pred? [equal: equal-pred?] [value ...]

Returns a new heap using the provided ordering and optional equality
predicates. If given values, all of them are added to the heap.

## heap? object

Returns `#t` if object is a heap.

## heap-length heap

Returns the number of elements stored in the heap.

## heap-empty? heap

Returns `#t` if the heap is empty.

## heap-insert! heap value

Adds a new element to the heap.

## heap-peek heap

Returns the max element of the heap, or raises an error if the heap is
empty.

## heap-extract-max! heap

Remove the max element and returns it, or raises an error if the heap
is empty.

## heap->list

Returns a list of the elements in the heap, in heap order.

## heap->sorted-list

Returns a list of the elements in the heap, in sorted order.

## heap->vector

Returns a vector of the elements in the heap, in heap order.

## heap->sorted-vector

Returns a vector of the elements in the heap, in sorted order.

## heap-sort pred? lst

Returns a new list sorted using heap sort.

## heap-sort-vector! pred? vec

Heap sorts the vector argument in place.
