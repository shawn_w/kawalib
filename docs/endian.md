# Endian conversion

While SRFI-74 has conversions between fixnums and bytearrays that
support both big and little endian, it lacks a way to directly convert
numbers from one endianness to the other. Thus. this module.

## Explicit conversions

For all fixnum 16, 32 and 64 bit types, signed and unsigned, there's a
function to convert from one endianness to the other. They have the
form `TYPE-little->big` and `TYPE-big->little`, for example
`u32-little->big`.

## Conversions to and from native endianness

These functions convert between native endianness and either big or
little. They have the form `TYPE-native->TARGET` and
`TYPE-TARGET->native`, for example `u32-native->little` and
`s32-big->native`. `u16-native->big` is the equivalent to the C
`htons()` and so on.
