[SRFI 111: Boxes](https://srfi.schemers.org/srfi-111/srfi-111.html)

In addition to the interface described in the reference documentation, the kawalib implmentation adds several things.

# Extensions

## box? val

Returns `#t` if val is any kind of box.

## unbox box-type

If passed a non-box value, unbox returns that value. It also works
with the following box specializations.

## sbox? val

Returns true if an object is a standard box.

## sbox-ref box

Fetches the value stored in a standard box.

# New box types

## Immutable boxes

These boxes don't support changing what object they hold.

### ibox val

Returns a new immutable box.

### ibox? val

Return true if a object is an immutable box.

### ibox-ref bx

Returns the value stored in the box.

## Weak boxes.

Weak references to objects that might get garbage collected. Trying to
unbox a collected weak box will return `#f`.

### wbox val

Returns a new weak box holding a weak copy of val.

### wbox? val

Returns true if an object is a weak box.

### wbox-ref wbox

Returns the object in the weak box, or #f if it no longer exists.

### wbox-set! wbox newval

Stores a weak reference to `newval` in the weak box.


