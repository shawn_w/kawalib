% Optional

Wraps `java.util.Optional`.

Functions
=========

Constructors
------------

### make-optional val

Returns a new optional wrapping val.

### make-empty-optional

Returns a new empty optional.

Predicates
----------

### optional? obj

Returns true of obj is an optional.

### opt-set? opt

Returns true if opt holds a value.

Extracting values
-----------------

### opt-ref opt

Returns the value in the optional or raises an error.

### opt-ref/default opt def

Returns the value in the optional, or def if it's empty.

### opt-ref/get opt thunk

Returns the value in the optional or the result of calling thunk if
it's empty.

Mapping and friends
-------------------

### opt-filter pred? opt

If opt has a value that makes pred? returns true, returns an optional
holding the same value, otherwise an empty optional.

### opt-map f opt

If opt has a value, returns a new optional holding the result of
applying f to that value, otherwise an empty one.

### opt-flat-map f opt

If opt has a value, returns a new optional holding the result of
applying f to that value, otherwise an empty one. f should return an
optional.

### opt-call proc opt

If opt has a value, calls proc with that value.

Miscellanous
------------

### optional=? opta optb

Returns true if the two optionals both have values that compare equal
per the java equals method.

### optional-hash opt

Returns a hash code for opt.
