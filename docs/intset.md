Integer Sets
============

Functional, space-efficient storage of sets of integers. Unlike the
bitset type, which uses an array with 1 bit per number, so it's total
size is based on `maxvalue / 8`, intsets can hold a very large maximum
value without taking up a large amount of space. Ranges of consecutive
numbers in particular are stored very compactly.

Uses
[Discrete Interval Encoding Trees](https://web.engr.oregonstate.edu/~erwig/diet/).

Functions
---------

### Constructors ###

#### empty-intset

Returns a new empty intset.

#### range->intset min max

Returns a new intset holding the integers from min to max inclusive.

#### vector->intset intvec

Given a vector of integers, returns a new intset holding them.

#### list->intset intlist

Given a list of integers, returns a new intset holding them.

### Inserters and deleters ###

#### intset-add iset int

Returns a new intset with the given integer added to the existing intset.

#### intset-delete iset int

Returns a new intset without the given integer in it.

### Predicates ###

#### intset? val

Returns true if val is an intset.

#### intset-contains? iset num

Returns true if iset holds num.

#### intset-empty? iset

Returns true if this is the empty intset.

### Other ###

#### intset-minimum iset

Returns the smallest number held in the intset, or false if empty.

#### intset-maximum iset

Returns the largest number held in the intset, or false if empty.

#### intset-before iset n

Returns an intset of all values less than n that are in the original.

#### intset-after iset n

Returns an intset of all values greater than n that are in the original.

#### intset-within-range iset min max

Returns an intset of all values from the original that are between min
and max, exclusive.

#### intset->list iset

Returns a list of all numbers stored in the intset, in sorted order.

Future Plans
------------

Consider taking the union of two intsets, and difference.

