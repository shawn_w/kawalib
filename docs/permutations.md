# Permutations Library

Provides a few functions for permuting vectors and/or lists.

## next-permutation! vec less?

Permutes vec in place and returns `#t` if more permutations are
possible, `#f` if this permutation restored the vector to its original
(sorted) state. Basically the same as C++'s
`std::next_permutation()`. When using this in a loop, the vector
should start out sorted according to `less?`.

## permutations->list vec-or-list less?

Returns a list of all permutations of a vector or list.

## permutations->stream vec-or-list less?

Since the number of permutations gets quite large as the size of the
vector increases, it's not always desirable to generate them all at
once with `permutations->list`. This returns a **SRFI-41** stream that
provides the permutations one at a time, and can be used with
`stream-for-each`, `stream-map`, etc.

## random-source-make-permutations random-source

Given a SRFI-27 random source, teturn a generator that takes one
integer argument N and returns a random permutation of the set `{0,
..., N-1}`.

## random-permutation N

Returns a random permutation of the set `{0, ..., N-1}`
