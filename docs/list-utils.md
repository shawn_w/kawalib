% List Utility functions

Things to supplement SRFI-1. Plus a few non-list functions with
similar themes.

List creation and conversion
============================

### string->listmap f str

Like `(map f (string->list str))` but more efficient.

### string->vectormap f str

Like `(vector-map f (string->vector str))` but more efficient.

### vector->listmap f vec

Like `(map f (vector->list vec))` but more efficient.

### list->vectormap f lst

Like `(list->vector (map f lst))` but more efficient.

List searching
==============

### filter-regex re lst
### filter-regex! re list

lst is a list of strings. Returns only those elements which match the
regex.

### minmax lst [less?]

Returns two values: The minimum and maximum elements of `lst`, as
determined by the function `less?` which defaults to `<`.
