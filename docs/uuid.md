UUIDs
=====

Based on
[java.util.UUID](https://docs.oracle.com/javase/8/docs/api/java/util/UUID.html).

Also see [RFC 4122](http://www.ietf.org/rfc/rfc4122.txt).

Functions
---------

### Constructors

#### generate-uuid

Returns a new random type 4 UUID.

#### string->uuid

Converts a string representation back to a UUID object.

#### make-uuid u64vector

Build a UUID from raw bits.

Takes a two-element `u64vector`, with the first element holding the 64
most signficant bits of a UUID and the second element the least 64.

#### make-type1-uuid [node: u8vector]

Make a type 1 UUID using the current time and a MAC address of the
host. If the node: keyword argument is used, that value (A 6 element
u8vector) is used instead of the address.

This deviates slightly from the RFC in that the clock sequence isn't
saved between runs of programs using this library. A new random number
is picked every run.

#### make-type3-uuid [uuid] u8vector
#### make-type5-uuid [uuid] u8vector

The first form creates a type 3 UUID based on the MD5 hash of its
argument. The optional uuid argument is used as a namespace.

The second form creates a type 5 UUID based on the SHA1 hash of its
argument. The optional uuid argument is used as a namespace.

### Predicates and Comparison

#### uuid? obj

Returns true if obj is a UUID.

#### uuid=? a b

Tests two UUIDs for equality.

#### uuuid<? a b

Tests if UUID a is less than UUID b.

#### uuid-compare a b

Compares two UUIDs and returns less than 0, 0, or greater than 0.

#### (variable) uuid-comparator

A SRFI-128 comparator object for UUIDs.

### Query functions

#### uuid-version uuid

Returns the version field of the UUID (1-5).

#### uuid-variant uuid

Returns the variant field of the UUID.

#### uuid-decompose uuid

Returns values corresponding to the UUID fields:

    timestamp version variant clock-sequence node

Only valid for time based UUIDs (Version/Type 1)

#### uuid-split uuid

Returns a `u64vector` with two elements: The most and least
significant bits of the UUID.

### Others

#### uuid-hash uuid

Hashes a UUID.

#### uuid->string

Returns a string representation of a UUID.

Namespace UUIDs
---------------

Predefined UUIDs that can be used with `make-type3-uuid` and `make-type5-uuid`.

* uuid-namespace-dns
* uuid-namespace-url
* uuid-namespace-oid
* uuid-namespace-x500
