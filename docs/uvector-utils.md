# Uniform Vector Utility Functions

This library wraps java.util.Arrays methods for SRFI-4 uniform
vectors, as well as some functions inspired by the SRFI-133 vector
library.

For each of the 8 integer types (s8, u8, ... s64, u64) and two
floating point types (f32 f64), the following functions are defined:

### Copying

#### Tvector-copy Tvector [from [to]]

Returns a newly allocated copy of the vector, or of the range
`[from, to)` out of the vector.

#### Tvector-copy! Tvector dest-pos Tvector2 [src-pos [len]]

Copies `len` elements from `Tvector2` starting at index `src-pos`
to `Tvector` starting at `dest-pos`. Note this is **different** from
the SRFI-66 definition of `u8vector-copy!`.

### Iteration

#### Tvector-fold func initial Tvector

Left fold over a uniform vector.

#### Tvector-for-each func Tvector

Applies func to each element of Tvector starting at index 0.

#### Tvector-map func Tvector

Returns a new Tvector whose elements are the result of calling func
on the element of the same index in Tvector.

#### Tvector-map! func Tvector

Replaces each element of Tvector with the result of calling func on
the previous value of that element. Unspecified order.

### Sorting and Searching

#### Tvector-sort! Tvector
#### Tvector-sort! Tvector from to

Sorts the entire vector in ascending order, or just the range `[from,to)`.

#### Tv-binary-search Tvector Tkey [from [to]]

Return the location of key in the *sorted* vector, or `#f` if it's not found.
`[from, to)` can be used to limit the search area.

Because Java does not have native support for unsigned integer types, using values higher than the signed MAX_VALUE of a particular width unsigned type will cause this to fail.

#### Tvector-index pred? Tvector [from [to]]
#### Tvector-index-right pred? Tvector [from [to]]

Returns the first (Or last) index for which `pred?` returns true, or `#f` if none do.

#### Tvector-find Tval Tvector [from [to]]
### Tvector-find-right Tval Tvector [from [to]]

Returns the first (Or last) index in the vector that is equal to Tval, or `#f` if not found.

### Predicates

#### Tvector=? Tvector Tvector

Returns true if the two vectors are the same length and with identical
elements.

#### Tvector<? Tvector Tvector

Returns true if the first vector is less than the second. It is lesser
if it is shorter, or if the two vectors are the same length and every
element is less than the corresponding element of the second.

#### [variable] Tvector-comparator

A SRFI-128 comparator for the uniform vector type.

### Miscellaneous

#### Tvector-hash Tvector

Returns a hash code for the vector.

#### Tvector-fill! Tvector value [from [to]]

Sets every element of the vector to value, or those in the range
`[from,to)`.



